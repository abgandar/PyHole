/* Spherical metric in Oldenburg form. */

// The user arguments to be passed down from the kernel
// Note that the leading comma is absolutely required!
// USER_ARGS_DEF must be such that:
// void func( int x, int g USER_ARGS_DEF )
// expands to a valid function definition
#define USER_ARGS_DEF , read_only image2d_t img, read_only image2d_t dX_img, read_only image2d_t dth_img
// USER_ARGS must be such that:
// func( 1, 2 USER_ARGS )
// expands to a valid function call when used inside a function with USER_ARGS_DEF in its function list
#define USER_ARGS , img, dX_img, dth_img


// declare the sampler used for the interpolation (must be done in program scope according to OpenCL 2.0 Spec pg. 51)
#if FIX_SAMPLER==1
// Implement our own linear interpolation sampler. This works around broken implementations in various GPUs and CPUs.
constant sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST | CLK_NORMALIZED_COORDS_FALSE;

inline static float4 my_read_imagef( read_only image2d_t img, const sampler_t sampler, float2 x_img )
{
    x_img -= 0.5f;
    const float2 i0 = floor( x_img );
    const float2 a = x_img - i0;
    return (1.0f - a.x)*(1.0f - a.y)*read_imagef( img, sampler, i0 )
         + a.x*(1.0f - a.y)*read_imagef( img, sampler, i0+(float2)(1.0, 0.0) )
         + (1.0f - a.x)*a.y*read_imagef( img, sampler, i0+(float2)(0.0, 1.0) )
         + a.x*a.y*read_imagef( img, sampler, i0+(float2)(1.0, 1.0) );
}
#else
// use built in linear interpolation
constant sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR | CLK_NORMALIZED_COORDS_FALSE;
#define my_read_imagef( img, sampler, x_img ) read_imagef( img, sampler, x_img )
#endif

// Convert radius r into rescaled variable X used in the interpolation function and its derivative dX/dr.
inline static real2 getX( const real l )
{
    return (real2)(atan(l/R0), R0/(R0*R0 + l*l));
}

// Compute the components of the metric at x and store them in the metric g.
static bool updateMetric( const realV x, metric* restrict g USER_ARGS_DEF )
{
    // ugly but ~2% faster than using "const real r = x.s1;" etc. instead of define
    #define l (x.s1)
    const real theta = maxmag( x.s2, (real)1e-10 );     // prevent coordinate singularity

    // interpolate and take derivatives
    const real2 XX = getX( l );
    #define X (XX.s0)
    #define dl (XX.s1)
    const float2 x_img = (float2)((NX-1)/PI*(X+PI*0.5), (NTHETA-1)/PI*theta)+0.5f;    // for normalized coordinates add /NX and /NTHETA

    const real4 pnt = convert_real4( my_read_imagef( img, sampler, x_img ) );
    #define f (pnt.s0)
    #define omega (pnt.s1)
    #define nu (pnt.s2)

    const real4 dr_pnt = dl*convert_real4( my_read_imagef( dX_img, sampler, x_img ) );
    #define dl_f (dr_pnt.s0)
    #define dl_omega (dr_pnt.s1)
    #define dl_nu (dr_pnt.s2)

    const real4 dth_pnt = convert_real4( my_read_imagef( dth_img, sampler, x_img ) );
    #define dth_f (dth_pnt.s0)
    #define dth_omega (dth_pnt.s1)
    #define dth_nu (dth_pnt.s2)

    // some common terms for reuse
    const real h = l*l + R0*R0;
    const real expf = exp(-f);
    const real expnu = exp(-nu);
    const real omega2 = omega*omega;
    const real st = sin(theta);
    const real ct = cos(theta);
    const real st2 = st*st;

    // calculate actual contravariant metric
    g->tt = -expf;
    g->rr = expnu/expf;        // this is the same as g->ll
    g->thth = g->rr/h;
    g->pp = 1.0/(expf*h*st2) - expf*omega2;
    g->tp = omega*expf;

    // note dr_ is the same as dl_
    g->dr_tt = dl_f*expf;
    g->dr_rr = (dl_f-dl_nu)*expnu/expf;
    g->dr_thth = g->dr_rr/h - 2.0*l*expnu/(h*h*expf);
    g->dr_pp = (dl_f - 2.0*l/h)/(expf*h*st2) + (dl_f*omega - 2.0*dl_omega)*omega*expf;
    g->dr_tp = dl_omega*expf - dl_f*omega*expf;

    g->dth_tt = dth_f*expf;
    g->dth_rr = (dth_f-dth_nu)*expnu/expf;
    g->dth_thth = g->dth_rr/h;
    g->dth_pp = (dth_f - 2.0*ct/st)/(expf*h*st2) + (dth_f*omega - 2.0*dth_omega)*omega*expf;
    g->dth_tp = dth_omega*expf - dth_f*omega*expf;

    // wormholes are always good
    return 0;

    #undef l
    #undef X
    #undef dl
    #undef f
    #undef omega
    #undef nu
    #undef dl_f
    #undef dl_omega
    #undef dl_nu
    #undef dth_f
    #undef dth_omega
    #undef dth_nu
}
