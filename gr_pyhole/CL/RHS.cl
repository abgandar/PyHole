// structure to hold components of the spherical metric
// must be defined before including the user defined metric
typedef struct _metric {
    real tt;
    real rr;
    real thth;
    real pp;
    real tp;

    real dr_tt;
    real dr_rr;
    real dr_thth;
    real dr_pp;
    real dr_tp;

    real dth_tt;
    real dth_rr;
    real dth_thth;
    real dth_pp;
    real dth_tp;
} metric;

// include the user defined metric function which also sets the additional user
// arguments etc. used below
#include RK_INCLUDE(METRIC_CL)

// check if the metric didn't define any user arguments and set them to empty (i.e. none)
#ifndef USER_ARGS
#define USER_ARGS
#endif

#ifndef USER_ARGS_DEF
#define USER_ARGS_DEF
#endif

// Compute the momenta from a viewing direction
inline static realV getMomenta( const realV x, const real3 v USER_ARGS_DEF )
{
    metric g;
    updateMetric( x, &g USER_ARGS );    // result is ignored
    const real sa = sin( x.s4 );
    const real ca = cos( x.s4 );
    const real sb = sin( x.s5 );
    const real cb = cos( x.s5 );

    // covariant
    const real det = 1.0/(g.tt*g.pp-g.tp*g.tp);      // covariant determinant
    const real g_tt = det*g.pp;
    const real g_rr = 1.0/g.rr;
    const real g_thth = 1.0/g.thth;
    const real g_pp = det*g.tt;
    const real g_tp = -det*g.tp;

    // calculate K matrix
    const real A = sqrt( -g_pp/det );
    const real B = -g_tp/g_pp*A;
    const real C = 1.0/sqrt( g_pp );
    const real D = 1.0/sqrt( g_thth );
    const real E = 1.0/sqrt( g_rr );
    const real v2 = dot( v, v );
    const real sv = 1.0/sqrt( 1.0-v2 );
    const real sv1 = 1.0/sqrt( 1.0-v.s0*v.s0 );
    const real sv12 = 1.0/sqrt( 1.0-v.s0*v.s0-v.s1*v.s1 );
    const real K00 = sv*A;
    const real K01 = -sv*v.s2*E;
    const real K02 = sv*v.s1*D;
    const real K03 = sv*(B+v.s0*C);
    const real K10 = sv1*A*v.s0;
    //const real K11 = 0.0;
    //const real K12 = 0.0;
    const real K13 = sv1*(C+v.s0*B);
    const real K20 = sv1*sv12*v.s1*A;
    //const real K21 = 0.0;
    const real K22 = sv1*sv12*(1.0-v.s0*v.s0)*D;
    const real K23 = sv1*sv12*(v.s1*B+v.s0*v.s1*C);
    const real K30 = sv*sv12*v.s2*A;
    const real K31 = -sv*sv12*(1.0-v.s0*v.s0-v.s1*v.s1)*E;
    const real K32 = sv*sv12*v.s1*v.s2*D;
    const real K33 = sv*sv12*(v.s2*B+v.s0*v.s2*C);
    // calculate dot
    real4 dot = (real4)(K00-sa*cb*K10-sa*sb*K20-ca*K30,
                        K01-ca*K31,
                        K02-sa*sb*K22-ca*K32,
                        K03-sa*cb*K13-sa*sb*K23-ca*K33);
    // calculate actual initial state
    realV res = (realV)(0.0);
    res.s0123 = x.s0123;    // first 4 components are the same
    res.s4 = g_tt*dot.s0 + g_tp*dot.s3;
    res.s5 = g_rr*dot.s1;
    res.s6 = g_thth*dot.s2;
    res.s7 = g_pp*dot.s3 + g_tp*dot.s0;

    return res;
}

// compute the radius from a given state x
#define RADIUS(x) (x.s1)

// compute the radial velocity at state x and y=RHS(x)
#define RADIAL_VELOCITY(x, y) (y.s1)

/* Right hand side of the ODE for propagation.

   :param t: Independent integration variable (lambda).
   :param x: Point where to evaluate the RHS.
   :param y: Pointer to the result of the RHS evaluation.
   :param : Any user defined extra function arguments
   :return: error status that is true if there has been an error.
*/
inline static bool RHS( const real t, const realV x, realV* restrict y USER_ARGS_DEF )
{
    metric g;
    const bool error = updateMetric( x, &g USER_ARGS );

    *y = (realV)(0.0);
    (*y).s0 = x.s4*g.tt+x.s7*g.tp;      // ugly (*y). notation is because AMD OpenCL doesn't like y->
    (*y).s1 = x.s5*g.rr;
    (*y).s2 = x.s6*g.thth;
    (*y).s3 = x.s7*g.pp+x.s4*g.tp;
    //(*y).s4 = 0.0;         // already initialized to zero
    (*y).s5 = -0.5*(x.s4*x.s4*g.dr_tt +x.s5*x.s5*g.dr_rr +x.s6*x.s6*g.dr_thth +x.s7*x.s7*g.dr_pp) -x.s4*x.s7*g.dr_tp;
    (*y).s6 = -0.5*(x.s4*x.s4*g.dth_tt+x.s5*x.s5*g.dth_rr+x.s6*x.s6*g.dth_thth+x.s7*x.s7*g.dth_pp)-x.s4*x.s7*g.dth_tp;
    //(*y).s7 = 0.0;
#if DIM>8
    (*y).s8 = (*y).s3;
    (*y).s9 = fabs( (*y).s3 );
#endif
    return error;
}

// Compute the null condition (i.e. g_ij * p^i * p^j)
inline static real getNullCondition( const realV x USER_ARGS_DEF )
{
    metric g;
    const bool error = updateMetric( x, &g USER_ARGS );
    return error ? NAN : 0.5*(x.s4*x.s4*g.tt+x.s5*x.s5*g.rr+x.s6*x.s6*g.thth+x.s7*x.s7*g.pp)+x.s4*x.s7*g.tp;
}
