/* Petya's metric in spherical coordinates. */


// The user arguments to be passed down from the kernel
// Note that the leading comma is absolutely required!
// USER_ARGS_DEF must be such that:
// void func( int x, int g USER_ARGS_DEF )
// expands to a valid function definition
#define USER_ARGS_DEF , global const real* restrict a
// USER_ARGS must be such that:
// func( 1, 2 USER_ARGS )
// expands to a valid function call when used inside a function with USER_ARGS_DEF in its function list
#define USER_ARGS , a

// Compute values of the first CONST_N Legendre polynomials P_n(x)
static void LegendrePolynomials( const real x, real* P )
{
    P[0] = 1.0;
    P[1] = x;
    #pragma unroll
    for( unsigned int i = 2; i < CONST_NA; i++ )
        P[i] = ((2*i-1)*x*P[i-1]-(i-1)*P[i-2])/i;
}

// compute U, dx_U and dy_U all at once given R0=sqrt(x^2+y^2-1) and xy
static real3 U( const real R0, const real2 xy, const real* P USER_ARGS_DEF )
{
    real R = 1.0;
    real3 res = (real3)(a[0], 0.0, 0.0);    // n=0 terms are computed explicitly (not applicable for derivatives)
    const real2 denom = 1.0/(xy*xy-1.0);
    #pragma unroll
    for( unsigned int n = 1; n < CONST_NA; n++ )
    {
        res.s12 += a[n]*n*R*(xy*R0*P[n]-xy.yx*P[n-1])*denom;    // swizzle and twizzle away....
        R *= R0;    // now: R=R0^n
        res.s0 += a[n]*R*P[n];
    }

    return res;
}

// compute gamma, dx_gamma and dy_gamma all at once given R0=sqrt(x^2+y^2-1) and xy
static real3 gamma( const real R0, const real2 xy, const real* P, const real3 U USER_ARGS_DEF )
{
    real R = 1.0;
    real3 res = (real3)(0.0);
//    const real2 denom = 1.0/(xy*xy-1.0);

    #pragma unroll
    for( unsigned int n = 1; n < CONST_NA; n++ )
    {
        // second term
        real RR = a[n];
        for( unsigned int k = 0; k < n; k++ )
        {
            const real f = (((n-k)%2) ? xy.y : -xy.x);
            res.s0 += 2.0*f*RR*P[k];
            RR *= R0;   // now: RR = R0^(k+1)*a[n]
        }
//        res.s1 += 2.0*a[n]*n*R*(R0*P[n]-xy.x*xy.y*P[n-1])*denom.x;
//        res.s2 += 2.0*a[n]*n*R*P[n-1];

        // first term
        real fact = n*a[n]*R;
        #pragma unroll
        for( unsigned int k = 1; k < CONST_NA; k++ )
        {
//            res.s12 += fact*k*a[k]*(xy*R*R*P[n]*P[k]-xy*(xy*xy-xy.yx*xy.yx-1.0)*P[n-1]*P[k-1]-2.0*xy.yx*R0*P[n-1]*P[k])*denom;
            fact *= R0;     // now: n*a[n]*R0^(n-1+k)
            res.s0 += fact*R0*k*a[k]*(P[n]*P[k]-P[n-1]*P[k-1])/(n+k);
        }
        R *= R0;    // R now is R0^n
    }

    // alternative implementation using ODE (2) defining gamma to compute derivatives
    // seems to be numerically more stable this way
    #define dx_U (U.s1)
    #define dy_U (U.s2)
    res.s1 = (1.0-xy.y*xy.y)/(xy.x*xy.x-xy.y*xy.y)*(xy.x*(xy.x*xy.x-1.0)*dx_U*dx_U - xy.x*(1.0-xy.y*xy.y)*dy_U*dy_U-2.0*xy.y*(xy.x*xy.x-1.0)*dx_U*dy_U+2.0*xy.x*dx_U-2.0*xy.y*dy_U);
    res.s2 = (xy.x*xy.x-1.0)/(xy.x*xy.x-xy.y*xy.y)*(xy.y*(xy.x*xy.x-1.0)*dx_U*dx_U - xy.y*(1.0-xy.y*xy.y)*dy_U*dy_U-2.0*xy.x*(xy.y*xy.y-1.0)*dx_U*dy_U) + 1.0/(xy.x*xy.x-xy.y*xy.y)*(2.0*xy.y*(xy.x*xy.x-1.0)*dx_U-2.0*xy.x*(xy.y*xy.y-1.0)*dy_U);
    #undef dx_U
    #undef dy_U

    return res;
}

// Compute the components of the metric at x and store them in the metric g.
static bool updateMetric( const realV x, metric* restrict g USER_ARGS_DEF )
{
    const real r = x.s1;
    const real r2 = r*r;
    const real theta = maxmag( x.s2, (real)1e-10 );     // prevent coordinate singularity
    const real st = sin( theta );
    const real st2 = st*st;
    const real ct = cos( theta );
    const real2 xy = (real2)(r-1.0, ct);
    const real R = sqrt( xy.x*xy.x + xy.y*xy.y - 1.0 );

    real P[CONST_NA];
    LegendrePolynomials( xy.x*xy.y/R, P );

    const real3 UU = U( R, xy, P, a );
    const real e2U = exp( 2.0*(UU.s0-(CONST_U0)) );
    #define dx_U (UU.s1)
    #define dy_U (UU.s2)
    #define dr_U dx_U
    const real dth_U = -dy_U*st;

    const real3 gg = gamma( R, xy, P, UU, a );
    //const real e2gamma = exp( 2.0*gg.s0 );
    #define dx_gamma (gg.s1)
    #define dy_gamma (gg.s2)
    #define dr_gamma dx_gamma
    const real dth_gamma = -dy_gamma*st;

    // helps avoiding overflows and underflows when these get large
    const real e2Ugamma = exp( 2.0*(UU.s0-(CONST_U0) - gg.s0) );

    g->tt = -1.0/((1.0-2.0/r)*e2U);
    g->rr = (1.0-2.0/r)*e2Ugamma;
    g->thth = e2Ugamma/(r2);
    g->pp = e2U/(r2*st2);
    g->tp = 0.0;

    g->dr_tt = 2.0/(e2U*(r-2.0)*(r-2.0))*(1.0+r*(r-2.0)*dr_U);
    g->dr_rr = 2.0*e2Ugamma/(r2)*(1.0+r*(r-2.0)*(dr_U-dr_gamma));
    g->dr_thth = -2.0*e2Ugamma/(r2*r)*(1.0-r*(dr_U-dr_gamma));
    g->dr_pp = -2.0*e2U/(r2*r*st2)*(1.0-r*dr_U);
    g->dr_tp = 0.0;

    g->dth_tt = 2.0*r*dth_U/(e2U*(r-2.0));
    g->dth_rr = 2.0*(r-2.0)*e2Ugamma/(r)*(dth_U-dth_gamma);
    g->dth_thth = 2.0*e2Ugamma/(r2)*(dth_U-dth_gamma);
    g->dth_pp = -2.0*e2U/(r2*st2*st)*(ct-st*dth_U);
    g->dth_tp = 0.0;

    return r<RCUTOFF;

    #undef dx_U
    #undef dy_U
    #undef dr_U
    #undef dx_gamma
    #undef dy_gamma
    #undef dr_gamma
}
