/* Spherical metric in Oldenburg form. */

// The user arguments to be passed down from the kernel
// Note that the leading comma is absolutely required!
// USER_ARGS_DEF must be such that:
// void func( int x, int g USER_ARGS_DEF )
// expands to a valid function definition
#define USER_ARGS_DEF , read_only image2d_t img, read_only image2d_t dX_img, read_only image2d_t dth_img
// USER_ARGS must be such that:
// func( 1, 2 USER_ARGS )
// expands to a valid function call when used inside a function with USER_ARGS_DEF in its function list
#define USER_ARGS , img, dX_img, dth_img


// declare the sampler used for the interpolation (must be done in program scope according to OpenCL 2.0 Spec pg. 51)
#if FIX_SAMPLER==1
// Implement our own linear interpolation sampler. This works around broken implementations in various GPUs and CPUs.
constant sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST | CLK_NORMALIZED_COORDS_FALSE;

inline static float4 my_read_imagef( read_only image2d_t img, const sampler_t sampler, float2 x_img )
{
    x_img -= 0.5f;
    const float2 i0 = floor( x_img );
    const float2 a = x_img - i0;
    return (1.0f - a.x)*(1.0f - a.y)*read_imagef( img, sampler, i0 )
         + a.x*(1.0f - a.y)*read_imagef( img, sampler, i0+(float2)(1.0, 0.0) )
         + (1.0f - a.x)*a.y*read_imagef( img, sampler, i0+(float2)(0.0, 1.0) )
         + a.x*a.y*read_imagef( img, sampler, i0+(float2)(1.0, 1.0) );
}
#else
// use built in linear interpolation
constant sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR | CLK_NORMALIZED_COORDS_FALSE;
#define my_read_imagef( img, sampler, x_img ) read_imagef( img, sampler, x_img )
#endif

// Convert radius r into rescaled variable X used in the interpolation function and its derivative dX/dr.
inline static real2 getX( const real r )
{
    return (real2)((r - EH)/(1.0 + r), (1.0 + EH)/((1.0 + r)*(1.0 + r)));
}

// Compute the components of the metric at x and store them in the metric g.
static bool updateMetric( const realV x, metric* restrict g USER_ARGS_DEF )
{
    // ugly but ~2% faster than using "const real r = x.s1;" etc. instead of define
    #define r (x.s1)
    const real theta = maxmag( x.s2, (real)1e-10 );     // prevent coordinate singularity

    // interpolate and take derivatives
    const real2 XX = getX( r );
    #define X (XX.s0)
    #define dr (XX.s1)
    const float2 x_img = (float2)((NX-1)*X, (NTHETA-1)/PI*theta)+0.5f;    // for normalized coordinates add /NX and /NTHETA

    const real4 pnt = convert_real4( my_read_imagef( img, sampler, x_img ) );
    #define f0 (pnt.s0)
    #define f1 (pnt.s1)
    #define f2 (pnt.s2)
    #define omega  (-pnt.s3)

    const real4 dr_pnt = dr*convert_real4( my_read_imagef( dX_img, sampler, x_img ) );
    #define dr_f0 (dr_pnt.s0)
    #define dr_f1 (dr_pnt.s1)
    #define dr_f2 (dr_pnt.s2)
    #define dr_omega  (-dr_pnt.s3)

    const real4 dth_pnt = convert_real4( my_read_imagef( dth_img, sampler, x_img ) );
    #define dth_f0 (dth_pnt.s0)
    #define dth_f1 (dth_pnt.s1)
    #define dth_f2 (dth_pnt.s2)
    #define dth_omega  (-dth_pnt.s3)

    // some common terms for reuse
    const real r2 = r*r;
    const real f02 = f0*f0;
    const real f12 = f1*f1;
    const real f22 = f2*f2;
    const real omega2 = omega*omega;
    const real st = sin(theta);
    const real st2 = st*st;

    // calculate actual contravariant metric
    g->tt = -1.0/f0;
    g->rr = 1.0/f1;
    g->thth = g->rr/r2;
    g->pp = 1.0/(f2*r2*st2) - omega2/f0;
    g->tp = omega/f0;

    g->dr_tt = dr_f0/f02;
    g->dr_rr = -dr_f1/f12;
    g->dr_thth = g->dr_rr/r2 - 2.0/(f1*r2*r);
    g->dr_pp = -2.0*omega*dr_omega/f0 + omega2*dr_f0/f02 - dr_f2/(f22*r2*st2) - 2.0/(f2*r*r2*st2);
    g->dr_tp = dr_omega/f0 - omega*dr_f0/f02;

    g->dth_tt = dth_f0/f02;
    g->dth_rr = -dth_f1/f12;
    g->dth_thth = g->dth_rr/r2;
    g->dth_pp = -2.0*omega*dth_omega/f0 + omega2*dth_f0/f02 - dth_f2/(f22*r2*st2) - 2.0*cos(theta)/(f2*r2*st*st2);
    g->dth_tp = dth_omega/f0 - omega*dth_f0/f02;

    // check if fallen in.
    return r<RCUTOFF;

    #undef r
    #undef X
    #undef dr
    #undef f0
    #undef f1
    #undef f2
    #undef omega
    #undef dr_f0
    #undef dr_f1
    #undef dr_f2
    #undef dr_omega
    #undef dth_f0
    #undef dth_f1
    #undef dth_f2
    #undef dth_omega
}
