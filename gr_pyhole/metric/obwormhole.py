# -*- coding: utf-8 -*-

#   Copyright 2015 - 2016 Alexander Wittig, Jai Grover
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# what is imported by "from oldeburg import *"
__all__ = ["OBWormhole"]

import numpy as np
from scipy.interpolate import RectBivariateSpline
from .base import SphericalMetric
from .hr import _HRCommon

class _OBWormholeCommon(_HRCommon):
    """Routines common to all Oldenburg metrics. Used as a mix-in to the Oldenburg class."""

    XBOUNDS = (-np.pi/2, np.pi/2)       #: Bounds of X variable for sampling
    THBOUNDS = (0.0, np.pi)             #: Bounds of theta variable for sampling

#    def getRadius(self, R):
#        """Get the radius in BL coordinates given a circumferential radius **R**.
#        This routine overwrites the one in SphericalMetric and also adds this
#        functionality to the Cartesian HR metrics as it is implemented directly
#        in terms of the generator functions.
#
#        :param R: The circumferential radius **R** to convert.
#        """
#        from scipy.optimize import newton
#        theta = np.pi/2
#        def f(r):
#            x = self.r2X(r)
#            return np.sqrt(self.f.F2(x, theta))*r-R
#        def df(r):
#            x = self.r2X(r)
#            dx = self.dr_r2X(r)
#            f2 = self.f.F2(x, theta)
#            df2 = dx*self.f.F2(x, theta, dX=1)
#            return np.sqrt(f2)+0.5*r/np.sqrt(f2)*df2
##        sol = newton(f, R, df)
#        return None         # XXX: to be implemented

    def X2r(self, X):
        """Convert reduced radius **X** into BL radius **r**.

        :param X: The reduced radius **X** to convert.
        """
        return self.r0*np.tan(X)

    def r2X(self, r):
        """Convert BL radius **r** into reduced radius **X**.

        :param r: The BL radius **r** to convert.
        """
        return np.arctan(r/self.r0)

    def dr_r2X(self, r):
        """Derivative dX/dr of reduced radius **X** with rrespect to BL radius **r** .

        :param r: The BL radius **r** at which to evaluate the derivative.
        """
        return self.r0/(self.r0*self.r0 + r*r)

    def cl_allocate(self, context, device, commandQueue, real):
        """Called after the OpenCL propagator has allocated the **device**,
        created a **context**, and set up a command queue. The metric here can
        perform additional setup tasks on the **device** before running
        propagations. This includes setting the various ``cl_*`` class members
        to appropriate values.

        :param context: The PyOpenCL context of the OpenCL computation.
        :param device: The PyOpenCL device used for the OpenCL computation.
        :param commandQueue: The PyOpenCL command queue on the device.
        :param real: The type of "real" numbers (either ``np.float32`` or
            ``np.float64``). Can be used as ``real(5.6)`` to convert Python numbers to
            the correct numerical memory layout expected by the OpenCL kernel
            for its arguments.
        """
        super(_OBWormholeCommon, self).cl_allocate(context, device, commandQueue, real)
        self.cl_flags += " -D R0={:.16e}".format(self.r0)


class OBWormhole(_OBWormholeCommon, SphericalMetric):
    ID = 'OBWH'    #: short identifier of this metric; used e.g. in file names
    CL_CODE = "OBWH.cl"                   #: Our OpenCL source file

    """Metric in exponential form in both spherical and Cartesian coordinates."""
    def __init__(self, f):
        """Create new exponential form blackhole.

        :param f: An HR function object.
        """
        super(OBWormhole, self).__init__()
        self.EH = 0.0  # wormhole has no event horizon
        self.rCutoff = 0.0
        self.r0 = f.r0  # we do have a minimum radius though
        self.f = f

    def __str__(self):
        """Show human readable summary of the current setup.
        """
        res = 'Metric: Oldenburg Wormhole\n'
        res = res + 'R0 (eta0):        {}\n'.format(self.r0)
        res = res + str(self.f) + '\n'
        res += super(OBWormhole,self).__str__();
        return res

    def toDisplayCoordinates(self, x):
        """Convert metric coordinates to cartesian display coordinates."""
        l = x[1]
        if isinstance(l, np.ndarray):
            color = np.empty((l.shape[0],3))
            color[l<0.0,:] = (1.0, 0.0, 0.0)
            color[l>=0.0,:] = (0.0, 0.0, 1.0)
        else:
            color = None
        theta = x[2]
        XX = self.r2X(l)
        f = self.f.F0(XX, theta)
        r = np.sqrt(np.exp(-f)*(l*l+self.r0*self.r0))
        temp = np.sin(x[2])*r
        return [x[0], temp*np.cos(x[3]), temp*np.sin(x[3]), np.cos(x[2])*r, color]

    def update(self):
        """Compute the relevant components of the metric at the current point and store them in the attributes.

        :return: An error flag that is True if an error occured (most likely the ray fell into the black hole) or False otherwise.
        """
        l = self.x[1]
        theta = self.x[2]
        # correct various coordinates. Done differently for scalars and vectors.
        if isinstance(theta, np.ndarray):
            # Hack to prevent divisions by zero: if we are too close to the coordinate singularity enforce minimum size for theta
            cond = theta<1e-12
            theta[cond] = 1e-12
        else:
            if theta<1e-12: theta = 1e-12

        # interpolate and take derivatives
        XX = self.r2X(l)       # this is the first index to use for the extrapolation table along with theta
        dl = self.dr_r2X(l)
        f = self.f.F0(XX, theta)
        omega = self.f.F1(XX, theta)
        nu = self.f.F2(XX, theta)

        dl_f = dl*self.f.F0(XX, theta, dX=1, dtheta=0)
        dl_omega = dl*self.f.F1(XX, theta, dX=1, dtheta=0)
        dl_nu = dl*self.f.F2(XX, theta, dX=1, dtheta=0)

        dth_f = self.f.F0(XX, theta, dX=0, dtheta=1)
        dth_omega = self.f.F1(XX, theta, dX=0, dtheta=1)
        dth_nu = self.f.F2(XX, theta, dX=0, dtheta=1)

        # calculate actual contravariant metric
        # some common terms for reuse
        h = l*l + self.r0*self.r0
        expf = np.exp(-f)
        expnu = np.exp(-nu)
        omega2 = omega*omega
        st = np.sin(theta)
        ct = np.cos(theta)
        st2 = st*st

        self.tt = -expf
        self.rr = expnu/expf        # this is the same as self.ll
        self.thth = self.rr/h
        self.pp = 1.0/(expf*h*st2) - expf*omega2
        self.tp = omega*expf

        # note dr_ is the same as dl_
        self.dr_tt = dl_f*expf
        self.dr_rr = (dl_f-dl_nu)*expnu/expf
        self.dr_thth = self.dr_rr/h - 2.0*l*expnu/(h*h*expf)
        self.dr_pp = (dl_f - 2.0*l/h)/(expf*h*st2) + (dl_f*omega - 2.0*dl_omega)*omega*expf
        self.dr_tp = dl_omega*expf - dl_f*omega*expf

        self.dth_tt = dth_f*expf
        self.dth_rr = (dth_f-dth_nu)*expnu/expf
        self.dth_thth = self.dth_rr/h
        self.dth_pp = (dth_f - 2.0*ct/st)/(expf*h*st2) + (dth_f*omega - 2.0*dth_omega)*omega*expf
        self.dth_tp = dth_omega*expf - dth_f*omega*expf

        return False
