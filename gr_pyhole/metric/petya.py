# -*- coding: utf-8 -*-

#   Copyright 2015 - 2016 Alexander Wittig, Jai Grover
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# what is imported by "from petya import *"
__all__ = ["Petya"]

import numpy as np
from .base import SphericalMetric

class Petya(SphericalMetric):
    """Petya's metric."""
    ID = 'P'        #: short identifier of this metric; used e.g. in file names
    CL_CODE = "Petya.cl"    #: OpenCL source file of this metric

    def __init__(self, a=[0.0, 0.0]):
        """Create new Petya blackhole.

        :param a: The multipole moments of this solution
        """
        super(Petya, self).__init__()
        self.EH = 2.0
        self.rCutoff = self.CUTOFF_FACTOR*self.EH
        self.a = list(a)
        if len(a)<2:
            self.a += [0.0]*(2-len(a))
        self.a = np.array(self.a)
        self.u0 = sum(self.a[0::2])
        if self.u0>0.0:
            print("Warning: sum of even moments is positive!")
        if sum(self.a[1::2])>1e-16:
            print("Warning: sum of odd moments is not zero!")

    def __str__(self):
        """Show human readable summary of the current setup.
        """
        res = 'Metric: Petya\n'
        res = res + 'Event horizon:        {}\n'.format(self.EH)
        res = res + 'Cutoff radius:        {}\n'.format(self.rCutoff)
        res = res + 'Moments:              {}\n'.format(self.a)
        res += super(Petya, self).__str__();
        return res

    def _getFileName(self):
        """Get a file name prefix for this metric based on the values of a_n."""
        res = "Petya"
        for i, a in enumerate(self.a[1:]):
            if a != 0: res += '-a{:d}={:.2e}'.format(i+1, a)
        return res

    def cl_allocate(self, context, device, commandQueue, real):
        """Add **CONST_U0**, **CONST_NA** and **RCUTOFF** to the OpenCL compiler
        flags to pass these values to the OpenCL metric code.
        Also allocate and load moments on OpenCL device.

        :param context: The PyOpenCL context of the OpenCL computation.
        :param device: The PyOpenCL device used for the OpenCL computation.
        :param commandQueue: The PyOpenCL command queue on the device.
        :param real: The type of "real" numbers (either ``np.float32`` or
            ``np.float64``).
        """
        super(Petya,self).cl_allocate(context, device, commandQueue, real)
        self.cl_flags += " -D RCUTOFF={:.16e} -D CONST_NA={:d} -D CONST_U0={:.16e}".format(self.rCutoff, len(self.a), self.u0)

        # Allocate memory for moments a.
        import pyopencl as cl       # only needed for these OpenCL specific routines so not included globally
        self.mem_a = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=self.a)
        self.cl_args = [self.mem_a]
        self.cl_argstypes = [None]

    def cl_free(self):
        """Free and OpenCL resources allocated earlier."""
        self.mem_a.release()

    def LegendrePolynomials(self, x):
        """Compute values of the Legendre polynomials P_n(x)"""
        N = len(self.a)
        if isinstance(x, np.ndarray):
            P = np.empty((N,)+x.shape)
        else:
            P = np.empty((N,))
        P[0] = 1.0
        P[1] = x
        for i in range(2,N):
            P[i] = ((2*i-1)*x*P[i-1]-(i-1)*P[i-2])/i
        return P

    def U(self, R0, xy, P):
        """Compute U, dx_U and dy_U all at once given R0=sqrt(x^2+y^2-1) and xy"""
        N = len(self.a)
        yx = np.array([xy[1], xy[0]])
        R = 1.0
        if isinstance(xy[0], np.ndarray):
            res = np.zeros((3,)+xy[0].shape)
        else:
            res = np.zeros((3,))
        res[0] = self.a[0]    # n=0 terms are computed explicitly (not applicable for derivatives)
        denom = 1.0/(xy*xy-1.0)
        for n in range(1,N):
            res[1:] += self.a[n]*R*n*(xy*R0*P[n]-yx*P[n-1])*denom
            R *= R0
            res[0] += self.a[n]*R*P[n]
        return res

    def gamma(self, R0, xy, P, dx_U, dy_U):
        """Compute gamma, dx_gamma and dy_gamma all at once given R0=sqrt(x^2+y^2-1) and xy"""
        N = len(self.a)
#        yx = np.array([xy[1], xy[0]])
        R = 1.0
        if isinstance(xy[0], np.ndarray):
            res = np.zeros((3,)+xy[0].shape)
        else:
            res = np.zeros((3,))
#        denom = 1.0/(xy*xy-1.0)
    
        for n in range(1,N):
            # second term
            RR = self.a[n]
            for k in range(n):
                f = xy[1] if ((n-k) % 2) == 1 else -xy[0]
                res[0] += 2.0*f*RR*P[k]
                RR *= R0
#            res[1] += 2.0*self.a[n]*n*R*(R0*P[n]-xy[0]*xy[1]*P[n-1])*denom[0]
#            res[2] += 2.0*self.a[n]*n*R*P[n-1]
    
            # first term
            fact = n*self.a[n]*R
            for k in range(1,N):
#                res[1:] += fact*k*self.a[k]*(xy*R*R*P[n]*P[k]-xy*(xy*xy-yx*yx-1.0)*P[n-1]*P[k-1]-2.0*yx*R0*P[n-1]*P[k])*denom
                fact *= R0     # now: R^(n-1)+(k)
                res[0] += fact*R0*k*self.a[k]*(P[n]*P[k]-P[n-1]*P[k-1])/(n+k)
            R *= R0    # R now is R0^n
    
        # alternative computation of derivatives using ODE definition of gamma
        res[1] = (1.0-xy[1]*xy[1])/(xy[0]*xy[0]-xy[1]*xy[1])*(xy[0]*(xy[0]*xy[0]-1.0)*dx_U*dx_U - xy[0]*(1.0-xy[1]*xy[1])*dy_U*dy_U-2.0*xy[1]*(xy[0]*xy[0]-1.0)*dx_U*dy_U+2.0*xy[0]*dx_U-2.0*xy[1]*dy_U)
        res[2] = (xy[0]*xy[0]-1.0)/(xy[0]*xy[0]-xy[1]*xy[1])*(xy[1]*(xy[0]*xy[0]-1.0)*dx_U*dx_U - xy[1]*(1.0-xy[1]*xy[1])*dy_U*dy_U-2.0*xy[0]*(xy[1]*xy[1]-1.0)*dx_U*dy_U) + 1.0/(xy[0]*xy[0]-xy[1]*xy[1])*(2.0*xy[1]*(xy[0]*xy[0]-1.0)*dx_U-2.0*xy[0]*(xy[1]*xy[1]-1.0)*dy_U)

        return res

    def update(self):
        """Compute the relevant components of the metric at the current point and store them in the attributes.

        :return: An error flag that is True if an error occured (most likely the ray fell into the black hole) or False otherwise.
        """
        r = self.x[1]
        r2 = r*r
        theta = self.x[2]
        # correct various coordinates. Done differently for scalars and vectors.
        if isinstance(r, np.ndarray):
            # correct for non-canonical coordinates. May be necessary for specific cases in flat metric.
            # Note there is no phi dependence, hence phi is not adjusted
            cond = r<0.0
            r[cond] = -r[cond]
            theta[cond] = np.pi-theta[cond]
            # check if fallen in, this is returned later.
            result = r<self.rCutoff
            # normalize theta. Note there is no phi dependence, hence phi is not adjusted
            theta = theta%(2.0*np.pi)
            cond = theta>=np.pi
            theta[cond] = 2.0*np.pi - theta[cond]
            # Hack to prevent divisions by zero: if we are too close to the coordinate singularity enforce minimum size for theta
            cond = theta<1e-12
            theta[cond] = 1e-12
        else:
            # correct for non-canonical coordinates. May be necessary for specific cases in flat metric.
            # Note there is no phi dependence, hence phi is not adjusted
            if r<0.0:
                r = -r
                theta = np.pi-theta
            # check if fallen in
            if r<self.rCutoff: return True
            result = False
            # normalize theta. Note there is no phi dependence, hence phi is not adjusted
            theta = theta%(2.0*np.pi)
            if theta>=np.pi:
                theta = 2.0*np.pi - theta
            # Hack to prevent divisions by zero: if we are too close to the coordinate singularity enforce minimum size for theta
            if theta<1e-12: theta = 1e-12

        st = np.sin(theta)
        st2 = st*st
        ct = np.cos(theta)
        xy = np.array([r-1.0, ct])
        R = np.sqrt(xy[0]*xy[0] + xy[1]*xy[1] - 1.0)
    
        P = self.LegendrePolynomials( xy[0]*xy[1]/R )
    
        U, dx_U, dy_U  = self.U(R, xy, P);
        e2U = np.exp(2.0*(U-self.u0));
        dr_U = dx_U
        dth_U = -dy_U*st
    
        gamma, dx_gamma, dy_gamma = self.gamma(R, xy, P, dx_U, dy_U)
        #e2gamma = np.exp(2.0*gamma)
        dr_gamma = dx_gamma
        dth_gamma = -dy_gamma*st
    
        e2Ugamma = np.exp(2.0*(U-self.u0-gamma));

        self.tt = -1.0/((1.0-2.0/r)*e2U)
        self.rr = (1.0-2.0/r)*e2Ugamma
        self.thth = e2Ugamma/(r2)
        self.pp = e2U/(r2*st2)
        self.tp = 0.0
    
        self.dr_tt = 2.0/(e2U*(r-2.0)*(r-2.0))*(1.0+r*(r-2.0)*dr_U)
        self.dr_rr = 2.0*e2Ugamma/(r2)*(1.0+r*(r-2.0)*(dr_U-dr_gamma))
        self.dr_thth = -2.0*e2Ugamma/(r2*r)*(1.0-r*(dr_U-dr_gamma))
        self.dr_pp = -2.0*e2U/(r2*r*st2)*(1.0-r*dr_U)
        self.dr_tp = 0.0
    
        self.dth_tt = 2.0*r*dth_U/(e2U*(r-2.0))
        self.dth_rr = 2.0*(r-2.0)*e2Ugamma/(r)*(dth_U-dth_gamma)
        self.dth_thth = 2.0*e2Ugamma/(r2)*(dth_U-dth_gamma)
        self.dth_pp = -2.0*e2U/(r2*st2*st)*(ct-st*dth_U)
        self.dth_tp = 0.0
        
        return result
