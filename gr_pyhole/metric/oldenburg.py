# -*- coding: utf-8 -*-

#   Copyright 2015 - 2016 Alexander Wittig, Jai Grover
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# what is imported by "from oldeburg import *"
__all__ = ["Oldenburg"]

import numpy as np
from scipy.interpolate import RectBivariateSpline
from .base import SphericalMetric
from .hr import _HRCommon

class _OldenburgCommon(_HRCommon):
    """Routines common to all Oldenburg metrics. Used as a mix-in to the Oldenburg class."""

    def getRadius(self, R):
        """Get the radius in BL coordinates given a circumferential radius **R**.
        This routine overwrites the one in SphericalMetric and also adds this
        functionality to the Cartesian HR metrics as it is implemented directly
        in terms of the generator functions.

        :param R: The circumferential radius **R** to convert.
        """
        from scipy.optimize import newton
        theta = np.pi/2
        def f(r):
            x = self.r2X(r)
            return np.sqrt(self.f.F2(x, theta))*r-R
        def df(r):
            x = self.r2X(r)
            dx = self.dr_r2X(r)
            f2 = self.f.F2(x, theta)
            df2 = dx*self.f.F2(x, theta, dX=1)
            return np.sqrt(f2)+0.5*r/np.sqrt(f2)*df2
        sol = newton(f, R, df)
        return sol

    def X2r(self, X):
        """Convert reduced radius **X** into BL radius **r**.

        :param X: The reduced radius **X** to convert.
        """
        return (self.EH + X)/(1.0 - X)

    def r2X(self, r):
        """Convert BL radius **r** into reduced radius **X**.

        :param r: The BL radius **r** to convert.
        """
        return (r - self.EH)/(1.0 + r)

    def dr_r2X(self, r):
        """Derivative dX/dr of reduced radius **X** with rrespect to BL radius **r** .

        :param r: The BL radius **r** at which to evaluate the derivative.
        """
        return (1.0 + self.EH)/((1.0 + r)*(1.0 + r))


class Oldenburg(_OldenburgCommon, SphericalMetric):
    ID = 'OB'    #: short identifier of this metric; used e.g. in file names
    CL_CODE = "OB.cl"                   #: Our OpenCL source file

    """Metric in exponential form in both spherical and Cartesian coordinates."""
    def __init__(self, f):
        """Create new exponential form blackhole.

        :param f: An HR function object.
        """
        super(Oldenburg, self).__init__()
        self.EH = f.EH
        self.rCutoff = self.CUTOFF_FACTOR*self.EH
        self.f = f

    def __str__(self):
        """Show human readable summary of the current setup.
        """
        res = 'Metric: Oldenburg\n'
        res = res + 'Event horizon:        {}\n'.format(self.EH)
        res = res + 'Cutoff radius:        {}\n'.format(self.rCutoff)
        res = res + str(self.f) + '\n'
        res += super(Oldenburg,self).__str__();
        return res

    def update(self):
        """Compute the relevant components of the metric at the current point and store them in the attributes.

        :return: An error flag that is True if an error occured (most likely the ray fell into the black hole) or False otherwise.
        """
        r = self.x[1]
        theta = self.x[2]
        # correct various coordinates. Done differently for scalars and vectors.
        if isinstance(r, np.ndarray):
            # correct for non-canonical coordinates. May be necessary for specific cases in flat metric.
            # Note there is no phi dependence, hence phi is not adjusted
            cond = r<0.0
            r[cond] = -r[cond]
            theta[cond] = np.pi-theta[cond]
            # check if fallen in, this is returned later.
            result = r<self.rCutoff
            # normalize theta. Note there is no phi dependence, hence phi is not adjusted
            theta = theta%(2.0*np.pi)
            cond = theta>=np.pi
            theta[cond] = 2.0*np.pi - theta[cond]
            # Hack to prevent divisions by zero: if we are too close to the coordinate singularity enforce minimum size for theta
            cond = theta<1e-12
            theta[cond] = 1e-12
        else:
            # correct for non-canonical coordinates. May be necessary for specific cases in flat metric.
            # Note there is no phi dependence, hence phi is not adjusted
            if r<0.0:
                r = -r
                theta = np.pi-theta
            # check if fallen in
            if r<self.rCutoff: return True
            result = False
            # normalize theta. Note there is no phi dependence, hence phi is not adjusted
            theta = theta%(2.0*np.pi)
            if theta>=np.pi:
                theta = 2.0*np.pi - theta
            # Hack to prevent divisions by zero: if we are too close to the coordinate singularity enforce minimum size for theta
            if theta<1e-12: theta = 1e-12

        # interpolate and take derivatives
        XX = self.r2X(r)       # this is the first index to use for the extrapolation table along with theta
        dr = self.dr_r2X(r)
        f0 = self.f.F0(XX, theta)
        f1 = self.f.F1(XX, theta)
        f2 = self.f.F2(XX, theta)
        omega = -self.f.W( XX, theta)

        dr_f0 = dr*self.f.F0(XX, theta, dX=1, dtheta=0)
        dr_f1 = dr*self.f.F1(XX, theta, dX=1, dtheta=0)
        dr_f2 = dr*self.f.F2(XX, theta, dX=1, dtheta=0)
        dr_omega = -dr*self.f.W( XX, theta, dX=1, dtheta=0)

        dth_f0 = self.f.F0(XX, theta, dX=0, dtheta=1)
        dth_f1 = self.f.F1(XX, theta, dX=0, dtheta=1)
        dth_f2 = self.f.F2(XX, theta, dX=0, dtheta=1)
        dth_omega = -self.f.W( XX, theta, dX=0, dtheta=1)

        # calculate actual contravariant metric
        r2 = r*r
        f02 = f0*f0
        f12 = f1*f1
        f22 = f2*f2
        omega2 = omega*omega

        # some common terms for reuse
        st = np.sin(theta)
        ct = np.cos(theta)
        st2 = st*st

        self.tt = -1.0/f0
        self.rr = 1.0/f1
        self.thth = self.rr/r2
        self.pp = 1.0/(f2*r2*st2) - omega2/f0
        self.tp = omega/f0

        self.dr_tt = dr_f0/f02
        self.dr_rr = -dr_f1/f12
        self.dr_thth = self.dr_rr/r2 - 2.0/(f1*r2*r)
        self.dr_pp = -2.0*omega*dr_omega/f0 + omega2*dr_f0/f02 - dr_f2/(f22*r2*st2) - 2.0/(f2*r*r2*st2)
        self.dr_tp = dr_omega/f0 - omega*dr_f0/f02

        self.dth_tt = dth_f0/f02
        self.dth_rr = -dth_f1/f12
        self.dth_thth = self.dth_rr/r2
        self.dth_pp = -2.0*omega*dth_omega/f0 + omega*omega*dth_f0/f02 - dth_f2/(f22*r2*st2) - 2.0*ct/(f2*r2*st*st2)
        self.dth_tp = dth_omega/f0 - omega*dth_f0/f02

        return result
