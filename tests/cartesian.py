# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

A simple code to view precomputed data files and analyze them.

@author: Alexander Wittig
"""

import unittest, sys, os
# only needed to set up the relative path to local pyhole module if not installed system-wide
# sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole import metric, observer, propagator
from gr_pyhole.image import Image

class TestCartesianMetricsEquirectangular(unittest.TestCase):
    TOLERANCE = 1e-6
    SIZE = (128,128)
    RSKY = 30.0
    DATAFILE = 'configuration-2.npz'
    OBS = observer.Equirectangular(r=15.0, theta=np.pi/2)
    PREFIX = "CRT-EQU-"

    def getPropagator(self, g):
        p = propagator.CartesianCPU(self.OBS, g, Rsky=self.RSKY)
        p.TOLERANCE = self.TOLERANCE
        p.VERBOSE = False   # don't print extra output
        return p

    def runtest(self, g, name):
        p = self.getPropagator(g)
        i = Image(p, self.SIZE)
        i.saveImage(self.PREFIX+name+'.png')
        return True        # no way to test if the image looks right

    def test_Flat(self):
        g = metric.Flat()
        self.assertRaises(ValueError, self.runtest, g, 'flat')      # cartesian propagator with spherical metric

    def test_CFlat(self):
        g = metric.CFlat()
        assert(self.runtest(g, 'cflat'))

    def test_CHR_Flat(self):
        g = metric.CHR(metric.hr.Flat())
        assert(self.runtest(g, 'chr-flat'))

    def test_CHR_Schwarzschild(self):
        g = metric.CHR(metric.hr.Schwarzschild(2.0))
        assert(self.runtest(g, 'chr-schwarzschild'))

    def test_HR_Interpolated(self):
        g = metric.CHR(metric.hr.Interpolated(self.DATAFILE))
        assert(self.runtest(g, 'chr-interpolated'))

class TestSphericalMetricsStereographic(TestCartesianMetricsEquirectangular):
    OBS = observer.Stereographic(r=15.0, theta=np.pi/2)
    PREFIX = "CRT-STR-"

class TestSphericalMetricsGnomonic(TestCartesianMetricsEquirectangular):
    OBS = observer.Gnomonic(r=15.0, theta=np.pi/2)
    PREFIX = "CRT-GNO-"

class GPUTestSphericalMetricsEquirectangularPole(TestCartesianMetricsEquirectangular):
    OBS = observer.Stereographic(r=15.0, theta=0.0)
    PREFIX = "CRT-EQU-POLE-"

if __name__ == "__main__":
    unittest.main()








# Set up a metric
#g = metric.Flat()
#g = metric.CFlat()
#g = metric.Schwarzschild(2.0)
#g = metric.Kerr(2.0, 1.0)
#g = metric.HR(metric.hr.Flat())
#g = metric.HR(metric.hr.Schwarzschild(2.0))
#g = metric.HR(metric.hr.Interpolated('/Users/alex/Documents/ACT/ownCloud/Blackholes/PyHole/datasets/configuration-2.npz'))

# set up an observer
#o = observer.Equirectangular(r=15.0, theta=np.pi/2)

# set up a propagator
#p = propagator.SphericalCPU(o, g, Rsky=30.0)
#p = propagator.SphericalGPU(o, g, Rsky=30.0, device="GPU")
#p = propagator.CartesianCPU(o, g, Rsky=30.0)
#p = propagator.CartesianGPU(o, g, Rsky=30.0)
#p.real = np.float32
#p.TOLERANCE = 1e-6

# generate and save an image
#i = Image(p, (128,128))
#i.saveImage('test10.png')
#print(str(i))
