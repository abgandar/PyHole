#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 6 2016

A simple code to view precomputed data files and analyze them.

@author: Alexander Wittig
"""

import sys
from scene import Scene

if sys.argc<2:
    print("Usage: webviewer.py <datafile.npz>")
    exit()

scene = Scene()
scene.load(sys.argv[1])
scene.show(True)
