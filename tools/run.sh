files=('w=0.681 M=0.9617'
    'w=0.681 M=0.9909'
    'w=0.682 M=0.9566'
    'w=0.682 M=0.9839'
    'w=0.683 M=0.9519'
    'w=0.683 M=0.9776'
    'w=0.684 M=0.9473'
    'w=0.684 M=0.9718')
angles="90"
NP=24
[ "$1" != "" ] && angles="$@"

for f in "${files[@]}"
do
    for theta in $angles
    do
        time python run.py $theta "$f" $NP &>> run.txt
        echo >> run.txt
        echo >> run.txt
        echo >> run.txt
    done
done
