# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

A simple code to precompute data files for later analysis.

@author: Alexander Wittig
"""

# system routines
import sys
# math routines
import numpy as np
# our own routines
from gr_pyhole.scene import Scene

### User settings
#

scene = Scene()

# The following settings specify the data file to generate.
#
# Which metric to use. Can be a fully instantiated metric, a metric name (Flat, Schwarzschild, Kerr, HRKerr, HRSchwarzschild)
# or a data file name for a HR metric
scene.metric = sys.argv[2] if len(sys.argv) > 2 else '5'

# The radius of the observer (BL coordinates) or None for automatic
scene.r = None

# The theta angle of the observer (rad)
scene.theta = np.deg2rad(float(sys.argv[1]) if len(sys.argv)>1 else 90.0)

# image size in pixels
scene.size = (1024,1024)

# Use cartesian coordinates for the propagation
scene.cartesian = False

# The zoom region (image coordinates)
scene.zoom = ((-1.0,1.0),(-1.0,1.0))

# Integration tolerance to use (lower accuracy means faster computation)
#scene.tolerance = scene.HIGH_ACCURACY
scene.tolerance = scene.MEDIUM_ACCURACY
#scene.tolerance = scene.LOW_ACCURACY

# Which projection to use ('S' sterographic, 'G' gnomonic, 'A' Aveiro)
scene.projection = 'Equirectangular'

# Field of view for the camera (rad)
scene.fov = np.arctan(10.0/15.0)

# Sky radius (None for automatic, 0.0 for infinite)
scene.rsky = None

# Custom suffix to be appended to the automatically generated file name (without leading -)
scene.my_suffix = ''

# The following settings for the celestial sphere decoration do not require
# a recomputation of the dataset
#

# Show an Einstein ring of this angular size (rad), or None for no ring
scene.ring = None

# Show a grid of this many lines in theta (and twice that in phi) or None for no grid
scene.grid = 18

# Celestial sphere background file
scene.background = 'bg-color.png'

# Color to use for rays that fell in the black hole
scene.shadowColor = [0.0, 0.0, 0.0, 1.0]

# Color to use for rays that float around the black hole (or None for shadow color)
scene.floaterColor = [1.0, 0.0, 1.0, 1.0]

# Color to use for rays that have an integrator error (or None for shadow color)
scene.errorColor = [0.0, 1.0, 1.0, 1.0]

### Main code
#

#scene.raytrace()
#scene.raytrace_parallel(__name__, NP=(int(sys.argv[3]) if len(sys.argv)>3 else None))
#scene.raytrace_MPI()
scene.raytrace_GPU(device="GPUCPU", fact=15)

# raytrace only saves an image and the data itself, now save also all the other stuff
scene.saveDescription()
scene.saveWebGLTexture()
if scene.c.zoomed():
    scene.saveOverlay()
