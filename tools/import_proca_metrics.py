# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

Import the data files from http://gravitation.web.ua.pt/index.php?q=node/416 for use in the Interpolated metric.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module
import sys, os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole.scene import Scene
import os.path
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import RectBivariateSpline

showPlots = False           #: Show a window with the plots of F0, F1, F2, and W as well as their interpolation

infiles = [
    { 'fn': 'solS11',     'EH': 0.008,     'OmegaH': 0.78,   'w': 0.78, 'm': 1.0, 'm_ADM' : 0.8390,   'L_ADM' : 0.6644,   'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
]


for n in infiles:
    fn = os.path.join(Scene.basePath, Scene.dataPath, 'Aveiro Files', 'Proca', n['fn']+'.dat')
    data = np.loadtxt(fn)
    cols = data.shape[1]
    TH = data[:,1]
    i = 0
    TH0 = TH[0]
    while i<len(TH) and TH[i]==TH0: i = i+1;
    data = data.reshape((len(TH)//i, i, cols))
    X = data[0,:,0]
    TH = data[:,0,1]
    F1 = data[:,:,2].transpose()
    F2 = data[:,:,3].transpose()
    F0 = data[:,:,4].transpose()
    W = data[:,:,5].transpose()
    H1 = data[:,:,6].transpose()
    H2 = data[:,:,7].transpose()
    H3 = data[:,:,8].transpose()
    V = data[:,:,9].transpose()

    # add a mirrored copy of all data to complete the pi/2 - pi interval
    TH = np.concatenate((TH, np.pi-TH[-2::-1]), axis=0)
    F0 = np.concatenate((F0, F0[:,-2::-1]), axis=1)
    F1 = np.concatenate((F1, F1[:,-2::-1]), axis=1)
    F2 = np.concatenate((F2, F2[:,-2::-1]), axis=1)
    W = np.concatenate((W, W[:,-2::-1]), axis=1)
    H1 = np.concatenate((H1, H1[:,-2::-1]), axis=1)
    H2 = np.concatenate((H2, H2[:,-2::-1]), axis=1)
    H3 = np.concatenate((H3, H3[:,-2::-1]), axis=1)
    V = np.concatenate((V, V[:,-2::-1]), axis=1)

    ofn = os.path.join(Scene.basePath, Scene.dataPath, 'Proca_'+n['fn']+'.npz')
    np.savez_compressed(ofn, X=X, TH=TH, F1=F1, F2=F2, F0=F0, H1=H1, H2=H2, H3=H3, W=W, V=V,
                        EH=n['EH'], OmegaH=n['OmegaH'], w=n['w'], m=n['m'], m_ADM=n['m_ADM'], L_ADM=n['L_ADM'], m_BH=n['m_BH'], L_BH=n['L_BH'], m_SF=n['m_SF'], L_SF=n['L_SF'])

    IF0 = RectBivariateSpline(X, TH, F0, kx=2, ky=2);
    IF1 = RectBivariateSpline(X, TH, F1, kx=2, ky=2);
    IF2 = RectBivariateSpline(X, TH, F2, kx=2, ky=2);
    IW = RectBivariateSpline(X, TH, W, kx=2, ky=2);

    mX, mTH = np.meshgrid(X, TH, indexing='ij')
    fig = pyplot.figure()
    fig.suptitle(n['fn'])
    ax = fig.add_subplot(2, 2, 1, projection='3d')
    ax.set_title('F0')
    ax.plot_surface(mX, mTH, F0, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 2, projection='3d')
    ax.set_title('F1')
    ax.plot_surface(mX, mTH, F1, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 3, projection='3d')
    ax.set_title('F2')
    ax.plot_surface(mX, mTH, F2, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 4, projection='3d')
    ax.set_title('W')
    ax.plot_surface(mX, mTH, W, rstride = 1, cstride = 1, linewidths=0.2)
    fig.savefig(os.path.join(Scene.basePath, Scene.dataPath, 'Proca_'+n['fn']+'.png'), dpi=500)

    if not showPlots:
        pyplot.close(fig)
    else:
        iX = np.linspace(0.0, 1.0, 100)
        iTH = np.linspace(0.0, np.pi/2, 100)
        mX, mTH = np.meshgrid(iX, iTH, indexing='ij')
        fig = pyplot.figure()
        fig.suptitle(n['fn']+' (Interpolation)')
        ax = fig.add_subplot(2, 2, 1, projection='3d')
        ax.set_title('F0')
        ax.plot_surface(mX, mTH, IF0(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 2, projection='3d')
        ax.set_title('F1')
        ax.plot_surface(mX, mTH, IF1(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 3, projection='3d')
        ax.set_title('F2')
        ax.plot_surface(mX, mTH, IF2(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 4, projection='3d')
        ax.set_title('W')
        ax.plot_surface(mX, mTH, IW(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
