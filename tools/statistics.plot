set term pdf
set logscale y
set xlabel 'Wall clock time [h]'
set ylabel 'Number of active rays'
plot 'statistics-GPU-5.dat' u ($2/60/60):3 w l title 'GPU', 'statistics-CPU-5.dat' u ($2/60/60):3 w l title 'CPU', 'statistics-GPUCPU-5.dat' u ($2/60/60):3 w l title 'GPU\&CPU'
pause -1
