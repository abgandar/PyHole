REM script to install PyOpenCL drivers on windows. Requires Visual Studio and AMD APP installed
REM AMD APP must be added to the include and library path for installation to succeed.

REM "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\VC\Auxiliary\Build\vcvarsall.bat"

set include=%include%C:\Program Files (x86)\AMD APP SDK\3.0\include;
set lib=%lib%C:\Program Files (x86)\AMD APP SDK\3.0\lib\x86_64;

pip uninstall pyopencl
pip install --upgrade --no-cache-dir pyopencl
