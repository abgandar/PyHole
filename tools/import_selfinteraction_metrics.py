# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

Import the data files from http://gravitation.web.ua.pt/index.php?q=node/416 for use in the Interpolated metric.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module
import sys, os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole.scene import Scene
import os.path
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import RectBivariateSpline

generatePlots = False       #: Generate any plots at all?
showPlots = False           #: Show generated plots in a window with the plots of F0, F1, F2, and W as well as their interpolation

infiles = [
#{ 'fn': 'C:\\Users\\alexander wittig\\ownCloud\\Blackholes\\PyHole\\datasets\\Aveiro Files\\self-interaction\\BS\\spinning\\500\\2nd\\0,86.dat'}
]

# add all command line arguments as files to convert
for f in sys.argv[1:]:
    infiles.append({ 'fn' : f })


for n in infiles:
    fn = n['fn']
    if not os.path.isfile(fn):
        fn = os.path.join(Scene.basePath, Scene.dataPath, 'Aveiro Files', 'self-interaction', n['fn'])
    data = np.loadtxt(fn)
    cols = data.shape[1]
    TH = data[:,1]
    i = 0
    TH0 = TH[0]
    while i<len(TH) and TH[i]==TH0: i = i+1;
    data = data.reshape((len(TH)//i, i, cols))
    X = data[0,:,0]
    TH = data[:,0,1]
    F1 = data[:,:,2].transpose()
    F2 = data[:,:,3].transpose()
    F0 = data[:,:,4].transpose()
    if cols==6:
        PHI = F1*0
        W = data[:,:,5].transpose()
    else:
        PHI = data[:,:,5].transpose()
        W = data[:,:,6].transpose()

    # add a mirrored copy of all data to complete the pi/2 - pi interval
    TH = np.concatenate((TH, np.pi-TH[-2::-1]), axis=0)
    F0 = np.concatenate((F0, F0[:,-2::-1]), axis=1)
    F1 = np.concatenate((F1, F1[:,-2::-1]), axis=1)
    F2 = np.concatenate((F2, F2[:,-2::-1]), axis=1)
    PHI = np.concatenate((PHI, PHI[:,-2::-1]), axis=1)
    W = np.concatenate((W, W[:,-2::-1]), axis=1)

    # extract data from file name and global data file
    s = os.path.basename(fn)[:-4].replace(',','.')
    num = float(s)
    globfn = os.path.join(os.path.dirname(fn), 'global-data.txt')
    with open(globfn) as globf:
        globlines = globf.readlines()
    vals = []
    for l in globlines:
        vals = l.strip("{} \t\n\r").split(',')
        if len(vals) == 4 and abs(float(vals[1])-num)<1e-7*num:         # num from file name is w
            break;
        elif len(vals) == 7 and abs(float(vals[0])-num)<1e-7*num:       # num from file name is lambda
            break;
    if len(vals) == 4:
        # boson star
        n['lambda'] = float(vals[0])
        n['EH'] = 0.0
        n['w'] = float(vals[1])
        n['m_ADM'] = float(vals[2])
        n['L_ADM'] = float(vals[3])
        n['m_BH'] = 0.0
        n['L_BH'] = 0.0
        n['m_SF'] = float(vals[2])
        n['L_SF'] = float(vals[3])
        # generate data file name from directories (e.g. BS-spinning-500-1st-0,85)
        p, d = os.path.split(fn)
        nfn = d[:-4]
        p, d = os.path.split(p)
        nfn = d+'-'+nfn
        p, d = os.path.split(p)
        nfn = d+'-'+nfn
        p, d = os.path.split(p)
        nfn = d+'-'+nfn
        p, d = os.path.split(p)
        nfn = d+'-'+nfn
    elif len(vals) == 7:
        # black hole
        n['lambda'] = float(vals[0])
        n['EH'] =  float(vals[2])
        n['OmegaH'] = 0.0
        n['w'] = float(vals[1])
        n['m_ADM'] = float(vals[3])
        n['L_ADM'] = float(vals[4])
        n['m_BH'] =  float(vals[5])
        n['L_BH'] =  float(vals[6])
        n['m_SF'] = n['m_ADM']-n['m_BH']
        n['L_SF'] = n['L_ADM']-n['L_BH']
        # generate data file name from directories and read mass (e.g. BH-w=0,9-rh=0,1-m=3.542-500)
        p, d = os.path.split(fn)
        nfn = d[:-4]
        p, d = os.path.split(p)
        nfn = d+'-M={:.3}-'.format(n['m_ADM'])+nfn
        p, d = os.path.split(p)
        nfn = d+'-'+nfn
    else:
        print("Datafile not found in global data file")
        continue;

    ofn = os.path.join(Scene.basePath, Scene.dataPath, nfn+'.npz')
    np.savez_compressed(ofn, X=X, TH=TH, F1=F1, F2=F2, F0=F0, PHI=PHI, W=W, **n)

    if generatePlots:
        IF0 = RectBivariateSpline(X, TH, F0, kx=2, ky=2);
        IF1 = RectBivariateSpline(X, TH, F1, kx=2, ky=2);
        IF2 = RectBivariateSpline(X, TH, F2, kx=2, ky=2);
        IW = RectBivariateSpline(X, TH, W, kx=2, ky=2);

        mX, mTH = np.meshgrid(X, TH, indexing='ij')
        fig = pyplot.figure()
        fig.suptitle(n['fn'])
        ax = fig.add_subplot(2, 2, 1, projection='3d')
        ax.set_title('F0')
        ax.plot_surface(mX, mTH, F0, rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 2, projection='3d')
        ax.set_title('F1')
        ax.plot_surface(mX, mTH, F1, rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 3, projection='3d')
        ax.set_title('F2')
        ax.plot_surface(mX, mTH, F2, rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 4, projection='3d')
        ax.set_title('W')
        ax.plot_surface(mX, mTH, W, rstride = 1, cstride = 1, linewidths=0.2)
        fig.savefig(os.path.join(Scene.basePath, Scene.dataPath, nfn+'.png'), dpi=500)

        if not showPlots:
            pyplot.close(fig)
        else:
            iX = np.linspace(0.0, 1.0, 100)
            iTH = np.linspace(0.0, np.pi/2, 100)
            mX, mTH = np.meshgrid(iX, iTH, indexing='ij')
            fig = pyplot.figure()
            fig.suptitle(n['fn']+' (Interpolation)')
            ax = fig.add_subplot(2, 2, 1, projection='3d')
            ax.set_title('F0')
            ax.plot_surface(mX, mTH, IF0(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
            ax = fig.add_subplot(2, 2, 2, projection='3d')
            ax.set_title('F1')
            ax.plot_surface(mX, mTH, IF1(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
            ax = fig.add_subplot(2, 2, 3, projection='3d')
            ax.set_title('F2')
            ax.plot_surface(mX, mTH, IF2(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
            ax = fig.add_subplot(2, 2, 4, projection='3d')
            ax.set_title('W')
            ax.plot_surface(mX, mTH, IW(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
