# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

@author: Alexander Wittig
"""

# math routines
import numpy as np
from math import sqrt
# image generation and plotting routines
import matplotlib.pyplot as pyplot
import os.path

# bright
RED    = (1.0, 0.0, 0.0)
GREEN  = (0.0, 1.0, 0.0)
BLUE   = (0.0, 0.0, 1.0)
YELLOW = (1.0, 1.0, 0.0)

# pastel
RED    = (0.94, 0.5, 0.5)
GREEN  = (0.7, 0.9, 0.5)
BLUE   = (0.39, 0.58, 0.93)
YELLOW = (1.0, 0.98, 0.80)

def generateBackgrounds(size=2048):
    """Create various background images."""
    im = np.ndarray((size,2*size,3), dtype=np.float32)

    # white
    im[:,:,:] = 1.0
    pyplot.imsave('bg-white.png', im)

    # 4 color faces
    im[:,:,:] = 0.0
    im[0:(size//2),0:(size),0] = GREEN[0]
    im[0:(size//2),0:(size),1] = GREEN[1]
    im[0:(size//2),0:(size),2] = GREEN[2]
    im[0:(size//2),(size):2*size,0] = RED[0]
    im[0:(size//2),(size):2*size,1] = RED[1]
    im[0:(size//2),(size):2*size,2] = RED[2]
    im[(size//2):size,0:(size),0] = BLUE[0]
    im[(size//2):size,0:(size),1] = BLUE[1]
    im[(size//2):size,0:(size),2] = BLUE[2]
    im[(size//2):size,(size):2*size,0] = YELLOW[0]
    im[(size//2):size,(size):2*size,1] = YELLOW[1]
    im[(size//2):size,(size):2*size,2] = YELLOW[2]
    pyplot.imsave('bg-color.png', im)

    # 4 color faces smoothly merged
    bw = 30
    tl = np.array(GREEN)
    tr = np.array(RED)
    bl = np.array(BLUE)
    br = np.array(YELLOW)

    def col(i,j):
        di = i-size//2
        dj = j-size
        fi = abs(di)/bw
        fj = abs(dj)/(2*bw)
        if di>=0 and dj>=0:
            wtl = sqrt(fi*fi + fj*fj)
            wtr = fi
            wbl = fj
            wbr = 0.0
        elif di<0 and dj<0:
            wtl = 0.0
            wtr = fj
            wbl = fi
            wbr = sqrt(fi*fi + fj*fj)
        elif di<0 and dj>=0:
            wtl = fj
            wtr = 0.0
            wbl = sqrt(fi*fi + fj*fj)
            wbr = fi
        elif di>=0 and dj<0:
            wtl = fi
            wtr = sqrt(fi*fi + fj*fj)
            wbl = 0.0
            wbr = fj
        wtl = max(0.0, 1.0-wtl)
        wtr = max(0.0, 1.0-wtr)
        wbl = max(0.0, 1.0-wbl)
        wbr = max(0.0, 1.0-wbr)
        return (wtl*tl+wtr*tr+wbl*bl+wbr*br)/(wtl+wtr+wbl+wbr)

    im[:,:,:] = 0.0
    im[0:(size//2),0:(size),0] = GREEN[0]
    im[0:(size//2),0:(size),1] = GREEN[1]
    im[0:(size//2),0:(size),2] = GREEN[2]
    im[0:(size//2),(size):2*size,0] = RED[0]
    im[0:(size//2),(size):2*size,1] = RED[1]
    im[0:(size//2),(size):2*size,2] = RED[2]
    im[(size//2):size,0:(size),0] = BLUE[0]
    im[(size//2):size,0:(size),1] = BLUE[1]
    im[(size//2):size,0:(size),2] = BLUE[2]
    im[(size//2):size,(size):2*size,0] = YELLOW[0]
    im[(size//2):size,(size):2*size,1] = YELLOW[1]
    im[(size//2):size,(size):2*size,2] = YELLOW[2]
    for i in range(size//2-bw,size//2+bw):
        for j in range(0,2*size):
            im[i,j,:] = col(i,j)
    for i in range(0,size):
        for j in range(size-2*bw,size+2*bw):
            im[i,j,:] = col(i,j)
    pyplot.imsave('bg-color-smooth.png', im)

    # 4 color points smoothly interpolated by distance
    colors = ( np.array(GREEN), np.array(RED), np.array(BLUE), np.array(YELLOW) )

    def col2(i,j):
        ii = i/size
        jj = j/(2*size)
        res = np.array([0,0,0])
        ind = 0
        s = 0.0
        for x in (0.25,0.75):
            for y in (0.25,0.75):
                di = abs(ii-x)
                if di>0.5: di = 1.0-di
                dj = abs(jj-y)
                if dj>0.5: dj = 1.0-dj
                r = max(di*di+dj*dj,0.001)
                f = 1/(r*r)
                s = s+f
                res = res+f*colors[ind]
                ind = ind+1
        return res/s

    im[:,:,:] = 0.0
    for i in range(0,size):
        for j in range(0,2*size):
            im[i,j,:] = col2(i,j)
    pyplot.imsave('bg-color-smooth2.png', im)

    # 4 color faces with einstein ring
    s = int(2.5*max(8, size/64))
    for x in range(size//2-s, size//2+s):
        for y in range(size-s, size+s):
            fact = min(1.0,((x-size//2.0)**2+(y-size)**2)/(s**2))**2
            im[x, y, :] = (1.0-fact)*1.0 + fact*im[x,y,:]
    pyplot.imsave('bg-color-er.png', im)

    # 4 color faces with einstein ring and grid
    w = size//1024+1
    lines = 18
    for i in range(lines+1):
        x = i*size//lines
        im[x-w:x+w,0:2*size,:] = 0.2
    for i in range(2*lines+1):
        x = i*2*size//(2*lines)
        im[0:size,x-w:x+w,:] = 0.2
    im[size-w:size,0:2*size,:] = 0.2
    im[0:w,0:2*size,:] = 0.2
    im[0:size,2*size-w:2*size,:] = 0.2
    im[0:size,0:w,:] = 0.2
    pyplot.imsave('bg-colorgrid-er.png', im)

    # 4 color faces with grid
    im[:,:,:] = 0.0
    im[0:(size//2),0:(size),0] = GREEN[0]
    im[0:(size//2),0:(size),1] = GREEN[1]
    im[0:(size//2),0:(size),2] = GREEN[2]
    im[0:(size//2),(size):2*size,0] = RED[0]
    im[0:(size//2),(size):2*size,1] = RED[1]
    im[0:(size//2),(size):2*size,2] = RED[2]
    im[(size//2):size,0:(size),0] = BLUE[0]
    im[(size//2):size,0:(size),1] = BLUE[1]
    im[(size//2):size,0:(size),2] = BLUE[2]
    im[(size//2):size,(size):2*size,0] = YELLOW[0]
    im[(size//2):size,(size):2*size,1] = YELLOW[1]
    im[(size//2):size,(size):2*size,2] = YELLOW[2]
    w = size//1024+1
    lines = 18
    for i in range(lines+1):
        x = i*size//lines
        im[x-w:x+w,0:2*size,:] = 0.2
    for i in range(2*lines+1):
        x = i*2*size//(2*lines)
        im[0:size,x-w:x+w,:] = 0.2
    im[size-w:size,0:2*size,:] = 0.2
    im[0:w,0:2*size,:] = 0.2
    im[0:size,2*size-w:2*size,:] = 0.2
    im[0:size,0:w,:] = 0.2
    pyplot.imsave('bg-colorgrid.png', im)

    # Portugal style
    im[:,:,:] = 0.0
    im[0:(size//2),0:(size),0] = GREEN[0]
    im[0:(size//2),0:(size),1] = GREEN[1]
    im[0:(size//2),0:(size),2] = GREEN[2]
    im[0:(size//2),(size):2*size,0] = RED[0]
    im[0:(size//2),(size):2*size,1] = RED[1]
    im[0:(size//2),(size):2*size,2] = RED[2]
    im[(size//2):size,0:(size),0] = BLUE[0]
    im[(size//2):size,0:(size),1] = BLUE[1]
    im[(size//2):size,0:(size),2] = BLUE[2]
    im[(size//2):size,(size):2*size,0] = YELLOW[0]
    im[(size//2):size,(size):2*size,1] = YELLOW[1]
    im[(size//2):size,(size):2*size,2] = YELLOW[2]
    s = int(5.0*max(8, size/64))
    for x in range(size//2-s, size//2+s):
        for y in range(size-s, size+s):
            fact = min(1.0,((x-size//2.0)**2+(y-size)**2)/(s**2))**2
            im[x, y, :] = (1.0-fact)*1.0 + fact*im[x,y,:]
    w = 5
    lines = 18
    for i in range(lines+1):
        x = i*size//lines
        im[x-w:x+w,0:2*size,:] = 0.0
    for i in range(2*lines+1):
        x = i*2*size//(2*lines)
        im[0:size,x-w:x+w,:] = 0.0
    im[size-w:size,0:2*size,:] = 0.0
    im[0:w,0:2*size,:] = 0.0
    im[0:size,2*size-w:2*size,:] = 0.0
    im[0:size,0:w,:] = 0.0
    pyplot.imsave('bg-colorgrid-erP.png', im)

generateBackgrounds()
