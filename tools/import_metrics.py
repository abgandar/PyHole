# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

Import the data files from http://gravitation.web.ua.pt/index.php?q=node/416 for use in the Interpolated metric.

@author: Alexander Wittig
"""

import numpy as np
from gr_pyhole.scene import Scene
import os.path
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import RectBivariateSpline

showPlots = False           #: Show a window with the plots of F0, F1, F2, and W as well as their interpolation

infiles = [
#    { 'fn': 'configuration-I',    'EH': 0.0,       'OmegaH': 0.0,     'w': 0.85,  'm': 1.0, 'm_ADM' : 1.25,     'L_ADM' : 1.30,     'm_BH' : 0.0,    'L_BH' : 0.0,    'm_SF' : 1.25,     'L_SF' : 1.30     },
#    { 'fn': 'configuration-II',   'EH': 0.0662902, 'OmegaH': 1.11118, 'w': 0.0,   'm': 0.0, 'm_ADM' : 0.415,    'L_ADM' : 0.172,    'm_BH' : 0.415,  'L_BH' : 0.172,  'm_SF' : 0.0,      'L_SF' : 0.0      },
#    { 'fn': 'configuration-III',  'EH': 0.2,       'OmegaH': 0.975,   'w': 0.975, 'm': 1.0, 'm_ADM' : 0.415,    'L_ADM' : 0.172,    'm_BH' : 0.393,  'L_BH' : 0.15,   'm_SF' : 0.022,    'L_SF' : 0.022    },
#    { 'fn': 'configuration-IV',   'EH': 0.1,       'OmegaH': 0.82,    'w': 0.82,  'm': 1.0, 'm_ADM' : 0.933,    'L_ADM' : 0.739,    'm_BH' : 0.234,  'L_BH' : 0.15,   'm_SF' : 0.699,    'L_SF' : 0.625    },
#    { 'fn': 'configuration-SLRNOERG', 'EH': 0.048,     'OmegaH': 0.72,    'w': 0.72,  'm': 1.0, 'm_ADM' : 0.870936, 'L_ADM' : 0.689504, 'm_BH' : 0.032253812, 'L_BH' : 0.0101641765, 'm_SF' : 0.838424754, 'L_SF' : 0.6798374698 },
#    { 'fn': 'configuration-IV.V', 'EH': 0.075,     'OmegaH': 0.72,    'w': 0.72,  'm': 1.0, 'm_ADM' : 0.954544, 'L_ADM' : 0.799248, 'm_BH' : 0.0567641, 'L_BH' : 0.018674, 'm_SF' : 0.8977799, 'L_SF' : 0.780574 },
#    { 'fn': 'configuration-V',    'EH': 0.04,      'OmegaH': 0.68,    'w': 0.68,  'm': 1.0, 'm_ADM' : 0.975,    'L_ADM' : 0.85,     'm_BH' : 0.018,  'L_BH' : 0.849,  'm_SF' : 0.957,    'L_SF' : 0.001    },
#    { 'fn': 'configuration-VI',   'EH': 0.0,       'OmegaH': 0.0,     'w': 0.95,  'm': 1.0, 'm_ADM' : 0.864328, 'L_ADM' : 0.878201, 'm_BH' : 0.0,    'L_BH' : 0.0,    'm_SF' : 0.864328, 'L_SF' : 0.878201 },
#    { 'fn': 'configuration-VII',  'EH': 0.0,       'OmegaH': 0.0,     'w': 0.85,  'm': 1.0, 'm_ADM' : 1.2505,   'L_ADM' : 1.30272,  'm_BH' : 0.0,    'L_BH' : 0.0,    'm_SF' : 1.2505,   'L_SF' : 1.30272  },
#    { 'fn': 'configuration-VIII', 'EH': 0.0,       'OmegaH': 0.0,     'w': 0.80,  'm': 1.0, 'm_ADM' : 1.30782,  'L_ADM' : 1.3719,   'm_BH' : 0.0,    'L_BH' : 0.0,    'm_SF' : 1.30782,  'L_SF' : 1.3719   },
#    { 'fn': 'configuration-IX',   'EH': 0.0,       'OmegaH': 0.0,     'w': 0.75,  'm': 1.0, 'm_ADM' : 1.31021,  'L_ADM' : 1.3747,   'm_BH' : 0.0,    'L_BH' : 0.0,    'm_SF' : 1.31021,  'L_SF' : 1.3747   },
#    { 'fn': 'configuration-X',    'EH': 0.0,       'OmegaH': 0.0,     'w': 0.70,  'm': 1.0, 'm_ADM' : 1.26074,  'L_ADM' : 1.30604,  'm_BH' : 0.0,    'L_BH' : 0.0,    'm_SF' : 1.26074,  'L_SF' : 1.30604  },
#    { 'fn': 'configuration-XI',   'EH': 0.0,       'OmegaH': 0.0,     'w': 0.65,  'm': 1.0, 'm_ADM' : 1.10913,  'L_ADM' : 1.07945,  'm_BH' : 0.0,    'L_BH' : 0.0,    'm_SF' : 1.10913,  'L_SF' : 1.07945  },
#    { 'fn': 'configuration-XII',  'EH': 0.0,       'OmegaH': 0.0,     'w': 0.70,  'm': 1.0, 'm_ADM' : 0.810523, 'L_ADM' : 0.626533, 'm_BH' : 0.0,    'L_BH' : 0.0,    'm_SF' : 0.810523, 'L_SF' : 0.626533 },
#    { 'fn': 'w=0.660 M=0.966',     'EH': 0.013,     'OmegaH': 0.66,    'w': 0.66,  'm': 1.0, 'm_ADM' : 0.966,    'L_ADM' : 0.851,    'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
#    { 'fn': 'w=0.670 M=0.971',     'EH': 0.026,     'OmegaH': 0.67,    'w': 0.67,  'm': 1.0, 'm_ADM' : 0.971,    'L_ADM' : 0.851,    'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
#    { 'fn': 'w=0.675 M=0.968',     'EH': 0.032,     'OmegaH': 0.675,   'w': 0.675, 'm': 1.0, 'm_ADM' : 0.968,    'L_ADM' : 0.844,    'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
#    { 'fn': 'w=0.680 M=0.967',     'EH': 0.038,     'OmegaH': 0.68,    'w': 0.68,  'm': 1.0, 'm_ADM' : 0.967,    'L_ADM' : 0.838,    'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
#    { 'fn': 'w=0.685 M=0.966',     'EH': 0.044,     'OmegaH': 0.685,   'w': 0.685, 'm': 1.0, 'm_ADM' : 0.966,    'L_ADM' : 0.834,    'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
#    { 'fn': 'w=0.690 M=0.967',     'EH': 0.050,     'OmegaH': 0.69,    'w': 0.69,  'm': 1.0, 'm_ADM' : 0.967,    'L_ADM' : 0.832,    'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
    { 'fn': 'w=0.681 M=0.9617',     'EH': 0.038,     'OmegaH': 0.681,   'w': 0.681, 'm': 1.0, 'm_ADM' : 0.9617,   'L_ADM' : 0.8306,   'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
    { 'fn': 'w=0.681 M=0.9909',     'EH': 0.044,     'OmegaH': 0.681,   'w': 0.681, 'm': 1.0, 'm_ADM' : 0.9909,   'L_ADM' : 0.8718,   'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
    { 'fn': 'w=0.682 M=0.9566',     'EH': 0.038,     'OmegaH': 0.682,   'w': 0.682, 'm': 1.0, 'm_ADM' : 0.9566,   'L_ADM' : 0.8229,   'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
    { 'fn': 'w=0.682 M=0.9839',     'EH': 0.044,     'OmegaH': 0.682,   'w': 0.682, 'm': 1.0, 'm_ADM' : 0.9839,   'L_ADM' : 0.86122,  'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
    { 'fn': 'w=0.683 M=0.9519',     'EH': 0.038,     'OmegaH': 0.683,   'w': 0.683, 'm': 1.0, 'm_ADM' : 0.9519,   'L_ADM' : 0.81573,  'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
    { 'fn': 'w=0.683 M=0.9776',     'EH': 0.044,     'OmegaH': 0.683,   'w': 0.683, 'm': 1.0, 'm_ADM' : 0.9776,   'L_ADM' : 0.8516,   'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
    { 'fn': 'w=0.684 M=0.9473',     'EH': 0.038,     'OmegaH': 0.684,   'w': 0.684, 'm': 1.0, 'm_ADM' : 0.9473,   'L_ADM' : 0.8089,   'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
    { 'fn': 'w=0.684 M=0.9718',     'EH': 0.044,     'OmegaH': 0.684,   'w': 0.684, 'm': 1.0, 'm_ADM' : 0.9718,   'L_ADM' : 0.8428,   'm_BH' : np.nan, 'L_BH' : np.nan, 'm_SF' : np.nan,   'L_SF' : np.nan },
]


for n in infiles:
    fn = os.path.join(Scene.basePath, Scene.dataPath, n['fn']+'.dat')
    data = np.loadtxt(fn)
    cols = data.shape[1]
    TH = data[:,1]
    i = 0
    TH0 = TH[0]
    while i<len(TH) and TH[i]==TH0: i = i+1;
    data = data.reshape((len(TH)//i, i, cols))
    X = data[0,:,0]
    TH = data[:,0,1]
    F1 = data[:,:,2].transpose()
    F2 = data[:,:,3].transpose()
    F0 = data[:,:,4].transpose()
    if cols==6:
        PHI = F1*0
        W = data[:,:,5].transpose()
    else:
        PHI = data[:,:,5].transpose()
        W = data[:,:,6].transpose()

    # add a mirrored copy of all data to complete the pi/2 - pi interval
    TH = np.concatenate((TH, np.pi-TH[-2::-1]), axis=0)
    F0 = np.concatenate((F0, F0[:,-2::-1]), axis=1)
    F1 = np.concatenate((F1, F1[:,-2::-1]), axis=1)
    F2 = np.concatenate((F2, F2[:,-2::-1]), axis=1)
    PHI = np.concatenate((PHI, PHI[:,-2::-1]), axis=1)
    W = np.concatenate((W, W[:,-2::-1]), axis=1)

    ofn = os.path.join(Scene.basePath, Scene.dataPath, n['fn']+'.npz')
    np.savez_compressed(ofn, X=X, TH=TH, F1=F1, F2=F2, F0=F0, PHI=PHI, W=W,
                        EH=n['EH'], OmegaH=n['OmegaH'], w=n['w'], m=n['m'], m_ADM=n['m_ADM'], L_ADM=n['L_ADM'], m_BH=n['m_BH'], L_BH=n['L_BH'], m_SF=n['m_SF'], L_SF=n['L_SF'])

    IF0 = RectBivariateSpline(X, TH, F0, kx=2, ky=2);
    IF1 = RectBivariateSpline(X, TH, F1, kx=2, ky=2);
    IF2 = RectBivariateSpline(X, TH, F2, kx=2, ky=2);
    IW = RectBivariateSpline(X, TH, W, kx=2, ky=2);

    mX, mTH = np.meshgrid(X, TH, indexing='ij')
    fig = pyplot.figure()
    fig.suptitle(n['fn'])
    ax = fig.add_subplot(2, 2, 1, projection='3d')
    ax.set_title('F0')
    ax.plot_surface(mX, mTH, F0, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 2, projection='3d')
    ax.set_title('F1')
    ax.plot_surface(mX, mTH, F1, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 3, projection='3d')
    ax.set_title('F2')
    ax.plot_surface(mX, mTH, F2, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 4, projection='3d')
    ax.set_title('W')
    ax.plot_surface(mX, mTH, W, rstride = 1, cstride = 1, linewidths=0.2)
    fig.savefig(os.path.join(Scene.basePath, Scene.dataPath, n['fn']+'.png'), dpi=500)

    if not showPlots:
        pyplot.close(fig)
    else:
        iX = np.linspace(0.0, 1.0, 100)
        iTH = np.linspace(0.0, np.pi/2, 100)
        mX, mTH = np.meshgrid(iX, iTH, indexing='ij')
        fig = pyplot.figure()
        fig.suptitle(n['fn']+' (Interpolation)')
        ax = fig.add_subplot(2, 2, 1, projection='3d')
        ax.set_title('F0')
        ax.plot_surface(mX, mTH, IF0(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 2, projection='3d')
        ax.set_title('F1')
        ax.plot_surface(mX, mTH, IF1(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 3, projection='3d')
        ax.set_title('F2')
        ax.plot_surface(mX, mTH, IF2(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 4, projection='3d')
        ax.set_title('W')
        ax.plot_surface(mX, mTH, IW(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
