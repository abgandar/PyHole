# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

Import the data files from http://gravitation.web.ua.pt/index.php?q=node/416 for use in the Interpolated metric.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole.scene import Scene
import os.path
import re
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import RectBivariateSpline

showPlots = False           #: Show a window with the plots of F0, F1, F2, and W as well as their interpolation

# hard coded list of input files
infiles = [
#    { 'fn': 'met_al0.05_oh0.15_a0.1500_91x51' },
]

# also add all command line arguments as files to convert
for f in sys.argv[1:]:
    infiles.append({ 'fn' : f })

for n in infiles:
    fn = n['fn']
    if not os.path.isfile(fn):
        fn = os.path.join(Scene.basePath, Scene.dataPath, 'Oldenburg', n['fn'])
    data = np.loadtxt(fn)
    cols = data.shape[1]
    TH = data[:,1]
    i = 0
    TH0 = TH[0]
    while i<len(TH) and TH[i]==TH0: i = i+1;
    data = data.reshape((len(TH)//i, i, cols))      # now data is an array indexed by [theta, x, column]
    X = data[0,:,0]
    TH = data[:,0,1]
    F0 = data[:,:,2].transpose()
    F1 = data[:,:,3].transpose()
    F2 = data[:,:,4].transpose()
    W = data[:,:,5].transpose()
    PHI = data[:,:,6].transpose()

    # add a mirrored copy of all data to complete the pi/2 - pi interval
    TH = np.concatenate((TH, np.pi-TH[-2::-1]), axis=0)
    F0 = np.concatenate((F0, F0[:,-2::-1]), axis=1)
    F1 = np.concatenate((F1, F1[:,-2::-1]), axis=1)
    F2 = np.concatenate((F2, F2[:,-2::-1]), axis=1)
    PHI = np.concatenate((PHI, PHI[:,-2::-1]), axis=1)
    W = np.concatenate((W, W[:,-2::-1]), axis=1)

    # extract data from file name and compute event horizon
    fn = os.path.basename(fn)
    match = re.search("[0-9_]al([0-9.]+)", fn)
    if match: n['alpha'] = float(match.group(1))
    match = re.search("[0-9_]oh([0-9.]+)", fn)
    if match: n['OmegaH'] = float(match.group(1))
    match = re.search("[0-9_]a([0-9.]+)", fn)
    if match: n['a'] = float(match.group(1))
    match = re.search("[0-9_]m([0-9.]+)", fn)
    if match: n['m_ADM'] = float(match.group(1))
    match = re.search("[0-9_]j([0-9.]+)", fn)
    if match: n['L_ADM'] = float(match.group(1))
    n['EH'] = (1.0 - 2.0*n['a']*n['OmegaH'])*np.sqrt(n['a']/(16.0*n['OmegaH']*(1.0 - n['a']*n['OmegaH'])))

    ofn = os.path.join(Scene.basePath, Scene.dataPath, os.path.basename(n['fn'])+'.npz')
    np.savez_compressed(ofn, X=X, TH=TH, F1=F1, F2=F2, F0=F0, PHI=PHI, W=W, **n)

    IF0 = RectBivariateSpline(X, TH, F0, kx=2, ky=2);
    IF1 = RectBivariateSpline(X, TH, F1, kx=2, ky=2);
    IF2 = RectBivariateSpline(X, TH, F2, kx=2, ky=2);
    IW = RectBivariateSpline(X, TH, W, kx=2, ky=2);

    mX, mTH = np.meshgrid(X, TH, indexing='ij')
    fig = pyplot.figure()
    fig.suptitle(os.path.basename(n['fn']))
    ax = fig.add_subplot(2, 2, 1, projection='3d')
    ax.set_title('F0')
    ax.plot_surface(mX, mTH, F0, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 2, projection='3d')
    ax.set_title('F1')
    ax.plot_surface(mX, mTH, F1, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 3, projection='3d')
    ax.set_title('F2')
    ax.plot_surface(mX, mTH, F2, rstride = 1, cstride = 1, linewidths=0.2)
    ax = fig.add_subplot(2, 2, 4, projection='3d')
    ax.set_title('W')
    ax.plot_surface(mX, mTH, W, rstride = 1, cstride = 1, linewidths=0.2)
    fig.savefig(os.path.join(Scene.basePath, Scene.dataPath, os.path.basename(n['fn'])+'.png'), dpi=500)

    if not showPlots:
        pyplot.close(fig)
    else:
        iX = np.linspace(0.0, 1.0, 100)
        iTH = np.linspace(0.0, np.pi/2, 100)
        mX, mTH = np.meshgrid(iX, iTH, indexing='ij')
        fig = pyplot.figure()
        fig.suptitle(os.path.basename(n['fn'])+' (Interpolation)')
        ax = fig.add_subplot(2, 2, 1, projection='3d')
        ax.set_title('F0')
        ax.plot_surface(mX, mTH, IF0(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 2, projection='3d')
        ax.set_title('F1')
        ax.plot_surface(mX, mTH, IF1(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 3, projection='3d')
        ax.set_title('F2')
        ax.plot_surface(mX, mTH, IF2(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
        ax = fig.add_subplot(2, 2, 4, projection='3d')
        ax.set_title('W')
        ax.plot_surface(mX, mTH, IW(iX, iTH), rstride = 1, cstride = 1, linewidths=0.2)
