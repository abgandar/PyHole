# -*- coding: utf-8 -*-
"""
A simple example to compute light ring structure for HR metric files.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole import metric

extra = []

print("dataset\tX\tr\teta\tstability\tE")

for arg in sys.argv[1:]+extra:
    g = metric.HR(metric.hr.Interpolated(arg))
    try:
        lrs = g.findLightrings()
        for lr, stab in lrs:
            print("\"{}\"\t{:.4f}\t{:.4f}\t{:.4f}\t{:.1f}\t{:.1f}".format(arg, g.r2X(lr[1]), lr[1], -lr[7]/lr[4], stab, -lr[4]))
    except:
        print("Error", file=sys.stderr)
        pass
