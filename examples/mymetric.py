# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

A simple example of a user defined metric.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module if not installed system-wide
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole import metric, observer, propagator
from gr_pyhole.image import Image

# our own metric is derived from the existing PyHole Cartesian metric
class MyMetric(metric.CartesianMetric):
    """Flat metric in cartesian coordinates."""
    ID = 'MY'       #: short identifier of this metric; used e.g. in file names
    CL_CODE = 'mymetric.cl'     #: OpenCL code file for this metric, needed for OpenCL

    def __init__(self, a = 1.0, m1 = 1.0, m2 = 1.0):
        """Initialize your metric here by storing various metric parameters in self"""
        self.a = a
        self.m1 = m1
        self.m2 = m2
        self.EH1 = m1*0.05
        self.EH2 = m2*0.05
        super(MyMetric, self).__init__()    # call our super-class initializer to make sure its code, whatever it may be, is run as well

    def __str__(self):
        """Show human readable summary of the current setup."""
        res = 'Metric: My Metric\n'
        res += 'a: {}\n'.format(self.a)
        res += 'm1:  {}        m2: {}\n'.format(self.m1, self.m2)
        res += 'EH1: {}       EH2: {}\n'.format(self.EH1, self.EH2)
        res += super(MyMetric,self).__str__();
        return res

    def cl_allocate(self, context, device, commandQueue, real):
        """Add various constants to the OpenCL compiler flags to pass these
        values to the OpenCL metric code.

        :param context: The PyOpenCL context of the OpenCL computation.
        :param device: The PyOpenCL device used for the OpenCL computation.
        :param commandQueue: The PyOpenCL command queue on the device.
        :param real: The type of "real" numbers (either np.float32 or
        np.float64).
        """
        super(MyMetric,self).cl_allocate(context, device, commandQueue, real)
        self.cl_flags += " -D CONST_A={:.16e} -D CONST_M1={:.16e} -D CONST_M2={:.16e} -D CONST_EH1={:.16e} -D CONST_EH2={:.16e}".format(self.a, self.m1, self.m2, self.EH1, self.EH2)

    def update(self):
        """Compute the relevant components of the Cartesian metric at the current point and store them in the attributes."""

        x, y, z = self.x[1:4]
        r1 = np.sqrt(x*x + y*y + (z-self.a)*(z-self.a))     # Note: always use the numpy function (np.sqrt) instead of the built in (sqrt)!
        r2 = np.sqrt(x*x + y*y + (z+self.a)*(z+self.a))
        f1 = 1.0/r1
        f2 = 1.0/r2
        U = 1.0 + self.m1*f1 + self.m2*f2
        U2 = U*U

        # contravariant form!
        self.tt = -U2
        self.xx = 1.0/U2
        self.yy = self.xx
        self.zz = self.xx

        self.dx_tt = 2.0*U*x*(self.m1*f1*f1*f1+self.m2*f2*f2*f2)
        self.dx_xx = 2.0/(U*U2)*x*(self.m1*f1*f1*f1+self.m2*f2*f2*f2)
        self.dx_yy = self.dx_xx
        self.dx_zz = self.dx_xx

        self.dy_tt = 2.0*U*y*(self.m1*f1*f1*f1+self.m2*f2*f2*f2)
        self.dy_xx = 2.0/(U*U2)*y*(self.m1*f1*f1*f1+self.m2*f2*f2*f2)
        self.dy_yy = self.dy_xx
        self.dy_zz = self.dy_xx

        self.dz_tt = 2.0*U*(self.m1*(z-self.a)*f1*f1*f1+self.m2*(z+self.a)*f2*f2*f2)
        self.dz_xx = 2.0/(U*U2)*(self.m1*(z-self.a)*f1*f1*f1+self.m2*(z+self.a)*f2*f2*f2)
        self.dz_yy = self.dz_xx
        self.dz_zz = self.dz_xx

        return (r1<self.EH1) | (r2<self.EH2)

    def getBlackholes(self):
        """Return a list of the center point (in cartesian display coordinates)
        and radius of all black holes in the metric.
        This information is used to draw the black holes in the interactive display.
        Currently, only spherical black holes are supported. By default, a single
        black hole centered at the origin of the size of the event horizon EH
        is returned."""
        # our special metric has two black holes, so we have to overwrite the default
        # function and return our own black holes to draw. The return value is a list of
        # center points and radii for the black holes.
        # This information is only used for illustration purposes
        return [((0.0, 0.0, self.a), self.EH1), ((0.0, 0.0, -self.a), self.EH2)]


# 1) Set up an instance of our new metric
g = MyMetric(a=2.0, m1=1.0, m2=1.0)       # The arguments here are what is passed to __init__ defined above

# 2) set up an observer
#o = observer.Equirectangular(r=15.0, theta=np.pi/2)
o = observer.Equirectangular(r=15.0, theta=0.0)

# 3) set up a Cartesian propagator
p = propagator.CartesianCPU(o, g, Rsky=30.0)
#p = propagator.CartesianGPU(o, g, Rsky=30.0, device='CPU')
#p.PLATFORM_ID = 1
p.TOLERANCE = 1e-6      # the propagator error tolerance. Around 1e-6 is fast, around 1e-10 is accurate.

# Python trick: by using this construct, you can prevent the following code from running when the file
# is imported into another python source file. That allows using the above metric also in other code without
# always running the full propagation.
# The variables defined above, however, are still available so you can make sure you always have a consistent
# setup of the metrics between different source files.
if __name__ == "__main__":
    # This is only executed when the file is run as a program, not when it is imported as a module

    # 4) generate and save an image
    i = Image(p, (128,128))         # (128,128) is the image size in pixels.
    i.save('mymetric.npz')          # save the raw data from the image to a file so it doesn't have to be recomputed later

    # 5) produce and save a standard black hole lensing image
    i.updateBackground(bgfile='data/backgrounds/bg-color.png', lines=18)        # set a new background image on the celestial sphere with 18 lines per 180 degrees and the given background image
    i.saveImage('mymetric.png')      # save the resulting image
