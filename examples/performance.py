# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

Set up a simple scene and run different ray tracing code to compare performance.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module if not installed system-wide
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))



from gr_pyhole.scene import Scene

scene = Scene()
scene.metric = '2'
#scene.metric = '5'
#scene.metric = '12'
scene.tolerance = scene.MEDIUM_ACCURACY
#scene.tolerance = scene.LOW_ACCURACY
scene.size = (1024,1024)
#scene.size = (64,64)


scene.my_suffix = 'PY'
scene.raytrace_parallel(__name__)

# protect further code from python parallelism
if __name__=='__main__':
    scene.saveDescription()

    scene.my_suffix = 'GPU'
    scene.raytrace_GPU(device="GPU")
    scene.saveDescription()
    os.rename('statistics.dat', 'statistics-'+scene.my_suffix+'.dat')

    scene.my_suffix = 'CPU'
    scene.raytrace_GPU(device="CPU")
    scene.saveDescription()
    os.rename('statistics.dat', 'statistics-'+scene.my_suffix+'.dat')

    scene.my_suffix = 'GPUCPU'
    scene.raytrace_GPU(device="GPUCPU")
    scene.saveDescription()
    os.rename('statistics.dat', 'statistics-'+scene.my_suffix+'.dat')
