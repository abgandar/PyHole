# -*- coding: utf-8 -*-
"""
Compute an image using Petya's metric.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole.scene import Scene
from gr_pyhole.metric.petya import Petya

scene = Scene()

scene.metric = Petya([0.0, 0.0, -1.0/300.])
#scene.metric = Petya([0.0, 0.9e-3, 0.0, -0.9e-3])
#scene.metric = Petya([0.0, 0.0, -0.02])

# Coordinates to use for the propagation (spherical or Cartesian)
#scene.coordinates = 'cartesian'
scene.coordinates = 'spherical'

# The radius of the observer (BL coordinates) or None for automatic
#scene.r = None
scene.r = 8.0

# The theta angle of the observer (rad)
scene.theta = np.deg2rad(90.0)

# image size in pixels
#scene.size = (32,32)
#scene.size = (64,64)
#scene.size = (128,128)
#scene.size = (256,256)
scene.size = (512,512)
#scene.size = (700,700)
#scene.size = (2048,1024)          # 360 degree VR sphere (use with Equirectangular projection and full fov=(pi,pi/2))

# The zoom region (image coordinates)
scene.zoom = ((-1.0,1.0),(-1.0,1.0))
#scene.size = (1024,-1)

# Integration precision to use (lower accuracy means faster computation)
scene.tolerance = scene.HIGH_ACCURACY
#scene.tolerance = scene.MEDIUM_ACCURACY
#scene.tolerance = scene.LOW_ACCURACY

# Which projection to use (Sterographic, Gnomonic, Equirectangular)
scene.projection = 'Equirectangular'

# Field of view for the camera (rad)
#scene.fov = np.arctan(10.0/15.0)
#scene.fov = np.deg2rad(90.0)
scene.fov = (np.pi, np.pi/2)

# Sky radius (None for automatic, 0.0 for infinite (not available in GPU propagator))
#scene.rsky = None
#scene.rsky = scene.r*2
scene.rsky = scene.r*1.8
#scene.rsky = 20

# Compute field of view to maximal allowed field of view such that entire sphere is in allowed region
# compute max allowed eta such that V<=0 at r=rsky
#x = scene.rsky-1.0
#P = scene.metric.LegendrePolynomials( 0.0 )
#U, dx_U, dy_U  = scene.metric.U(np.sqrt(x*x-1.0), np.array([x, 0.0]), P);
#eta = np.sqrt((x+1.0)**3/(x-1.0)*np.exp(-4.0*U))
## compute max angle at r=robserver such that resulting eta<0.99*eta_max
#scene.metric.setPoint([0.0, scene.r, np.pi/2.0, 0.0])
#sa = np.sqrt(-scene.metric.pp/scene.metric.tt)*eta*0.99      # contravariant components!
#if sa >= 1.0:
#    scene.fov = np.pi/2.0
#else:
#    scene.fov = np.arcsin(sa)
#print(eta, np.rad2deg(scene.fov))

# Custom suffix to be appended to the automatically generated file name (without leading -)
#scene.my_suffix = '_r={:.1f}_rsky={:.1f}_fov={:.1f}'.format(scene.r, scene.rsky, scene.fov)
scene.my_suffix = '_r={:.1f}_rsky={:.1f}_fov={:.1f},{:.1f}'.format(scene.r, scene.rsky, scene.fov[0], scene.fov[1])

# The following settings for the image rendering do not change the file name and do not require
# a recomputation of the dataset. They will not be overwritten when loading a scene data file.
#

# Show an Einstein ring by adding a star of this angular size (rad) diametrally oposed to the observer, or 0.0 for no Einstein ring
scene.ring = 0.0

# Show a grid of this many lines in theta (and twice that in phi). If 0, no grid is shown.
scene.grid = 18

# Celestial sphere background file
scene.background = 'bg-color.png'
#scene.background = 'bg-color-pastel.png'

# Color to use for rays that fell in the black hole
scene.shadowColor = [0.0, 0.0, 0.0, 1.0]

# Color to use for rays that float around the black hole (or None for shadow color)
scene.floaterColor = [1.0, 0.0, 1.0, 1.0]

# Color to use for rays that have an integrator error (or None for shadow color)
scene.errorColor = [0.0, 1.0, 1.0, 1.0]

### Main code
#

# 1a) Try to load a previously generated data file
if True or not scene.load():
    # 1b) If that failed, generate the data (chose one of these depending on your setup)
    # Computing the image based on all the settings above may take quite some time depending on the settings.
    # At the end of the computation, the image as well as the raw data is saved to a file.
    #scene.raytrace()
    #scene.raytrace_parallel(__name__)           # Note: before this particular command, you should not run anything except the setup code of the scene.
    #scene.raytrace_MPI()
    scene.raytrace_GPU(device="GPUCPU", platform_id=0, gpu_device_id=0)         # Note: the Intel OpenCL toolkit seems very buggy and does not work well with our code. Use The AMD APP OpenCL implementation instead (the CPU implementation also works on non-AMD systems)
    #raise Exception('uuuups, file not found')

# This is protecting the following code from child processes spawned in the raytrace_parallel command above
if __name__=='__main__':
    # 2) Save the image and additional information in various different ways
    #scene.saveImage('test.png')
    #scene.saveEHImage('test-EH.png')
    #scene.saveDescription('test.txt')
    #scene.saveWebGLTexture('test-tex.png')
    #scene.saveOverlay('test-ovl.png')

    # 3) Show interactive display
    scene.show(True)
