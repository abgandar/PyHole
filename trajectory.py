# -*- coding: utf-8 -*-
"""
A simple example to compute a trajectory output used for blackhole flybys.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module
import sys, os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole.scene import Scene

### User settings
#

scene = Scene()

# The following settings specify the data file to load.
# These must be exactly the same as when the data file was computed!
# They will be overwritten with the information in the scene data file when loading one!
#
# Which metric to use. Can be a fully instantiated metric, a metric name (Flat, Schwarzschild, Kerr, HRKerr, HRSchwarzschild)
# or a data file name for a HR metric
#scene.metric = 'BS-spherical-25-1st-0,8'
scene.metric = '10'
scene.metric = 'Schwarzschild'
#from mymetric import MyMetric
#scene.metric = MyMetric(a=2.0)

# Coordinates to use for the propagation (spherical or Cartesian)
#scene.coordinates = 'cartesian'
scene.coordinates = 'spherical'

# The radius of the observer (BL coordinates) or None for automatic
scene.r = None

# The theta angle of the observer (rad)
scene.theta = np.deg2rad(90.0)

# image size in pixels
scene.size = (32,32)
#scene.size = (64,64)
#scene.size = (128,128)
#scene.size = (256,256)
#scene.size = (1024,1024)
#scene.size = (2048,1024)          # 360 degree VR sphere (use with Equirectangular projection and full fov=(pi,pi/2))

# The zoom region (image coordinates)
scene.zoom = ((-1.0,1.0),(-1.0,1.0))
#scene.zoom = ((-0.340,-0.142), (-0.086,0.086))
#scene.size = (1024,-1)
#scene.zoom = ((0.1,0.6), (0.3,0.6))             # example of zooming in on upper brow in dataset 5

# Integration precision to use (lower accuracy means faster computation)
#scene.tolerance = scene.HIGH_ACCURACY
scene.tolerance = scene.MEDIUM_ACCURACY
#scene.tolerance = scene.LOW_ACCURACY

# Which projection to use (Sterographic, Gnomonic, Equirectangular)
scene.projection = 'Equirectangular'

# Field of view for the camera (rad)
scene.fov = np.arctan(10.0/15.0)
#scene.fov = (np.pi, np.pi/2.0)         # only possible with Equirectangular projection. Use for 360 degree VR sphere

# Sky radius (None for automatic, 0.0 for infinite (not available in GPU propagator))
scene.rsky = None

# Custom suffix to be appended to the automatically generated file name (without leading -)
scene.my_suffix = ''

# The following settings for the image rendering do not change the file name and do not require
# a recomputation of the dataset. They will not be overwritten when loading a scene data file.
#

# Show an Einstein ring by adding a star of this angular size (rad) diametrally oposed to the observer, or 0.0 for no Einstein ring
scene.ring = 0.0

# Show a grid of this many lines in theta (and twice that in phi). If 0, no grid is shown.
scene.grid = 18

# Celestial sphere background file
scene.background = 'bg-color.png'
#scene.background = 'bg-color-pastel.png'

# Color to use for rays that fell in the black hole
scene.shadowColor = [0.0, 0.0, 0.0, 1.0]

# Color to use for rays that float around the black hole (or None for shadow color)
scene.floaterColor = [1.0, 0.0, 1.0, 1.0]

# Color to use for rays that have an integrator error (or None for shadow color)
scene.errorColor = [0.0, 1.0, 1.0, 1.0]

### Main code
#

# 1a) Try to load a previously generated data file
if False or not scene.load():
    # 1b) If that failed, generate the data (chose one of these depending on your setup)
    # Computing the image based on all the settings above may take quite some time depending on the settings.
    # At the end of the computation, the image as well as the raw data is saved to a file.
    scene.raytrace()
    #scene.raytrace_parallel(__name__)           # Note: before this particular command, you should not run anything except the setup code of the scene.
    #scene.raytrace_MPI()
    #scene.raytrace_GPU(device="GPUCPU", platform_id=0)         # Note: the Intel OpenCL toolkit seems very buggy and does not work well with our code. Use The AMD APP OpenCL implementation instead (the CPU implementation also works on non-AMD systems)

# This is protecting the following code from child processes spawned in the raytrace_parallel command above
if __name__=='__main__':
    # 2) Save the image and additional information in various different ways
    #scene.saveImage('test.png')
    #scene.saveEHImage('test-EH.png')
    #scene.saveDescription('test.txt')
    #scene.saveWebGLTexture('test-tex.png')
    #scene.saveOverlay('test-ovl.png')

    # 3) Show interactive display
    scene.show(False)




#%%
def exportTrajectory():
    # Get the trajectory of the first selected trace in the display
    traj = scene.d.traces[0].traj
    lambda_data = traj[:,0]
    t_data = traj[:,1]
    r_data = traj[:,2]
    theta_data = traj[:,3]
    phi_data = traj[:,4]

    # create interpolated version of the trajectory
    # Note: the first parameter is lambda_data, meaning the spline interpolates as a function of lambda. This could be useful to change to t so that the spline interpolates as a function of coordinate time t.
    # Watch out: first argument of spline must be increasing, but we integrate backward in time so both lambda_data and t_data are strictly decreasing. So we put a minus here and remember to always evaluate at the negative of the point we want to evaluate the spline.
    from scipy.interpolate import InterpolatedUnivariateSpline as Spline
    t_spline = Spline(-lambda_data, t_data)
    r_spline = Spline(-lambda_data, r_data)
    theta_spline = Spline(-lambda_data, theta_data)
    phi_spline = Spline(-lambda_data, phi_data)

    # evaluate interpolation and first derivatives at new set of points
    # see note above for the minus sign in spline calls!
    lambda_new = np.linspace(lambda_data[0], lambda_data[-1], 25)
    t_ = t_spline(-lambda_new)
    r_ = r_spline(-lambda_new)
    theta_ = theta_spline(-lambda_new)
    phi_ = phi_spline(-lambda_new)
    dt_ = t_spline(-lambda_new, 1)
    dr_ = r_spline(-lambda_new, 1)
    dtheta_ = theta_spline(-lambda_new, 1)
    dphi_ = phi_spline(-lambda_new, 1)

    # compute cartesian coordinates and derivatives at interpolated points
    X_ = r_*np.sin(theta_)*np.cos(phi_)
    Y_ = r_*np.sin(theta_)*np.sin(phi_)
    Z_ = r_*np.cos(theta_)
    # compute cartesian derivatives at interpolated points
    dX_ = dr_*np.sin(theta_)*np.cos(phi_) + r_*np.cos(theta_)*np.cos(phi_)*dtheta_ - r_*np.sin(theta_)*np.sin(phi_)*dphi_
    dY_ = dr_*np.sin(theta_)*np.cos(phi_) + r_*np.cos(theta_)*np.cos(phi_)*dtheta_ + r_*np.sin(theta_)*np.sin(phi_)*dphi_
    dZ_ = dr_*np.cos(theta_) - r_*np.sin(theta_)*dtheta_

    # now compute and save texture at each point
    # of course adjust filenames, raytrace calls, etc to whatever you need
    # also add whatever other things are required for you here
    for i in range(len(r_)):
        scene.t = t_[i]
        scene.r = r_[i]
        scene.theta = theta_[i]
        scene.phi = phi_[i]
    #    scene.raytrace_GPU(device="GPUCPU", platform_id=0)
        scene.raytrace()
        scene.saveWebGLTexture("BH_{}_t.png".format(i))
        scene.saveImage("BH_{}_i.png".format(i))
        # the cartesian coordinates of this point are X_[i], Y_[i], Z_[i], and the (cartesian) tangent vector to the trajectory is dX_[i], dY_[i], dZ_[i] (not yet normalized!)
        # you probably need to write those out as well I assume
        print(np.array([X_[i], Y_[i], Z_[i]]))
        def normalize(z):
            z = np.array(z)
            return z/np.linalg.norm(z)
        print(normalize([dX_[i], dY_[i], dZ_[i]]))
