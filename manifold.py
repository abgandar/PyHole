# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 14:10:26 2017

@author: Alexander Wittig
"""

from scipy.integrate import ode
from scipy.optimize import ridder, newton
import numpy as np
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
import sys

class Manifold:
    TOL = 1e-11
    NTOL = 1e-9

    def __init__(self,scene):
        """Save our associated scene object"""
        self.scene = scene

    def followManifold(self, x0, dr, num=10):
        """Follow the manifold in the Image (alpha, beta) stating from light ring at x"""
        resx = []
        resy = []
        res = []
        x = np.array(x0)
        for i in range(0,num):
            try:
                x[1] += dr/num
                print("   Starting with: ", x)
                x, T = self.findClosedOrbit(x)
                print("   Found closed orbit #{}: ".format(i+1), x)
                cool = self.getObserverOrbit(x, T)
                for states in cool:
                    state = states[1]
                    print("   Got observer orbit: ", state)
                    alpha, beta = self.getAlphaBeta(state)
                    xx, yy = self.scene.o.project(alpha, beta)
                    print("   alpha: {}, beta: {}     xx: {}, yy: {}".format(alpha, beta, xx, yy))
                    resx.append(xx)
                    resy.append(yy)
                    res.append({ "X": xx, "Y": yy, "alpha": alpha, "beta": beta, "initial": states[0], "final": states[1], "x0": x, "T": T })
            except:
                print(sys.exc_info()[0])
                pass
        return (resx, resy, res)

    def getAlphaBeta(self,x):
        """get viewing angles alpha, beta from a state at the observer location"""
        self.scene.g.setPoint(x)
        [g_tt, g_rr, g_thth, g_pp, g_tp] = self.scene.g.toLower()
        alpha = np.arctan2(np.sqrt(g_rr*(x[6]**2/g_thth + x[7]**2/g_pp)), x[5])
        beta = np.arctan2(-x[6]*np.sqrt(g_pp), -x[7]*np.sqrt(g_thth))
        return (alpha, beta)

    def getObserverOrbit(self, xin, T, num = 8,  which="u", sign=1.0, delta=1e-6):
        """Get an unstable orbit passing through the observer"""
        dxs, dxu, ew, ev = self.getStableUnstable(xin, T, delta)
        dx = dxu
        s = 1.0
        if which != "u":
            dx = dxs
            s = -1.0
        if sign*dx[1]<0: dx = -dx
        #if sign*dx[5]<0: dx = -dx
        points = self.getPointsAlongOrbit(x0, s*T, 1e-3*dx, num)
        targets = []
        for i in points[0]:
            temp = self.propagateToObserver(i, s)
            if not temp is False: targets.append(temp[0])
        if len(targets) == 0: return [] # nothing actually reached the observer
        targets.append(targets[0])  # make sure to close the circle by repeating the first point
        targets = np.array(targets)
        theta = targets[:,2]-self.scene.o.theta
        indices = np.where(theta[0:-1]*theta[1:]<0.0)[0]
        res = []
        print("    Found ", len(indices), " potential observer orbits")
        for idx in indices:
            try:
                # now we have the one orbit that is closest to the observer, optimize on it to find the one that hits exactly
                y = points[1,idx,:]
                dy = points[0,idx,:]-y

                def obj(dt):
                    # objective function: the value of theta when we hit the observer
                    yy, dyy = self.pushForward(y, dy, s*dt)
                    dyy *= np.linalg.norm(dy)/np.linalg.norm(dyy)
                    res = self.propagateToObserver(yy+dyy, s)[0][2] - self.scene.o.theta
                    print("    ====> RESULT: ", res)
                    return res

                dt = ridder(obj, 0.0, T/(2*num), xtol=1e-4, rtol=1e-4)
                yy, dyy = self.pushForward(y, dy, s*dt)
                dyy *= np.linalg.norm(dy)/np.linalg.norm(dyy)
                yfinal = self.propagateToObserver(yy+dyy, s)[0][0:8]
                res.append((yy+dyy, yfinal))
            except:
                print(sys.exc_info()[0])
                pass

        return res

    def pushForward(self, x0, dx0, dt, dl=1e-5):
        """Take point x0 and tangent vector dx0 and propagate both for dt."""
        y = self.propagate(x0, dt)[:8]
        y[0] = 0.0  # remove the time and phi variables
        y[3] = 0.0
        l = np.linalg.norm(dx0)
        # propagate small perturbation in dx0 direction
        temp = self.propagate(x0+dx0*(dl/l), dt)[:8]
        temp[0] = 0.0  # remove the time and phi variables
        temp[3] = 0.0
        # scale back to original length
        dy = (temp-y)*(l/dl)
        return (y, dy)

    def getPointsAlongOrbit(self, xin, T, dxin, num = 20):
        """Return list of points that are equally spaced around the given periodic orbit
        and offset along the (pushed forward) direction dxin by the length of dxin"""
        y = np.array(xin)
        dy = np.array(dxin)
        dy.resize(y.shape)
        res = np.empty((2, 2*num, y.shape[0]))
        dt = T/(2*num)
        l = np.linalg.norm(dy)
        yy = np.array(y)
        dyy = np.array(dy)
        print('Generating points along orbit: ', end='', flush=True)

        # skip first few points, they seem numerically bad (probably the unstable direction is not very accurate)
        for i in range(25):
            yy, dyy = self.pushForward(yy, dyy, T/25)
            dyy *= l/np.linalg.norm(dyy)
        yy = np.array(y)    # reset reference point on periodic orbit

        # compute actual points
        for i in range(0,2*num):
            yy, dyy = self.pushForward(yy, dyy, dt)
            res[0,i,:] = yy + dyy*(l/np.linalg.norm(dyy))
            res[1,i,:] = yy
            print('.', end='', flush=True)
        print(" done")
        #return res[:,np.array([0,1,2,-3,-2,-1]),:]
        return res

    def getStableUnstable(self, x, T, delta=1e-6):
        """Return the stable and unstable direction estimates at the given periodic orbit"""
        mat = self.getMatrix(x, T, delta)
        ew, ev = np.linalg.eig(mat)
        # assume ordering of eigenvalues real>1, real<1, 2 complex conjugates
        dxu = self.expandVector(np.real(ev[:,0]))
        dxs = self.expandVector(np.real(ev[:,1]))
        return (dxs, dxu, ew, ev)

    def expandVector(self, x):
        """Expand given vector to full 8D size by padding with zeros"""
        return np.array( [0.0, x[0], x[1], 0.0, 0.0, x[2], x[3], 0.0 ] )

    def getMatrix(self, xin, Tin, delta=1e-6):
        """Get the transition matrix for closed orbit x with period T"""
        # jiggle r, theta, pr, ptheta up and down to compute approximate derivatives
        DELTA = delta
        DELTAr = delta*min(1.0, abs(xin[1])) # fudgy fudge fudge
        DELTAp = delta
#        T = Tin/2   # only use half turn for better numerical accuracy?
        T = Tin
        x = np.array(xin)
        x[1] += DELTAr
        x_rp = self.propagate(x, T)
        x = np.array(xin)
        x[1] -= DELTAr
        x_rm = self.propagate(x, T)
        x = np.array(xin)
        x[2] += DELTA
        x_thp = self.propagate(x, T)
        x = np.array(xin)
        x[2] -= DELTA
        x_thm = self.propagate(x, T)
        x = np.array(xin)
        x[5] += DELTAp
        x_prp = self.propagate(x, T)
        x = np.array(xin)
        x[5] -= DELTAp
        x_prm = self.propagate(x, T)
        x = np.array(xin)
        x[6] += DELTAp
        x_pthp = self.propagate(x, T)
        x = np.array(xin)
        x[6] -= DELTAp
        x_pthm = self.propagate(x, T)
        # compute derivatives
        dxdr = (x_rp - x_rm)/(2*DELTAr)
        dxdth = (x_thp - x_thm)/(2*DELTA)
        dxdpr = (x_prp - x_prm)/(2*DELTAp)
        dxdpth = (x_pthp - x_pthm)/(2*DELTAp)
        # assemble transition matrix
        res = [ [dxdr[1], dxdr[2], dxdr[5], dxdr[6]],
                [dxdth[1], dxdth[2], dxdth[5], dxdth[6]],
                [dxdpr[1], dxdpr[2], dxdpr[5], dxdpr[6]],
                [dxdpth[1], dxdpth[2], dxdpth[5], dxdpth[6]] ]
        res = np.array(res)
        return res;
        #return np.matmul(res, res);

    def findClosedOrbit(self, xin):
        """Find a closed orbit starting from the given point changing eta/L"""

        def obj(pth, ic, L0):
            #ic[6] = min(pth, pth_max*0.9999999)
            ic[6] = pth
            self.scene.g.setPoint(ic)
            LL = self.scene.g.getL(pth=ic[6], pt=ic[4])
            ic[5] = 0       # pr = 0
            # pick the L that is closer to the original one
            if abs(LL[0]-L0) < abs(LL[1]-L0):
                ic[7] = LL[0]
            else:
                ic[7] = LL[1]
            if np.isnan(ic[7]):
                print(pth, False)
                return 10.0 # False
            print(" findClosedOrbit: L=", ic[7])
            res = self.halfturn(ic)
            if not res:
                print(pth, False, "    integrator failure")
                return 10.0
            else:
                print(pth, res[0][5])
                return res[0][5]

        self.scene.g.setPoint(xin)
        temp = (self.scene.g.tp**2/self.scene.g.pp - self.scene.g.tt)/self.scene.g.thth     # from null condition solved for L (positive determinant)
        if temp <= 0.0:
            return False
        pth_max = np.sqrt(temp)
#        pth_max = 1000000
        if xin[6]<pth_max and xin[6]>0.0 or True:
            pth0 = xin[6]
        else:
            pth0 = pth_max*0.9        # just a first guess
            print("reduced pth0 to pth_max=", pth_max)
        L0 = xin[7]
        try:
            x = np.array(xin)
            pth = newton(obj, pth0, args=(x,L0), tol=self.NTOL)
            x[6] = pth
            T = self.halfturn(x)[1]
        except:
            # if Newton doesn't work (close to singularity), try Ridder and hope for the best
            print("switching to Ridder")
            x = np.array(xin)
            pth = ridder(obj, pth0*0.98, pth_max*0.9999999, args=(x,L0))
            x[6] = pth
            T = self.halfturn(x)[1]
        return (x, 2*T)

    def halfturn(self, ic):
        """Propagate ic until it hits the equatorial plane from above"""
        state = self.scene.p.getMomenta(ic)
        rk = ode(self.scene.p).set_integrator(self.scene.p.integrator, atol=self.TOL, rtol=self.TOL, max_step=1.0, nsteps=5000)
        rk.set_initial_value(state, 0.0)
        deltat = deltatmax = 1e-2
        returning = False
        self.scene.p.error = False
        while (not self.scene.p.error) and rk.successful() and ((not returning) or np.abs(state[2]-np.pi/2)>self.TOL) and (rk.t<200):
            #print(" Halfturn: ", rk.t, state[1], state[2])
            state = rk.integrate(rk.t+deltat)
            deltat_new = deltatmax
            if state[2] < np.pi/2-1e-4 or returning:
                # we're close to or below the plane, so try to return there
                returning = True
                rhs = self.scene.p(rk.t, state)
                deltat_new = 0.95*(np.pi/2-state[2])/rhs[2]
            # WARNING: vode is not able to reverse integration direction
            # Hack around it by reinitializing if direction changes (shouldn't be too often)
            if deltat*deltat_new<0.0 and self.scene.p.integrator=='vode':
                rk.set_initial_value(state, rk.t)
            # limit step size to maximum allowed
            if abs(deltat_new) > deltatmax:
                deltat_new = deltatmax*(deltat_new/abs(deltat_new))
            deltat = deltat_new
        if (not returning) or np.abs(state[2]-np.pi/2)>self.TOL:
            print("   returning: ", returning, "   Error: ", np.abs(state[2]-np.pi/2), "   success: ", rk.successful(), "   rkt ", rk.t, "   scene.p.error", self.scene.p.error )
            return False
        else:
            return (state, rk.t)

    def propagateToObserver(self, ic, s=1.0):
        """Propagate ic until it hits the observer r"""
        return self.propagateToPlane(ic, self.scene.o.r, s)

    def propagateToPlane(self, ic, r1, s=1.0):
        """Propagate ic until it hits the given r1. The integration direction is given by s (+1.0 forward, -1.0 backward)"""
        state = self.scene.p.getMomenta(ic)
        rk = ode(self.scene.p).set_integrator(self.scene.p.integrator, atol=self.TOL, rtol=self.TOL, max_step=1.0, nsteps=50000)
        rk.set_initial_value(state, 0.0)
        deltatmax = 0.5
        deltat = s*deltatmax
        sign = 1
        if state[1]>r1: sign = -1.0
        returning = False
        self.scene.p.error = False
        while (not self.scene.p.error) and rk.successful() and ((not returning) or np.abs(state[1]-r1)>1e-5) and (abs(rk.t)<500):
            state = rk.integrate(rk.t+deltat)
            #print(state[1], state[2])
            deltat_new = s*deltatmax
            if sign*(state[1]-r1) > 1e-2 or returning:
                # we're close to or after the r target, so try to return there
                returning = True
                rhs = self.scene.p(rk.t, state)
                deltat_new = 0.9*(r1-state[1])/rhs[1]
            # WARNING: vode is not able to reverse integration direction
            # Hack around it by reinitializing if direction changes (shouldn't be too often)
            if deltat*deltat_new<0.0 and self.scene.p.integrator=='vode':
                rk.set_initial_value(state, rk.t)
            # limit step size to maximum allowed
            if abs(deltat_new) > abs(deltatmax):
                deltat_new = abs(deltatmax)*(deltat_new/abs(deltat_new))
            deltat = deltat_new
        if (not returning) or np.abs(state[1]-r1)>1e-5:
            print("    failed to propagate to plane: ", self.scene.p.error, rk.successful(), rk.t)
            return False
        else:
            print("    propagated to plane")
            return (state, rk.t)

    def propagate(self, ic, t):
        """Propagate ic by time t"""
        state = self.scene.p.getMomenta(ic)
        rk = ode(self.scene.p).set_integrator(self.scene.p.integrator, atol=self.TOL, rtol=self.TOL, max_step=1.0, nsteps=10000)
        rk.set_initial_value(state, 0.0)
        self.scene.p.error = False
        return rk.integrate(t)

    def propagateTrace(self, ic, t, num=300):
        """Propagate ic by time t"""
        state = self.scene.p.getMomenta(ic)
        rk = ode(self.scene.p).set_integrator(self.scene.p.integrator, atol=self.TOL, rtol=self.TOL, max_step=1.0, nsteps=10000)
        rk.set_initial_value(state, 0.0)
        self.scene.p.error = False
        res = np.empty((num,8))
        res[0,:] = state[:8]
        for i in range(1,num):
            st = rk.integrate(rk.t+t/num)
            res[i,:] = st[:8]
        return res



def getConstEtaLine(eta):
    pthmax = 9.0
    res = []
    for i in range(200):
        pth = (-1.0+i/100.0)*pthmax
        x = scene.g.getNullIC(scene.o.r, scene.o.theta, pth=pth, L=eta, normalize=False)
        a, b = mfd.getAlphaBeta(x)
        x, y = scene.o.project(a, b)
        res.append((x, y))
    return np.array(res)

def plotMfdIntersection(r1, x0, T=None, num=40, which="u", sign=1, delta=1e-6):
    if T is None: x0, T = mfd.findClosedOrbit(x0)
    dxs, dxu, ew, ev = mfd.getStableUnstable(x0, T, delta)
    dx = dxu
    s = 1.0
    if which != "u":
        dx = dxs
        s = -1.0
    if sign*dx[1]<0: dx = -dx
    points = mfd.getPointsAlongOrbit(x0, s*T, 1e-4*dx, num)
    targets = []
    for i in points[0]:
        temp = mfd.propagateToPlane(i, r1, s)
        if not temp is False: targets.append(temp[0])
    targets.append(targets[0])  # close the loop
    targets = np.array(targets)
    pyplot.figure()
    pyplot.xlabel("θ")
    pyplot.ylabel("pθ")
    pyplot.plot(targets[:,2],targets[:,6])
    return targets

def plotMfdIntersectionSlices(r1s, x0, T=None, num=40, which="u", sign=1, delta=1e-6):
    if T is None: x0, T = mfd.findClosedOrbit(x0)
    dxs, dxu, ew, ev = mfd.getStableUnstable(x0, T, delta)
    dx = dxu
    s = 1.0
    if which != "u":
        dx = dxs
        s = -1.0
    if sign*dx[1]<0: dx = -dx
    points = mfd.getPointsAlongOrbit(x0, s*T, 1e-4*dx, num)[0]
    import collections
    if not isinstance(r1s, (collections.Sequence, np.ndarray)):
        r1s = [r1s]
    points = np.concatenate((points, [points[0]]))  # close the loop
    points = np.pad(points, ((0,0), (0,3)), mode='constant', constant_values=0)
    res = [points,]
    for ns, r1 in enumerate(r1s):
        targets = []
        for i in res[-1]:
            temp = mfd.propagateToPlane(i, r1, s)
            if not temp is False: targets.append(temp[0])
        targets = np.array(targets)
        pyplot.figure()
        pyplot.xlabel("θ")
        pyplot.ylabel("pθ")
        pyplot.title('X={}'.format(scene.g.r2X(r1)))
        pyplot.plot(targets[:,2],targets[:,6])
        res.append(targets)
        print('Finished slice {} of {} ({}%)'.format(ns+1,len(r1s), 100*(ns+1)/len(r1s)))
    return res

def plotMfdPotential(x0, T=None, col=((1.0, 0.7, 0.7), (1.0, 0.0, 0.0)), num=40, which="u", sign=1, delta=1e-6):
    if T is None: x0, T = mfd.findClosedOrbit(x0)
    dxs, dxu, ew, ev = mfd.getStableUnstable(x0, T, delta)
    dx = dxu
    s = 1.0
    if which != "u":
        dx = dxs
        s = -1.0
    if sign*dx[1]<0: dx = -dx
    #if sign*dx[5]<0: dx=-dx
    points = mfd.getPointsAlongOrbit(x0, T, 1e-4*dx, num)
    print('Computing traces: ', end='', flush=True)
    for i in points[0]:
        tr = Trace()
        tr.color = col[0]
        scene.d.traceIC(i, dt=-s*0.003, trace=tr)          # this does not make sense for unstable mfd because it integrates backward!!!!!
        print('.', end='', flush=True)
    print(' done')

    # Do the periodic orbit on top
    lmold = scene.p.LMAX
    scene.p.LMAX = T*1.05
    tr = Trace()
    tr.color = col[1]
    scene.d.traceIC(x0, dt=-s*0.003, trace=tr)
    scene.p.LMAX = lmold
    
    scene.d.showPotentialAllTraces(showErgo=False, showLightrings=False)
    return points

def plotBoundaries(res1, res2, res3, lrs=[]):
    scene.show(False)

    X1, Y1 = ( [i["X"] for i in res1], [i["Y"] for i in res1])
    X2, Y2 = ( [i["X"] for i in res2], [i["Y"] for i in res2])
    X3, Y3 = ( [i["X"] for i in res3], [i["Y"] for i in res3])
    scene.d.figImage.gca().plot(X1, Y1, "ow", ms=3.5)
    scene.d.figImage.gca().plot(X2, Y2, "ow", ms=3.5)
    scene.d.figImage.gca().plot(X3, Y3, "ow", ms=3.5)

    XL = []
    YL = []
    for lr in lrs:
        x, y = scene.o.project(*mfd.getAlphaBeta(scene.g.getNullIC(scene.o.r, scene.o.theta, pth=0.0, L=lr[7], normalize=False)))
        XL.append(x)
        YL.append(y)
    scene.d.figImage.gca().plot(XL, YL, "sw", ms=4.5)

def plotBoundariesColor(res1, res2, res3, lrs=[]):
    scene.show(False)

    X1, Y1 = ( [i["X"] for i in res1], [i["Y"] for i in res1])
    X2, Y2 = ( [i["X"] for i in res2], [i["Y"] for i in res2])
    X3, Y3 = ( [i["X"] for i in res3], [i["Y"] for i in res3])
    scene.d.figImage.gca().plot(X1, Y1, "or", ms=3.5)
    scene.d.figImage.gca().plot(X2, Y2, "og", ms=3.5)
    scene.d.figImage.gca().plot(X3, Y3, "ob", ms=3.5)

    col = ['r', 'g', 'b']
    for col, lr in zip(col,lrs):
        x, y = scene.o.project(*mfd.getAlphaBeta(scene.g.getNullIC(scene.o.r, scene.o.theta, pth=0.0, L=lr[7], normalize=False)))
        scene.d.figImage.gca().plot(x, y, "s"+col, ms=4.5)

def plotEnvelopes(res1, fig, col, lr, step):
    X01 = [i["x0"] for i in res1]
    env = [[lr[1], np.pi/2]]
    lastr = -1
    lastrd = -1
    pth = X01[0][6]
    j = 0
    for x0 in X01:
        if abs(x0[1]-lastr)<1e-9: continue      # skip multiple points
        j += 1
        lastr = x0[1]
        x0[6] = pth
        try:
            x0, T = mfd.findClosedOrbit(x0)
        except:
            continue
        pth = x0[6]
        trace = mfd.propagateTrace(x0, T, 50)
        i = np.argmax(trace[:,2])
        env.append([trace[i,1], trace[i,2]])
        print("  COMPLETED: ", j)
        if abs(x0[1]-lastrd)<step: continue      # skip drawing nearby points
        lastrd = x0[1]
        X = scene.g.r2X(trace[:,1])
        fig.gca().plot(X, trace[:,2], col+"-")
    env = np.array(env)
    envX = scene.g.r2X(env[:,0])
    fig.gca().plot(envX, env[:,1], col+"--", lw=2)
    fig.gca().plot(envX, np.pi-env[:,1], col+"--", lw=2)

def plotEnvelopesNew(fig, col, lr, rfinal, step, num=30):
    x0 = np.array(lr)
    env = [[lr[1], np.pi/2]]
    lastrd = -1
    x0[6] = 0.8
    j = 0
    for Xpos in np.linspace(scene.g.r2X(lr[1]), scene.g.r2X(rfinal), num)[1:]:
        x0[1] = scene.g.X2r(Xpos)
        j += 1
        try:
            x0, T = mfd.findClosedOrbit(x0)
        except:
            print("  SKIPPED")
            continue
        trace = mfd.propagateTrace(x0, T, 50)
        i = np.argmax(trace[:,2])
        env.append([trace[i,1], trace[i,2]])
        print("  COMPLETED: ", j)
        if abs(x0[1]-lastrd)<step: continue      # skip drawing nearby points
        lastrd = x0[1]
        X = scene.g.r2X(trace[:,1])
        fig.gca().plot(X, trace[:,2], col+"-")
    env = np.array(env)
    envX = scene.g.r2X(env[:,0])
    fig.gca().plot(envX, env[:,1], col+"--", lw=2)
    fig.gca().plot(envX, np.pi-env[:,1], col+"--", lw=2)

def plotEtas(etas=[], labels=None, va='bottom', offset=0):
    if len(etas) == 0: return
    if va == 'top':
        offset = -offset-1
    cel = getConstEtaLine(etas[0])
    if va == 'bottom':
        y = cel[offset,1]+0.05
    else:
        y = cel[offset,1]-0.05
    for i, eta in enumerate(etas):
        cel = getConstEtaLine(eta)
        scene.d.figImage.gca().plot(cel[:,0], cel[:,1],"w",lw=1.5)
        if labels:
            if labels[i][0] == 'L':
                scene.d.figImage.gca().text(cel[offset,0]+0.03, y, labels[i][1:], ha='left', va=va, color='w')
            elif labels[i][0] == 'R':
                scene.d.figImage.gca().text(cel[offset,0]-0.03, y, labels[i][1:], ha='right', va=va, color='w')
            else:
                scene.d.figImage.gca().text(cel[offset,0]-0.03, y, labels[i], ha='right', va=va, color='w')


compute = False
plot = False
plotEnvelope = True
plotPotential = False
if __name__=="__main__":
    from sceneManifold import scene
    from pyhole.display import Display, Trace
    Display.FONTSIZE = 10
    scene.show(False)
    mfd = Manifold(scene)
    lrs = scene.g.findLightrings()

    lr1 = np.array(lrs[2][0])
    lr2 = np.array(lrs[0][0])
    lr3 = np.array(lrs[3][0])
    if compute:
        # first light ring (move inward)
        x0 = np.array(lr1)
        x0[6] = 0.3
        dummy, dummy, res1 = mfd.followManifold(x0, -1.92, 30)

        # second light ring (move inward)
        x0 = np.array(lr2)
        x0[6] = 0.3
        dummy, dummy, res2 = mfd.followManifold(x0, -0.0039, 30)

        # third light ring (move outward)
        x0 = np.array(lr3)
        x0[6] = 0.3
        dummy, dummy, res3 = mfd.followManifold(x0, 0.02, 30)

        np.savez_compressed("boundary5.npz", res1, res2, res3)

        # load intersect points and compute intersections
        f = np.load("boundary-intersect5.npz")
        x0_1_06 = f['x0_1_06']      # these are points I manually fiddled to have a given value of eta
        x0_1_26 = f['x0_1_26']
        x0_2_26 = f['x0_2_26']
        x0_3_06 = f['x0_3_06']
        f.close()
        res_1_06 = plotMfdIntersection(scene.o.r, x0_1_06, T=None, num=500)     #OK
        res_1_26 = plotMfdIntersection(scene.o.r, x0_1_26, T=None, num=100)     #OK
        res_2_26 = plotMfdIntersection(scene.o.r, x0_2_26, T=None, num=100)     #OK
        res_3_06 = plotMfdIntersection(scene.o.r, x0_3_06, T=None, num=300)     #OK

        np.savez_compressed("boundary-intersect5.npz", x0_1_06=x0_1_06, x0_1_26=x0_1_26, x0_2_26=x0_2_26, x0_3_06=x0_3_06, res_1_06=res_1_06, res_1_26=res_1_26, res_2_26=res_2_26, res_3_06=res_3_06)
    else:
        f = np.load("boundary5.npz")
        res3 = f['arr_2']
        res2 = f['arr_1']
        res1 = f['arr_0']
        f.close()
        f = np.load("boundary-intersect5.npz")
        x0_1_06 = f['x0_1_06']
        x0_1_26 = f['x0_1_26']
        x0_2_26 = f['x0_2_26']
        x0_3_06 = f['x0_3_06']
        res_1_06 = f['res_1_06']
        res_1_26 = f['res_1_26']
        res_2_26 = f['res_2_26']
        res_3_06 = f['res_3_06']
        f.close()

    if plot:
        plotBoundaries(res1, res2, res3, (lr1, lr2, lr3))
        scene.d.figImage.savefig("boundary.png", dpi=400, bbox_inches="tight", pad_inches=0)
        etas = (lr1[7], lr2[7], lr3[7])
        labels = ("η1", "η2 ", "η3")
        plotEtas(etas, labels, offset=23)
        scene.d.figImage.savefig("boundary-etas.png", dpi=400, bbox_inches="tight", pad_inches=0)

        plotBoundariesColor(res1, res2, res3, (lr1, lr2, lr3))
        scene.d.switchImage(5)
        scene.d.figImage.savefig("boundary-finaltime.png", dpi=400, bbox_inches="tight", pad_inches=0)
        etas = (-2.6, -0.6)
        labels = ("Lη=-2.6", "Rη=-0.6")
        plotEtas(etas, labels, va='top', offset=31)
        scene.d.figImage.savefig("boundary-finaltime-etas.png", dpi=400, bbox_inches="tight", pad_inches=0)

        # increase font size
        import matplotlib
        matplotlib.rcParams.update({'font.size': 18})

        fig = pyplot.figure()
        fig.gca().set_xlabel("θ")
        fig.gca().set_ylabel("pθ")
        fig.gca().set_xlim((0,np.pi))
        fig.gca().set_ylim((-5.5,5.5))
        p_1_06 = matplotlib.patches.Polygon(np.stack((res_1_06[:,2], res_1_06[:,6]),axis=1), color="r", alpha=0.3)
        fig.gca().add_patch(p_1_06)
        p_3_06 = matplotlib.patches.Polygon(np.stack((res_3_06[:,2], res_3_06[:,6]),axis=1), color="b", alpha=0.5)
        fig.gca().add_patch(p_3_06)
        fig.gca().plot(res_1_06[:,2], res_1_06[:,6], "r-", lw=2.5)
        fig.gca().plot(res_3_06[:,2], res_3_06[:,6], "g-", lw=2.5)
        fig.gca().plot((np.pi/2, np.pi/2), (-5.49, 5.49), "k--")
        fig.savefig("boundary-intersect-06.png", dpi=400, bbox_inches="tight", pad_inches=0)

        fig = pyplot.figure()
        fig.gca().set_xlabel("θ")
        fig.gca().set_ylabel("pθ")
        fig.gca().set_xlim((0,np.pi))
        fig.gca().set_ylim((-5.5,5.5))
        p_1_06 = matplotlib.patches.Polygon(np.stack((res_1_26[:,2], res_1_26[:,6]),axis=1), color="r", alpha=0.3)
        fig.gca().add_patch(p_1_06)
        p_3_06 = matplotlib.patches.Polygon(np.stack((res_2_26[:-10,2], res_2_26[:-10,6]),axis=1), color="g", alpha=0.5)    # -10 to skip the windup part that numerically doesn't close well!
        fig.gca().add_patch(p_3_06)
        fig.gca().plot(res_1_26[:,2], res_1_26[:,6], "r-", lw=2.5)
        fig.gca().plot(res_2_26[:,2], res_2_26[:,6], "g-", lw=2.5)
        fig.gca().plot((np.pi/2, np.pi/2), (-5.49, 5.49), "k--")
        fig.savefig("boundary-intersect-26.png", dpi=400, bbox_inches="tight", pad_inches=0)

    if plotEnvelope:
        # increase font size
        import matplotlib
        matplotlib.rcParams.update({'font.size': 18})

        fig = pyplot.figure()
        fig.gca().set_xlabel("X")
        fig.gca().set_ylabel("θ")
        #fig.gca().set_xlim((0,1))
        fig.gca().set_ylim((0,np.pi))
        fig.gca().set_yticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi])
        fig.gca().set_yticklabels(['0', 'π/4', 'π/2', '3π/4', 'π'])
        mfd.NTOL = 1e-6
        plotEnvelopesNew(fig, "g", lr=lr3, rfinal=lr2[1], step=0.00013*10, num=400)
#        plotEnvelopes(res2, fig, "g", lr=lr2, step=0.00013*5)
#        plotEnvelopes(res3, fig, "g", lr=lr3, step=0.00065*2) # b
        fig.gca().plot(scene.g.r2X(lr2[1]), lr2[2], "gs")
        fig.gca().plot(scene.g.r2X(lr3[1]), lr3[2], "gs") #bs
        fig.savefig("boundary-envelope2.png", dpi=400, bbox_inches="tight", pad_inches=0)

        fig = pyplot.figure()
        fig.gca().set_xlabel("X")
        fig.gca().set_ylabel("θ")
        #fig.gca().set_xlim((0,1))
        fig.gca().set_ylim((0,np.pi))
        fig.gca().set_yticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi])
        fig.gca().set_yticklabels(['0', 'π/4', 'π/2', '3π/4', 'π'])
        mfd.NTOL = 1e-6
        plotEnvelopesNew(fig, "r", lr=lr1, rfinal=scene.g.X2r(0.42), step=0.064*3, num=30)
        fig.gca().plot(scene.g.r2X(lr1[1]), lr1[2], "rs")
        fig.savefig("boundary-envelope1.png", dpi=400, bbox_inches="tight", pad_inches=0)

    if plotPotential:
        col1 = ((1.0, 0.7, 0.7), (1.0, 0.0, 0.0))
        col0 = ((0.7, 1.0, 0.7), (0.0, 1.0, 0.0))
        scene.d.p.Rsky = scene.d.o.r
        plotMfdPotential(x0_1_26, num=7, col=col1)
        plotMfdPotential(x0_2_26, num=14, col=col0)
        pyplot.savefig("mfd-potential.pdf", dpi=400, bbox_inches="tight", pad_inches=0)

        #%% compute and plot 3D manifold tube of x0_2_26
        rslices = scene.g.X2r(np.linspace(scene.g.r2X(x0_2_26[1]), scene.g.r2X(scene.o.r), 155))
        rslices = rslices[5:]
        res = plotMfdIntersectionSlices(rslices, x0_2_26, num=80)
        res = np.array(res)
        fig = pyplot.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.cla()
        ax.set_xlabel('X')
        ax.set_ylabel('θ')
        ax.set_zlabel('pθ')
        X = scene.g.r2X(res[:,:,1])
        Y = res[:,:,2]
        Z = res[:,:,6]
        ax.plot_surface(X, Y, Z, rstride=1, cstride=1, shade=True)

        #%% save STL file (not closing circle)
        X = scene.g.r2X(res[:,:-1,1])
        Y = res[:,:-1,2]
        Z = res[:,:-1,6]
        XX = X/np.max(X)
        YY = Y/np.max(Y)
        ZZ = Z/np.max(Z)

        #%% save STL file (closing circle)
        X = scene.g.r2X(res[:,:,1])
        Y = res[:,:,2]
        Z = res[:,:,6]
        XX = X/np.max(X)
        YY = Y/np.max(Y)
        ZZ = Z/np.max(Z)

        #thicken walls
        XX1 = np.array(XX)
        YY1 = np.array(YY)
        ZZ1 = np.array(ZZ)
        XX2 = np.array(XX)
        YY2 = np.array(YY)
        ZZ2 = np.array(ZZ)
        wid = 0.02
        for i in range(1,len(XX)-1):
            lj = len(XX[i])
            for j in range(0,lj):
                v1 = np.array([XX[i+1,j], YY[i+1,j], ZZ[i+1,j]])-np.array([XX[i,j], YY[i,j], ZZ[i,j]])
                v2 = np.array([XX[i,(j+1)%lj], YY[i,(j+1)%lj], ZZ[i,(j+1)%lj]])-np.array([XX[i,j], YY[i,j], ZZ[i,j]])
                v3 = np.array([XX[i-1,j], YY[i-1,j], ZZ[i-1,j]])-np.array([XX[i,j], YY[i,j], ZZ[i,j]])
                v4 = np.array([XX[i,(j-1)%lj], YY[i,(j-1)%lj], ZZ[i,(j-1)%lj]])-np.array([XX[i,j], YY[i,j], ZZ[i,j]])
                n1 = np.cross(v1,v2)
                n2 = np.cross(v2,v3)
                n3 = np.cross(v3,v4)
                n4 = np.cross(v4,v1)
                n1 /= np.linalg.norm(n1)
                n2 /= np.linalg.norm(n2)
                n3 /= np.linalg.norm(n3)
                n4 /= np.linalg.norm(n4)
                n = (n1+n2+n3+n4)/4
                XX1[i,j] += wid*n[0]
                XX2[i,j] -= wid*n[0]
                YY1[i,j] += wid*n[1]
                YY2[i,j] -= wid*n[1]
                ZZ1[i,j] += wid*n[2]
                ZZ2[i,j] -= wid*n[2]
        XX = np.concatenate( ([XX[0]], XX1, [XX[-1]], XX2[::-1], [XX[0]]) )
        YY = np.concatenate( ([YY[0]], YY1, [YY[-1]], YY2[::-1], [YY[0]]) )
        ZZ = np.concatenate( ([ZZ[0]], ZZ1, [ZZ[-1]], ZZ2[::-1], [ZZ[0]]) )
        XX = np.concatenate( (XX, np.array([XX[:,0]]).T), 1 )
        YY = np.concatenate( (YY, np.array([YY[:,0]]).T), 1 )
        ZZ = np.concatenate( (ZZ, np.array([ZZ[:,0]]).T), 1 )

        f = open("x0_2_26-thin.stl", 'w')
        f.write("solid x0_2_26\n")
        for i in range(0,len(XX)-1):
            for j in range(len(XX[i])-1):
                f.write("facet normal 0 0 0\n")
                f.write("outer loop\n")
                f.write("vertex {} {} {}\n".format(XX[i,j], YY[i,j], ZZ[i,j]))
                f.write("vertex {} {} {}\n".format(XX[i+1,j], YY[i+1,j], ZZ[i+1,j]))
                f.write("vertex {} {} {}\n".format(XX[i+1,j+1], YY[i+1,j+1], ZZ[i+1,j+1]))
                f.write("endloop\n")
                f.write("endfacet\n")
                f.write("facet normal 0 0 0\n")
                f.write("outer loop\n")
                f.write("vertex {} {} {}\n".format(XX[i,j+1], YY[i,j+1], ZZ[i,j+1]))
                f.write("vertex {} {} {}\n".format(XX[i,j], YY[i,j], ZZ[i,j]))
                f.write("vertex {} {} {}\n".format(XX[i+1,j+1], YY[i+1,j+1], ZZ[i+1,j+1]))
                f.write("endloop\n")
                f.write("endfacet\n")
        f.write("endsolid\n")
        f.close()



        #%% print out G code for 3D printing directly
        import scipy.interpolate
        
        FILAMENT = 1.75   # filament diameter
        DELTAZ = 0.25     # layer height (mm)       should be about half the nozzle size
        DELTAW = 0.25     # distance between 2 walls (mm)         probably reasonable to set to DELTAZ
        WALLS = 3         # number of walls to draw around the object (each offset by 2*DELTAZ)
        DELTAE = 1        # safety wire feed during non-print transitions (mm)
        FEEDRATE = 3000   # printing speed (mm / min)
        FLOWRATE = (DELTAZ/FILAMENT)**2        # rate of wire feeding (mm of thread / mm printed)
                          # Estimated as (diameter_printed / diameter_filament)^2 so total volume is same
        MAXZ = 200        # maximum height supported by printer (mm)
        WIDX = 100        # object length (mm)
        WIDY = 120        # object width (mm)
        WIDZ = 160        # object height (mm)
        
        # rescale and shuffle layer coordinates
        XX = (Y-np.min(Y))*WIDX/(np.max(Y)-np.min(Y))+DELTAW*WALLS
        YY = (Z-np.min(Z))*WIDY/(np.max(Z)-np.min(Z))+DELTAW*WALLS
        ZZ = (X-np.min(X))*WIDZ/(np.max(X)-np.min(X))
        
        # resampler for layers
        LX = scipy.interpolate.interp1d(ZZ[:,0], XX, axis=0, fill_value='extrapolate')
        LY = scipy.interpolate.interp1d(ZZ[:,0], YY, axis=0, fill_value='extrapolate')
        
        # setup
        epos = 0.0
        f = open('x0_2_26.g', 'w')
        f.write('G21 G90 G92 E0 G28\n')
        f.write('M83 M104 S195 M301 P39.04 I2.55 D149.66 M109 S195 M106 S127\n')
        f.write('G0 F{:f} Z{:f}\n'.format(FEEDRATE, DELTAZ))
        
        # loop through layers
        layers = np.linspace(0.0, WIDZ, int(WIDZ/DELTAZ)+1)
        for z in layers:
            xo = LX(WIDZ-z)  # flip layer order to build backwards
            yo = LY(WIDZ-z)
            for offset in range(WALLS):
                # push each point to the left (outside for us) by dl along the path
                dl = 0.5*offset*DELTAW
                x = np.array(xo)[:-1]
                y = np.array(yo)[:-1]
                lx = len(x)
                for j in range(lx):
                    dx1 = xo[(j+1)%lx]-xo[j]
                    dy1 = yo[(j+1)%lx]-yo[j]
                    l1 = np.sqrt(dx1*dx1+dy1*dy1)
                    dx2 = xo[j]-xo[(j-1)%lx]
                    dy2 = yo[j]-yo[(j-1)%lx]
                    l2 = np.sqrt(dx2*dx2+dy2*dy2)
                    x[j] -= dl*(dy1/l1+dy2/l2)
                    y[j] += dl*(dx1/l1+dx2/l2)
                # print the whole loop
                x0 = x[0]
                y0 = y[0]
                f.write('G1 E{:f} G0 Z{:f} G0 X{:f} Y{:f} G1 E{:f}\n'.format(epos-DELTAE, z+DELTAZ, x0, y0, epos))
                for i in list(range(1,len(x)))+[0,]:
                    dx = x[i]-x0
                    dy = y[i]-y0
                    x0 = x[i]
                    y0 = y[i]
                    l = np.sqrt(dx*dx+dy*dy)
                    epos += FLOWRATE*l
                    f.write('G1 X{:f} Y{:f} E{:f}\n'.format(x0, y0, epos))
        f.write('G0 Z{:f}\n'.format(MAXZ))
        f.close()
