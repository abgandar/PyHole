# -*- coding: utf-8 -*-
"""
Compute an image using an Oldenburg metric.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole.scene import Scene

scene = Scene()

from pyhole.metric import Oldenburg
from pyhole.metric.hr import Interpolated
scene.coordinates = 'spherical'
scene.hr_fact = 30
Oldenburg.CUTOFF_FACTOR = 2.0
scene.metric = Oldenburg(Interpolated(os.path.join(scene.basePath, scene.dataPath, "metw_al0.40_oh0.20a2.48000m2.49309j6.26882.npz"), scene.hr_fact))


#scene.r_circ = 15.0*scene.metric.f.m_ADM
scene.theta = np.deg2rad(90.0)

scene.size = (1024,1024)
scene.zoom = ((-1.0,1.0),(-1.0,1.0))

#scene.size = (1024,-1)
#scene.zoom = ((-0.340,-0.142), (-0.086,0.086))

#scene.tolerance = scene.HIGH_ACCURACY
scene.tolerance = scene.MEDIUM_ACCURACY
#scene.tolerance = scene.LOW_ACCURACY

scene.projection = 'Equirectangular'
scene.fov = np.arctan(10.0/15.0)
scene.my_suffix = ''


scene.ring = 0.0
scene.grid = 18
scene.background = 'bg-color.png'
#scene.background = 'bg-color-pastel.png'
scene.shadowColor = [0.0, 0.0, 0.0, 1.0]
scene.floaterColor = [1.0, 0.0, 1.0, 1.0]
scene.errorColor = [0.0, 1.0, 1.0, 1.0]

### Main code
#

# 1a) Try to load a previously generated data file
if False or not scene.load():
    scene.raytrace_GPU(device="GPUCPU", platform_id=0)

# This is protecting the following code from child processes spawned in the raytrace_parallel command above
if __name__=='__main__':
    # 2) Save the image and additional information in various different ways
    #scene.saveImage('test.png')
    #scene.saveEHImage('test-EH.png')
    #scene.saveDescription('test.txt')
    #scene.saveWebGLTexture('test-tex.png')
    #scene.saveOverlay('test-ovl.png')

    # 3) Show interactive display
    scene.show(False)
