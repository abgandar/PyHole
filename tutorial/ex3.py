import numpy as np
from gr_pyhole.scene import Scene

scene = Scene()
scene.metric = '5'
scene.theta = np.deg2rad(90.0)
scene.size = (1024,1024)
#scene.zoom = ((0.1,0.6), (0.3,0.6))
scene.tolerance = scene.MEDIUM_ACCURACY

scene.load()    # load pre-computed data file
scene.show()    # start interactive display

