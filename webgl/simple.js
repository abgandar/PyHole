/*
   Copyright 2015 - 2016 Alexander Wittig, Jai Grover

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

function clamp( x, xmin, xmax )
{
    return Math.min( Math.max( x, xmin ), xmax );
}

// WGL viewer object created inside canvas "id"
WGLViewer = function( canvas_id )
{
    // Get A WebGL context
    this.canvas = document.getElementById( canvas_id );
    this.canvas.width  = this.canvas.clientWidth;
    this.canvas.height = this.canvas.clientHeight;
    try
    {
        this.gl = this.canvas.getContext( 'webgl', { preserveDrawingBuffer: true } );    // Firefox needs the second argument in order for the image to be save-able
    }
    catch( x ) { this.gl = null; }
    if( this.gl == null )
    {
        try
        {
            this.gl = this.canvas.getContext( 'experimental-webgl', { preserveDrawingBuffer: true } );    // Firefox needs the second argument in order for the image to be save-able
        }
        catch( x ) { throw 'Your browser does not seem to support WebGL.'; }
    }

    // set up various listeners, using event listener on window to make sure we don't overwrite someone else's
    window.addEventListener( 'resize', (function( ) { window.requestAnimationFrame( this.boundRedraw ); }).bind( this ) );  // asynchronously repaint when resizing
    this.canvas.onmousedown = this.canvas.ontouchstart = this.mousedown.bind( this );
    this.canvas.onmouseup = this.canvas.ontouchend = this.mouseup.bind( this );
    canvas.addEventListener( 'dragenter', this.dragenter.bind( this ), false );     // drag and drop listeners
    canvas.addEventListener( 'dragleave', this.dragleave.bind( this ), false );
    canvas.addEventListener( 'dragover', this.dragover.bind( this ), false );
    canvas.addEventListener( 'drop', this.drop.bind( this ), false );
    // pre-bind some commonly used functions
    this.boundRedraw = this.redraw.bind( this );
    this.boundMM = this.mousemove.bind( this );

    // setup GLSL shaders
    var vshader            = this.gl.createShader( this.gl.VERTEX_SHADER ),
        fshader360         = this.gl.createShader( this.gl.FRAGMENT_SHADER ),
        vshaderSource      = document.getElementById( 'blackhole-vertex-shader' ).text,
        fshaderSource360   = document.getElementById( 'blackhole-fragment-shader' ).text;
    this.gl.shaderSource( vshader, vshaderSource );
    this.gl.compileShader( vshader );
    if( !this.gl.getShaderParameter( vshader, this.gl.COMPILE_STATUS ) )
        throw 'Error compiling vertex shader: ' + this.gl.getShaderInfoLog( vshader );
    this.gl.shaderSource( fshader360, fshaderSource360 );
    this.gl.compileShader( fshader360 );
    if( !this.gl.getShaderParameter( fshader360, this.gl.COMPILE_STATUS ) )
        throw 'Error compiling 360 VR fragment shader: ' + this.gl.getShaderInfoLog( fshader360 );

    // provide coordinates for the rectangle. Texture coordinates are derived in vertex shader.
    var buffer = this.gl.createBuffer( );
    this.gl.bindBuffer( this.gl.ARRAY_BUFFER, buffer );
    this.gl.bufferData( this.gl.ARRAY_BUFFER, new Float32Array(
        [ -1.0, -1.0,
           1.0, -1.0,
          -1.0,  1.0,
           1.0,  1.0 ] ), this.gl.STATIC_DRAW );

    // Create a textures and set the parameters so we can render any size image.
    this.blackhole_texture = this.gl.createTexture( );
    this.gl.activeTexture( this.gl.TEXTURE0 );
    this.gl.bindTexture( this.gl.TEXTURE_2D, this.blackhole_texture );
    this.gl.texParameteri( this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE );
    this.gl.texParameteri( this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE );
    this.gl.texParameteri( this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST );
    this.gl.texParameteri( this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST );

    this.background_texture = this.gl.createTexture( );
    this.gl.activeTexture( this.gl.TEXTURE1 );
    this.gl.bindTexture( this.gl.TEXTURE_2D, this.background_texture );
    this.gl.texParameteri( this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE );
    this.gl.texParameteri( this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE );
    this.gl.texParameteri( this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR );
    this.gl.texParameteri( this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR );

    // 360 VR view program
    this.program360 = this.gl.createProgram( );
    this.gl.attachShader( this.program360, vshader );
    this.gl.attachShader( this.program360, fshader360 );
    this.gl.linkProgram( this.program360 );
    this.gl.useProgram( this.program360 );

    var positionLoc = this.gl.getAttribLocation( this.program360, 'a_position' );
    this.gl.enableVertexAttribArray( positionLoc );
    this.gl.vertexAttribPointer( positionLoc, 2, this.gl.FLOAT, false, 0, 0 );
    this.gl.uniform1i( this.gl.getUniformLocation( this.program360, 'u_blackhole' ), 0 );
    this.gl.uniform1i( this.gl.getUniformLocation( this.program360, 'u_background' ), 1 );

    // load program and set variable locations
    this.gl.useProgram( this.program360 );
    this.fovLoc = this.gl.getUniformLocation( this.program360, 'u_fov' );          // The (half) field of view
    this.textureSizeLoc = this.gl.getUniformLocation( this.program360, 'u_bhsize' );   // size of the texture image
    this.smLoc = this.gl.getUniformLocation( this.program360, 'u_sm' );        // The matrix describing where the background is centered
    this.vmLoc = this.gl.getUniformLocation( this.program360, 'u_vm' );        // The view matrix describing where we look at

    // images initially used for the textures
    this.blackhole_image = new Image( );
    this.blackhole_image.onload = (function( ) { this.updateTexture( this.gl.TEXTURE0, this.blackhole_image ); this.redraw( ); }).bind( this );

    this.background_image = new Image( );
    this.background_image.onload = (function( ) { this.updateTexture( this.gl.TEXTURE1, this.background_image ); this.redraw( ); }).bind( this );

    this.logo_image_ESAACT = new Image( );
    this.logo_image_ESAACT.src = 'esaactlogo.png'

    this.logo_image_ESA = new Image( );
    this.logo_image_ESA.src = 'esalogo.png'

    this.logo_image_ACT = new Image( );
    this.logo_image_ACT.src = 'actlogo.png'

    this.restoreState( );
};

// the default state
WGLViewer.prototype.defaultState = {
    blackhole_image: 'textures/3vr.png',
    background_image: 'backgrounds/estec-flags.jpg',
    zoom: 1.0,
    view: [0.5, 0.5],
    vm: [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ],
    shift: [ 0.0, 0.0 ],
    sm: [ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ],
    fov360: [0.25, 0.5],
};

WGLViewer.prototype.getState = function( )
{
    return { blackhole_image: this.blackhole_image.src,
             background_image: this.background_image.src,
             zoom: this.zoom,
             view: this.view,
             shift: this.shift,
             fov360: this.fov360, };
};

WGLViewer.prototype.restoreState = function( state )
{
    if( !state )
        state = this.defaultState;
    this.canvas.parentElement.classList.remove( 'complete' );
    this.blackhole_image.src = state.blackhole_image;
    this.background_image.src = state.background_image;
    this.zoom = state.zoom;
    this.view = state.view;
    this.shift = state.shift;
    this.fov360 = state.fov360;

    this.redraw( );
};

// update all settings that affect the view in the current program, adjusting them as needed. Then redraw the WebGL view.
WGLViewer.prototype.redraw = function( )
{
    var w = this.canvas.width,
        h = this.canvas.height;

    // update canvas size if layout changed
    if( w != this.canvas.clientWidth || h != this.canvas.clientHeight )
    {
        w = this.canvas.width = this.canvas.clientWidth;
        h = this.canvas.height = this.canvas.clientHeight;
        this.gl.viewport( 0.0, 0.0, w, h );
    }

    // compute shift matrix
    this.shift[0] = this.shift[0]%1.0;
    this.shift[1] = this.shift[1]%1.0;
    var sp = Math.sin( 2.0*Math.PI*this.shift[0] ),
        cp = Math.cos( 2.0*Math.PI*this.shift[0] ),
        st = Math.sin( 2.0*Math.PI*this.shift[1] ),
        ct = Math.cos( 2.0*Math.PI*this.shift[1] );
    sm = [ cp*ct, sp*ct, st, -sp, cp, 0.0, -cp*st, -sp*st, ct ];   // column major!

    // compute view matrix
    this.view[0] = this.view[0]%1.0;
    this.view[1] = clamp( this.view[1], 0.0, 1.0 );
    sp = Math.sin( 2.0*Math.PI*this.view[0] );
    cp = Math.cos( 2.0*Math.PI*this.view[0] );
    st = Math.sin( Math.PI*(this.view[1]-0.5) );        // we want equator in the center
    ct = Math.cos( Math.PI*(this.view[1]-0.5) );
    vm = [ cp*ct, sp*ct, st, -sp, cp, 0.0, -cp*st, -sp*st, ct ];   // column major!

    var aspectRatio = (w > h) ? [ 1.0, h/w ] : [ w/h, 1.0 ],
        fov = [ 2.0*Math.PI*this.zoom*aspectRatio[0]*this.fov360[0], Math.PI*this.zoom*aspectRatio[1]*this.fov360[1] ];

    this.gl.uniform2f( this.textureSizeLoc, this.blackhole_image.width, this.blackhole_image.height );
    this.gl.uniform2fv( this.fovLoc, fov );
    this.gl.uniformMatrix3fv( this.smLoc, false, sm );
    this.gl.uniformMatrix3fv( this.vmLoc, false, vm );

    // actually trigger a redraw
    this.gl.drawArrays( this.gl.TRIANGLE_STRIP, 0, 4 );
};

WGLViewer.prototype.setZoomFactor = function( zoom )
{
    this.zoom = 1.0/zoom;       // zooming in means smaller field of view
    window.requestAnimationFrame( this.boundRedraw );   // be nice and redraw asynchronously
};

WGLViewer.prototype.mousedown = function( e )
{
    if( e.type == 'touchstart' )
    {
        e.preventDefault( );
        this.button = (e.touches.length == 1) ? 0 : 3;
        e = e.touches[0];
    }
    else
    {
        this.button = e.shiftKey ? 3 : 0;
    }
    this.mouse_x = e.clientX;
    this.mouse_y = e.clientY;
    this.canvas.onmousemove =  this.canvas.ontouchmove = this.boundMM;
};

WGLViewer.prototype.mouseup = function( e )
{
    this.canvas.onmousemove = this.canvas.ontouchmove = null;
    this.button = null;
};

WGLViewer.prototype.mousemove = function( e )
{
    if( e.type == 'touchmove' )
    {
        e.preventDefault( );
        e = e.touches[0];
    }
    dx = (e.clientX - this.mouse_x)/this.canvas.width;
    dy = (e.clientY - this.mouse_y)/this.canvas.height;
    if( this.button == 0 )
    {
        dx *= this.zoom;
        dy *= this.zoom;
        this.view = [ this.view[0]-dx, this.view[1]+dy ];
    }
    else
    {
        dx *= 0.5;     // heuristic factor
        dy *= 0.5;
        this.shift = [ this.shift[0]-dx, this.shift[1]-dy ];
    }
    window.requestAnimationFrame( this.boundRedraw );   // be nice and redraw asynchronously
    this.mouse_x = e.clientX;
    this.mouse_y = e.clientY;
};

WGLViewer.prototype.dragleave = function( e )
{
    this.canvas.classList.remove( 'dragdrop' );
};

WGLViewer.prototype.dragenter = function( e )
{
    this.canvas.classList.add( 'dragdrop' );
};

WGLViewer.prototype.dragover = function( e )
{
    e.preventDefault( );
};

WGLViewer.prototype.drop = function( e )
{
    e.preventDefault( );
    this.canvas.classList.remove( 'dragdrop' );
    if( e.dataTransfer.files.length > 0 && /^image\/.+/.test( e.dataTransfer.files[0].type ) )
        this.loadBackgroundFile( e.dataTransfer.files );
};

// update texture but do not automatically redraw!
WGLViewer.prototype.updateTexture = function( texture, image )
{
    if( !image.complete ) return;        // skip incomplete images

    if( (this.canvas.parentElement.className.indexOf( 'complete' ) == -1) && this.background_image.complete && this.blackhole_image.complete )
        this.canvas.parentElement.classList.add( 'complete' );

    // copy the image content into WebGL buffer
    this.gl.activeTexture( texture );
    this.gl.pixelStorei( this.gl.UNPACK_FLIP_Y_WEBGL, true );
    this.gl.pixelStorei( this.gl.UNPACK_COLORSPACE_CONVERSION_WEBGL, this.gl.NONE );
    this.gl.texImage2D( this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, image );
    if( this.gl.getError( ) != this.gl.NO_ERROR )
        alert( "There was an error loading the image as a texture, probably the image is too large for this WebGL implementation. Please try a different browser such as Google's Chrome, Mozilla's Firefox, Opera, Apple's Safari, or Microsoft's Edge." );
};

WGLViewer.prototype.loadBackgroundFile = function( files )
{
    if( files.length < 1 ) return;

    // get the file content into background image (triggers its onload automatically to update texture)
    this.canvas.parentElement.classList.remove( 'complete' );
    var reader = new FileReader( );
    reader.onload = (function( event ) { this.background_image.src = event.target.result; }).bind( this );
    reader.readAsDataURL( files[0] );
};

WGLViewer.prototype.loadBackgroundURL = function( url )
{
    // get the file content into background image (triggers its onload automatically to update texture)
    this.canvas.parentElement.classList.remove( 'complete' );
    this.background_image.src = url;
};

WGLViewer.prototype.loadBlackholeURL = function( url )
{
    // get the file content into background image (triggers its onload automatically to update texture)
    this.canvas.parentElement.classList.remove( 'complete' );
    this.blackhole_image.src = url;
};

