<!--
   Copyright 2015 - 2016 Alexander Wittig, Jai Grover

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-->

<!DOCTYPE html>
<html>
<head>
    <title>ESA ACT blackhole visualization</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <link rel="stylesheet" type="text/css" href="simple.css"/>
    <script type="text/javascript" src="simple.js"></script>
    <script type="text/javascript">
        // Set the URL of the image files for the celestial background and the black hole textures here
        WGLViewer.prototype.defaultState.blackhole_image = 'schwarzschild-texture.png';
        WGLViewer.prototype.defaultState.background_image = 'bg-color-grid-ER.png';
    </script>
    
    <script id="blackhole-vertex-shader" type="x-shader/x-vertex">
        attribute vec2 a_position;
        varying vec2 v_texCoord;    // passed on to the fragment shader

        void main( )
        {
            gl_Position = vec4( a_position, 0, 1 );
            v_texCoord = a_position;
        }
    </script>

    <script id="blackhole-fragment-shader" type="x-shader/x-fragment">
        precision highp float;
        #define M_PI 3.14159265358979323846

        uniform sampler2D u_blackhole;
        uniform sampler2D u_background;
        uniform vec2 u_fov;         // "field of view" (really the size of the image in the tangent plane)
        uniform vec2 u_bhsize;      // size of the blackhole texture
        uniform mat3 u_vm;          // view matrix determining where we look
        uniform mat3 u_sm;          // shift matrix determining where the background goes

        varying vec2 v_texCoord;    // passed in from the vertex shader

        // round a number
        float round( float x )
        {
            return x-floor( x ) < 0.5 ? floor( x ) : ceil( x );
        }

        // extract 2 high precision floats from image components
        vec2 unpack( vec4 x )
        {
            return (x.zx + x.wy/256.0)*(255.0/256.0);
        }

        // Project fragment onto correct part of black hole sphere
        vec2 unproject(  )
        {
            vec2 t = u_fov*v_texCoord;
            vec3 x = vec3( 4.0 - t.x*t.x - t.y*t.y, 4.0*t );           // (non-normalized) point on tangent plane, stereographic projection
            x = normalize( u_vm*x );                                   // point on unit sphere rotated to viewing position
            return vec2( mod( atan( x.y, x.x )/(2.0*M_PI), 1.0 ), 1.0 - acos( x.z )/M_PI );   // texture coordinates of that point on unit sphere, adjust for theta=0 being at y=1 in the texture
        }

        // Look up angles from blackhole mapping, manually performing linear interpolation with modulo for phi
        vec2 interpolate( vec2 stex )
        {
            vec3 delta = vec3( vec2( 1.0, 1.0 )/u_bhsize, 0.0 );
            vec2 rem = stex*u_bhsize - 0.5;
            stex = floor( rem );
            rem -= stex;
            stex /= u_bhsize;
            // look up unpacked high precision value of 4 neighboring pixels
            vec2 t1 = unpack( texture2D( u_blackhole, stex ) );
            vec2 t2 = unpack( texture2D( u_blackhole, stex+delta.xz ) );
            vec2 t3 = unpack( texture2D( u_blackhole, stex+delta.zy ) );
            vec2 t4 = unpack( texture2D( u_blackhole, stex+delta.xy ) );
            // ignore blackhole pixels fro purpose of interpolation
            t2 = (t2 == vec2( 0.0, 0.0 )) ? t1 : t2;
            t3 = (t3 == vec2( 0.0, 0.0 )) ? t1 : t3;
            t4 = (t4 == vec2( 0.0, 0.0 )) ? t1 : t4;
            // ensure that phi component is always closest to that of t1 for interpolation to work
            t2.x = t2.x-round(t2.x-t1.x);
            t3.x = t3.x-round(t3.x-t1.x);
            t4.x = t4.x-round(t4.x-t1.x);
            // linearly interpolate unless the main pixel was in blackhole
            return (t1 == vec2( 0.0, 0.0 )) ? t1 : (1.0-rem.x)*(1.0-rem.y)*t1 + (rem.x)*(1.0-rem.y)*t2 + (1.0-rem.x)*(rem.y)*t3 + (rem.x)*(rem.y)*t4;
        }

        // Project a second time onto equirectangular background sphere image
        vec2 equirect( vec2 tex )
        {
            vec3 x = u_sm*vec3( sin( tex.y*M_PI )*cos( tex.x*M_PI*2.0 ), -sin( tex.y*M_PI )*sin( tex.x*M_PI*2.0 ), cos( tex.y*M_PI ) );  // rotated point on unit sphere, minus in y is so textures are not mirrored
            return vec2( mod( atan( x.y, x.x )/(2.0*M_PI), 1.0 ), 1.0 - acos( x.z )/M_PI );                                        // texture coordinates, theta = 0 is the top of the image, i.e. y=1.0 in texture coordinates
        }

        void main( )
        {
            vec2 stex = unproject( );           // unproject onto viewing directions sphere
            vec2 tex = interpolate( stex );     // interpolate resulting direction of the ray
            stex = equirect( tex );             // look up the corresponding point in the background image

            // black hole is black, otherwise do second lookup in background texture
            gl_FragColor = (tex == vec2( 0.0, 0.0 )) ? vec4( 0.0, 0.0, 0.0, 1.0 ) : texture2D( u_background, stex );
        }
    </script>
</head>

<body onload="wgl = new WGLViewer( 'canvas' );">
    <div id="container" class="vr360">
        <canvas id="canvas" width="786" height="786"></canvas>
        <a href="https://www.esa.int/" title="The European Space Agency" target="_blank"><img id="logoESA" alt="ESA Logo" src="esalogo.png"/></a>
        <a href="https://www.esa.int/act" title="The ESA Advanced Concepts Team" target="_blank"><img id="logoACT" alt="ACT Logo" src="actlogo.png"/></a>
        <img id="spinner" src="ajax-loader.gif" alt="Loading..."/>
    </div>
    <div id="attribution">Images and code &copy; 2016 <a href="http://www.esa.int/act">ESA Advanced Concepts Team</a> (Alexander Wittig, Jai Grover).</div>
</body>
</html>
