# -*- coding: utf-8 -*-
"""
A simple example to compute a small image from scratch.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_yhole.scene import Scene
from gr_pyhole.metric import SphericalMetric

class MyMetric(SphericalMetric):
    """Wormhole."""
    ID = 'WH'       #: short identifier of this metric; used e.g. in file names

    def __init__(self, r0 = 1.0):
        """Initialize your metric here by storing various metric parameters in self"""
        self.r0 = r0
        super(MyMetric, self).__init__()    # call our super-class initializer to make sure its code, whatever it may be, is run as well

    def __str__(self):
        """Show human readable summary of the current setup."""
        res = 'Metric: Wormhole\n'
        res += 'r0: {}\n'.format(self.r0)
        res += super(MyMetric,self).__str__();
        return res

    def update(self):
        """Compute the relevant components of the Cartesian metric at the current point and store them in the attributes."""

        l = self.x[1]
        theta = self.x[2]
        # correct various coordinates. Done differently for scalars and vectors.
        if isinstance(l, np.ndarray):
            # Hack to prevent divisions by zero: if we are too close to the coordinate singularity enforce minimum size for theta
            cond = theta<1e-12
            theta[cond] = 1e-12
        else:
            if theta<1e-12: theta = 1e-12

        h = l*l + self.r0*self.r0

        self.tt = -1.0
        self.rr = 1.0
        self.thth = 1.0/h
        self.pp = 1.0/(h*np.sin(theta)**2)
        self.tp = 0.0

        # note dr_ is the same as dl_
        self.dr_tt = 0.0
        self.dr_rr = 0.0
        self.dr_thth = -2.0*l/(h*h)
        self.dr_pp = -2.0*l/(h*h*np.sin(theta)**2)
        self.dr_tp = 0.0

        self.dth_tt = 0.0
        self.dth_rr = 0.0
        self.dth_thth = 0.0
        self.dth_pp = -2.0*np.cos(theta)/(h*np.sin(theta)**3)
        self.dth_tp = 0.0

        return False





scene = Scene()

from pyhole.metric import OBWormhole
from pyhole.metric.hr import Interpolated, Schwarzschild
scene.coordinates = 'spherical'
scene.hr_fact = 10

scene.metric = OBWormhole(Interpolated(os.path.join(scene.basePath, scene.dataPath, "rotwwx_r0=0.5_w0=1.0_153x51o6.npz"), scene.hr_fact))

# Non-rotating wormhole (using their metric setup)
#func = Schwarzschild(0.0)
#func.r0 = 1.0
#scene.metric = OBWormhole(func)

# non-rotating Wormhole (using explicit metric)
#scene.metric = MyMetric(1.0)

scene.r = 20.0*scene.metric.r0
#scene.r = 15.0*scene.metric.r0

#scene.r_circ = 15.0*scene.metric.f.m_ADM
scene.theta = np.deg2rad(90.0)

#scene.size = (100,100)
scene.size = (1024,1024)
#scene.size = (32,32)
scene.zoom = ((-1.0,1.0),(-1.0,1.0))

#scene.size = (1024,-1)
#scene.zoom = ((-0.340,-0.142), (-0.086,0.086))

#scene.tolerance = scene.HIGH_ACCURACY
scene.tolerance = scene.MEDIUM_ACCURACY
#scene.tolerance = scene.LOW_ACCURACY

scene.projection = 'Equirectangular'
scene.fov = np.arctan(10.0/15.0)
scene.my_suffix = ''


scene.ring = 0.05
scene.grid = 18
scene.background = 'bg-color.png'
#scene.background = 'bg-color-pastel.png'
scene.shadowColor = [0.0, 0.0, 0.0, 1.0]
scene.floaterColor = [1.0, 0.0, 1.0, 1.0]
scene.errorColor = [0.0, 1.0, 1.0, 1.0]

### Main code
#

# 1a) Try to load a previously generated data file
if True or not scene.load():
#    scene.raytrace()
#    scene.raytrace_parallel(__name__, NP=2)
#    scene.raytrace_GPU(device="GPUCPU", platform_id=0)
    raise Exception('noooo! file not found.')

# This is protecting the following code from child processes spawned in the raytrace_parallel command above
if __name__=='__main__':
    # 2) Save the image and additional information in various different ways
    #scene.saveImage('test.png')
    #scene.saveEHImage('test-EH.png')
    #scene.saveDescription('test.txt')
    #scene.saveWebGLTexture('test-tex.png')
    #scene.saveOverlay('test-ovl.png')

    # highlight the "other side" (maybe add this somewhere else)
    mask = scene.i.data[:,:,4]<0.0
    scene.i.image[mask,:] += (np.array([0.9, 0.8, 0.8]) - scene.i.image[mask,:])*0.75
    scene.saveImage()

    # 3) Show interactive display
    scene.show(False)
