# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 10:16:48 2015

@author: Joerg H Mueller
"""

# math routines
import os.path
import numpy as np
# timing routines
from time import clock
# our own routines
from metrics import Kerr, Schwarzschild, Flat, Interpolated
from cameras import StereographicCamera
from display import Display
from config import Configuration

if __name__ == '__main__':
    integrators = [ 'dop853', 'dopri5', 'vode' ]

    directly = True

    g = Kerr(2.00, 1.0)

    if directly:
        num_shadow_samples = 1000
        betas = np.linspace(0, 2*np.pi, num_shadow_samples, endpoint=False)
        alphas = np.zeros(num_shadow_samples)
        cam = StereographicCamera(g, 15.0, np.pi/2, np.pi/4)
        for i, beta in enumerate(betas):
            print("shadow {0}: {1}".format(i, beta))
            alphas[i] = cam.getShadow(beta)

    suffix = g.getID()
    filename = os.path.join(Configuration.data_path, 'metric-{0}.npz'.format(suffix))

    g = Interpolated(filename)
    
    fov = np.pi / 4
    cam = StereographicCamera(g, 15.0, np.pi/2, fov)
    
    for integrator in integrators:
        cam.prop.integrator = integrator
        if directly:
            num_samples = 10000
            indices = np.random.randint(0, num_shadow_samples, num_samples)
            b = betas[indices]
            a1 = np.random.sample(num_samples) * alphas[indices]
            a2 = np.random.sample(num_samples) * (fov - alphas[indices]) + alphas[indices]
            t0 = clock()
            for alpha, beta in zip(a1, b):
                cam.prop.propagate(cam.getIC(alpha, beta))
            t1 = clock()
            print("Comptuation time for {0} blackhole samples with {1}: {2} s".format(num_samples, integrator, t1-t0))
            t0 = clock()
            for alpha, beta in zip(a2, b):
                cam.prop.propagate(cam.getIC(alpha, beta))
            t1 = clock()
            print("Comptuation time for {0} non-blackhole samples with {1}: {2} s".format(num_samples, integrator, t1-t0))
        else:
            t0 = clock()
            cam.generateDirections()
            t1 = clock()
            print("Comptuation time for {0}: {1} s".format(integrator, t1-t0))
            cam.generateImage()
            cam.saveAll(additional=integrator)

