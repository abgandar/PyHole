# -*- coding: utf-8 -*-

import numpy as np

def fixup_directions(d):
    #...
    # New order: theta, phi, lambda, x, variation, total variation, stuff
    # Old: theta, phi, lambda, x, total var, var, stuff (cartesian, len>=13)
    # Very Old: theta, phi, x, total var, var, stuff, lambda (cartesian, len>=13)
    nx, ny = d.shape[0:2]
    extra = max(0, d.shape[2]-13)
    l = 14+extra
    res = np.zeros((nx,ny,l))
    cart = d.shape[2] >= 13

    theta = d[:,:,0]
    phi = d[:,:,1]
    mask_in = (theta <= -16.0)
    mask_in_old = (phi == -1.0)
    mask_float = phi == -2.0
    mask_err = phi == -3.0
    theta[mask_float] = -100.0
    phi[mask_float] = 2.0
    theta[mask_err] = -100.0
    phi[mask_err] = 3.0
    theta[mask_in] = (-16.0 - theta[mask_in])-2*np.pi
    phi[mask_in] = (-16.0 - phi[mask_in])-2*np.pi
    theta[mask_in_old] = -1.0   # didn't record position yet
    phi[mask_in_old] = -1.0

    res[:,:,0] = theta
    res[:,:,1] = phi

    res[:,:,2] = d[:,:,-1]  # lambda
    res[:,:,3:11] = d[:,:,2:10]  # x
    if cart:
        res[:,:,11] = d[:,:,11]  # variation
        res[:,:,12] = d[:,:,10]  # total variation
    else:
        res[:,:,11] = d[:,:,5]  # variation

    # no final null condition violation (13) in old files
    res[:,:,13] = -1.0

    res[:,:,14:l] = d[:,:,12:-1]    # extra stuff

    return res

def fixup_dataset(d):
    if d.upper() == 'KERR':
        return 'Kerr'
    elif d.upper() == 'FLAT':
        return 'Flat'
    elif d.upper() == 'SCHWARZSCHILD':
        return 'Schwarzschild'
    elif d.upper() == 'EFLAT':
        return 'HRFlat'
    elif d.upper() == 'ESCHWARZSCHILD':
        return 'HRSchwarzschild'
    else:
        return d

def fixup_projection(p):
    if p.upper() == 'A':
        return 'Equirectangular'
    elif p.upper() == 'S':
        return 'Stereographic'
    elif p.upper() == 'G':
        return 'Gnomonic'
    else:
        return p

def fixup_coordinates(c):
    if c:
        return 'Cartesian'
    else:
        return 'spherical'

def fixup_filename(f):
    return f.replace('-A-', '-Equirectangular-').\
             replace('-G-', '-Gnomonic-').\
             replace('-S-', '-Stereographic-').\
             replace('-A.npz', '-Equirectangular.npz').\
             replace('-G.npz', '-Gnomonic.npz').\
             replace('-S.npz', '-Stereographic.npz')

def convert(fn):
    import os
    data = dict(np.load(fn))

    if 'imageData' in data.keys():
        return      # new type data file, ignore

    if 'directions' in data.keys():
        data['imageData'] = fixup_directions(data['directions'])
        del data['directions']
    if 'dataset' in data.keys():
        data['metric'] = fixup_dataset(data['dataset'][()])
        del data['dataset']
    if 'projection' in data.keys():
        data['projection'] = fixup_projection(data['projection'][()])
    if 'cartesian' in data.keys():
        data['coordinates'] = fixup_coordinates(data['cartesian'][()])
        del data['cartesian']
    os.rename(fn, fn+".bak")
    np.savez(fixup_filename(fn), **data)


if __name__ == "__main__":
    import sys
    convert(sys.argv[1])
