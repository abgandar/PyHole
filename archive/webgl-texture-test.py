# -*- coding: utf-8 -*-
"""
Created on Mon Jun  6 18:33:26 2016

@author: Alexander Wittig
"""
import numpy as np
from matplotlib.pyplot import imsave, imread
from PIL import Image

def generateWebGL(d):
    h, w, dummy = d.shape
    y = d[:,:,0:2]/np.pi
    y[:,:,1] *= 0.5

    img = np.empty((h, w, 4), dtype=np.uint8)
    y = np.clip(y, +0.0, (2.0**16 - 1)/(2.0**16))    # clips all errors to coordinate (0, 0), which must be black in background image
    yy = y      # this is the data we want to compare against
    y = (y*(2.0**16)).astype(np.uint32)
    img[:,:,0::2] = (y[:,:,:]//(2**8)).astype(np.uint8)
    img[:,:,1::2] = (y[:,:,:] - img[:,:,0::2]*(2**8)).astype(np.uint8)

    pil_img = Image.fromarray(img, 'RGBA')
    pil_img.save('texture.png', 'png')

# Pyplot image saving is broken
#    img[:,:,3] = np.clip(img[:,:,3], 1, 255)    # pyplot is idiotic in that it does not respect the data passed in. Instead it changes the value of fully transparent pixels.
#    imsave('texture.png', img, vmin=0, vmax=255)

    ogl = imread('texture.png')
    z = (255.0/256.0)*(ogl[:,:,0::2] + ogl[:,:,1::2]/256.0)
    diff = z-yy
    print("Maximum error in data: ", np.max(np.abs(diff)))

generateWebGL(scene.c.directions)


def testcase():
    x = np.zeros((5,5,4), dtype=np.uint8)

    imsave('test.png', x)
    y = imread('test.png')
    print(np.max(abs(x-y)))

    pilx = Image.fromarray(x)
    pilx.save('testPIL.png','png')
    pily = Image.open('testPIL.png')
    yy = np.array(pily)
    print(np.max(abs(x-yy)))
