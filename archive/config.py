import platform
from pyhole.scene import Scene

# Automatic configuration of paths depending on the machine

if platform.node() == 'sumin':
    # Joerg
    Scene.basePath = '../../../../../ownCloud/Blackholes/PyHole'
elif platform.node() == 'LPT-TEC-2199':
    # Alex ESA Work
    Scene.basePath = '../../../../ownCloud/Blackholes/PyHole'
elif platform.node() == 'Luftikus.local':
    # Alex MB Air
    Scene.basePath = '../../../ownCloud/Blackholes/PyHole'
elif platform.node() == 'DSK-TEC-3412':
    # ESA GPU Desk
    Scene.basePath = '../../../ownCloud/Blackholes/PyHole'
elif platform.node() == 'LPT-TEC-2200':
    # Jai's ESA computer
    Scene.basePath = '../../../../../../ownCloud/Blackholes/PyHole'
elif platform.node() == 'Jais-MacBook-Pro.local' or platform.node() == 'Jais-MBP.home' or platform.node() == 'dhcp-10-176-42-122.estec.esa.int':
    # Jai's home computer
    Scene.basePath = '../../../../../../ownCloud/Blackholes/PyHole'
