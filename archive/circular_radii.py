# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

@author: Alexander Wittig
"""

# math routines
import numpy as np
import matplotlib.pyplot as pyplot
from matplotlib.ticker import FuncFormatter, MaxNLocator
# our own routines
from metrics import ExponentialInterpolated
# Configuration
from config import Configuration
import os

#datasets = [ 'I', 'II', 'III', 'IV', 'V' ]
#datasets = [ 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII' ]
datasets = [ 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII' ]
r1 = []
r2 = []
g = []

for ind, dataset in enumerate(datasets):
    g.append(ExponentialInterpolated(os.path.join(Configuration.data_path, 'configuration-'+dataset+'.npz')))
    M = 0
    m = 500
    eh = g[ind].EH if g[ind].EH>0 else 0.01
#    for i in np.linspace(eh*1.1, eh*30.0, 100):
    for i in np.linspace(eh*1.01, 5.0, 200):
        try:
            x = g[ind].findCircular(i, normalize=False)
            r = x[1]
            g[ind].setPoint(x)
            residue = g[ind].dr_potential(L=x[7], pt=x[4], pth=x[6])
            if residue<1e-5:
                if r>M: M = r
                if r>0 and r<m: m = r
            else:
                print("large residue ignored")
        except:
            pass
    r1.append(m if m!=500 else np.nan)
    r2.append(M if M!=0 else np.nan)



x = range(len(datasets))

def format_fn(tick_val, tick_pos):
    if int(tick_val) in x:
        return datasets[int(tick_val)]
    else:
        return ''
pyplot.gca().xaxis.set_major_formatter(FuncFormatter(format_fn))
pyplot.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
pyplot.xlabel('Dataset')
pyplot.ylabel('Radius')

pyplot.plot(x, r1)
pyplot.plot(x, r2)


#pyplot.fill_between(r1, r2)
