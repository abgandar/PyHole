# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 19:00:48 2016

@author: alex


    .animated_path {
      stroke-dasharray: 50000;
      stroke-dashoffset: 50000;
      /*animation: dash 30s ease-in-out normal 1 forwards;*/
      animation: dash 1200s linear normal 1 forwards;
    }

    @keyframes dash {
      from {
        stroke-dashoffset: 50000;
      }
      to {
        stroke-dashoffset: 0;
      }
"""

from math import sqrt, sin, cos
import numpy as np


# find the point that is closest to the initial point ignoring the first N points
def diff(res, N=100):
    pos = np.apply_along_axis(lambda x: np.array( [x[1]*sin(x[2])*cos(x[3]), x[1]*sin(x[2])*sin(x[3]), x[1]*cos(x[2]), 3*x[5],x[6]] ), 1, res)
    dist = np.apply_along_axis(lambda x: np.linalg.norm(pos[0]-x), 1, pos)
    ind = N+np.argmin(dist[N:])
    return (ind,dist[ind])

def obj(r):
    res = cam.prop.propagate( getNullIC(g, r), True, 0.01 )[1]
    return diff(res)[1]
