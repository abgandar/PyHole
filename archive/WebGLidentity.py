# -*- coding: utf-8 -*-
"""
Created on Fri Aug 19 18:31:31 2016

Save an identity VR texture from a flat space.

@author: Alexander Wittig
"""

import numpy as np



def saveWebGLTexture(directions,filename):
    """Save a texture suitable for the WebGL viewer.

    :param filename: The file name to save the image to.
    """
    # The pyplot imsave routine is broken for fully transparent pixels, so use PIL as an alternative
    from PIL import Image

    h, w, dummy = directions.shape
    y = directions[:,:,0:2]/np.pi
    y[:,:,1] *= 0.5
    y[y[:,:,0]==0.0,1] = np.clip(y[y[:,:,0]==0.0,1], 2.0**(-16), 1.0);       # when theta is 0.0, ensure phi is non-zero so (0,0) cannot happen for legitimate data. Doesn't change actual coordinates of the points on the sphere!

    img = np.empty((h, w, 4), dtype=np.uint8)
    y = np.clip(y, 0.0, (2.0**16 - 1)/(2.0**16))    # clips everything to a value in [0,1), mapping errors to (0,0)
    y = (y*(2.0**16)).astype(np.uint32)
    img[:,:,0::2] = (y[:,:,:]//(2**8)).astype(np.uint8)
    img[:,:,1::2] = (y[:,:,:] - img[:,:,0::2]*(2**8)).astype(np.uint8)

    pil_img = Image.fromarray(img, 'RGBA')
    pil_img.save(filename, 'png')


R = 10      # observer
r = 60      # celestial sphere
theta_o, phi_o = np.meshgrid( np.linspace(0.0, np.pi, 1024), np.linspace(0.0, 2*np.pi, 2048), indexing='ij' )
st = np.sin(theta_o)
ct = np.cos(theta_o)
sp = np.sin(phi_o)
cp = np.cos(phi_o)
B = st*cp*R
C = R*R-r*r
rho = -B+np.sqrt(B*B-C)
x = -(st*cp*rho+R)
y = st*sp*rho
z = ct*rho
th = np.arccos(z/np.sqrt(x*x+y*y+z*z))%(2.0*np.pi)
phi = (np.arctan2(y, x)+np.pi)%(2.0*np.pi)

directions = np.stack( (th, phi), -1)
saveWebGLTexture(directions,'0vr.png')

