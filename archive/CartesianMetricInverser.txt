gi00: 
 -(1/(E**(2*F0(x,y,z))*n(x,y,z)))
gi01: 
 (y*w(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi02: 
 -((x*w(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)))
gi11: 
 (x**2/E**(2*F1(x,y,z)) + y**2/E**(2*F2(x,y,z)))/(x**2 + y**2) + (x**2*(-1 + \
n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(y**2*w(x,y,z)**2)/(E**(2*F0(x,y,z))*n(x,y,z))
gi22: 
 (x**2/E**(2*F2(x,y,z)) + y**2/E**(2*F1(x,y,z)))/(x**2 + y**2) + (y**2*(-1 + \
n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(x**2*w(x,y,z)**2)/(E**(2*F0(x,y,z))*n(x,y,z))
gi33: 
 E**(-2*F1(x,y,z)) + (z**2*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2)
gi12: 
 ((E**(-2*F1(x,y,z)) - E**(-2*F2(x,y,z)))*x*y)/(x**2 + y**2) + (x*y*(-1 + \
n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x*y*w(x,y,z)**2)/(E**(2*F0(x,y,z))*n(x,y,z))
gi13: 
 (x*z*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2)
gi23: 
 (y*z*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2)
gi001: 
 (2*Derivative(1,0,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) + \
Derivative(1,0,0)(n)(x,y,z)/(E**(2*F0(x,y,z))*n(x,y,z)**2)
gi002: 
 (2*Derivative(0,1,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) + \
Derivative(0,1,0)(n)(x,y,z)/(E**(2*F0(x,y,z))*n(x,y,z)**2)
gi003: 
 (2*Derivative(0,0,1)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) + \
Derivative(0,0,1)(n)(x,y,z)/(E**(2*F0(x,y,z))*n(x,y,z)**2)
gi011: 
 (-2*y*w(x,y,z)*Derivative(1,0,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) - \
(y*w(x,y,z)*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) + \
(y*Derivative(1,0,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi012: 
 w(x,y,z)/(E**(2*F0(x,y,z))*n(x,y,z)) - \
(2*y*w(x,y,z)*Derivative(0,1,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) - \
(y*w(x,y,z)*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) + \
(y*Derivative(0,1,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi013: 
 (-2*y*w(x,y,z)*Derivative(0,0,1)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) - \
(y*w(x,y,z)*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) + \
(y*Derivative(0,0,1)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi021: 
 -(w(x,y,z)/(E**(2*F0(x,y,z))*n(x,y,z))) + \
(2*x*w(x,y,z)*Derivative(1,0,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) + \
(x*w(x,y,z)*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) - \
(x*Derivative(1,0,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi022: 
 (2*x*w(x,y,z)*Derivative(0,1,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) + \
(x*w(x,y,z)*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) - \
(x*Derivative(0,1,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi023: 
 (2*x*w(x,y,z)*Derivative(0,0,1)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) + \
(x*w(x,y,z)*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) - \
(x*Derivative(0,0,1)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi111: 
 (-2*x*(x**2/E**(2*F1(x,y,z)) + y**2/E**(2*F2(x,y,z))))/(x**2 + y**2)**2 + \
(2*x*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(2*y**2*w(x,y,z)**2*Derivative(1,0,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)\
) - (2*x**2*(-1 + \
n(x,y,z))*Derivative(1,0,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
((2*x)/E**(2*F1(x,y,z)) - \
(2*x**2*Derivative(1,0,0)(F1)(x,y,z))/E**(2*F1(x,y,z)) - \
(2*y**2*Derivative(1,0,0)(F2)(x,y,z))/E**(2*F2(x,y,z)))/(x**2 + y**2) + \
(x**2*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(y**2*w(x,y,z)**2*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2\
) - (2*x**2*(-1 + \
n(x,y,z))*Derivative(1,0,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) - \
(2*y**2*w(x,y,z)*Derivative(1,0,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi112: 
 (-2*y*(x**2/E**(2*F1(x,y,z)) + y**2/E**(2*F2(x,y,z))))/(x**2 + y**2)**2 - \
(2*y*w(x,y,z)**2)/(E**(2*F0(x,y,z))*n(x,y,z)) + \
(2*y**2*w(x,y,z)**2*Derivative(0,1,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)\
) - (2*x**2*(-1 + \
n(x,y,z))*Derivative(0,1,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
((2*y)/E**(2*F2(x,y,z)) - \
(2*x**2*Derivative(0,1,0)(F1)(x,y,z))/E**(2*F1(x,y,z)) - \
(2*y**2*Derivative(0,1,0)(F2)(x,y,z))/E**(2*F2(x,y,z)))/(x**2 + y**2) + \
(x**2*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(y**2*w(x,y,z)**2*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2\
) - (2*x**2*(-1 + \
n(x,y,z))*Derivative(0,1,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) - \
(2*y**2*w(x,y,z)*Derivative(0,1,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi113: 
 \
(2*y**2*w(x,y,z)**2*Derivative(0,0,1)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)\
) - (2*x**2*(-1 + \
n(x,y,z))*Derivative(0,0,1)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
((-2*x**2*Derivative(0,0,1)(F1)(x,y,z))/E**(2*F1(x,y,z)) - \
(2*y**2*Derivative(0,0,1)(F2)(x,y,z))/E**(2*F2(x,y,z)))/(x**2 + y**2) + \
(x**2*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(y**2*w(x,y,z)**2*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2\
) - (2*x**2*(-1 + \
n(x,y,z))*Derivative(0,0,1)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) - \
(2*y**2*w(x,y,z)*Derivative(0,0,1)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi221: 
 (-2*x*(x**2/E**(2*F2(x,y,z)) + y**2/E**(2*F1(x,y,z))))/(x**2 + y**2)**2 - \
(2*x*w(x,y,z)**2)/(E**(2*F0(x,y,z))*n(x,y,z)) + \
(2*x**2*w(x,y,z)**2*Derivative(1,0,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)\
) - (2*y**2*(-1 + \
n(x,y,z))*Derivative(1,0,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
((2*x)/E**(2*F2(x,y,z)) - \
(2*y**2*Derivative(1,0,0)(F1)(x,y,z))/E**(2*F1(x,y,z)) - \
(2*x**2*Derivative(1,0,0)(F2)(x,y,z))/E**(2*F2(x,y,z)))/(x**2 + y**2) + \
(y**2*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x**2*w(x,y,z)**2*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2\
) - (2*y**2*(-1 + \
n(x,y,z))*Derivative(1,0,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) - \
(2*x**2*w(x,y,z)*Derivative(1,0,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi222: 
 (-2*y*(x**2/E**(2*F2(x,y,z)) + y**2/E**(2*F1(x,y,z))))/(x**2 + y**2)**2 + \
(2*y*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(2*x**2*w(x,y,z)**2*Derivative(0,1,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)\
) - (2*y**2*(-1 + \
n(x,y,z))*Derivative(0,1,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
((2*y)/E**(2*F1(x,y,z)) - \
(2*y**2*Derivative(0,1,0)(F1)(x,y,z))/E**(2*F1(x,y,z)) - \
(2*x**2*Derivative(0,1,0)(F2)(x,y,z))/E**(2*F2(x,y,z)))/(x**2 + y**2) + \
(y**2*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x**2*w(x,y,z)**2*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2\
) - (2*y**2*(-1 + \
n(x,y,z))*Derivative(0,1,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) - \
(2*x**2*w(x,y,z)*Derivative(0,1,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi223: 
 \
(2*x**2*w(x,y,z)**2*Derivative(0,0,1)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)\
) - (2*y**2*(-1 + \
n(x,y,z))*Derivative(0,0,1)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
((-2*y**2*Derivative(0,0,1)(F1)(x,y,z))/E**(2*F1(x,y,z)) - \
(2*x**2*Derivative(0,0,1)(F2)(x,y,z))/E**(2*F2(x,y,z)))/(x**2 + y**2) + \
(y**2*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x**2*w(x,y,z)**2*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2\
) - (2*y**2*(-1 + \
n(x,y,z))*Derivative(0,0,1)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) - \
(2*x**2*w(x,y,z)*Derivative(0,0,1)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi331: 
 (-2*Derivative(1,0,0)(F1)(x,y,z))/E**(2*F1(x,y,z)) - (2*z**2*(-1 + \
n(x,y,z))*Derivative(1,0,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(z**2*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*z**2*(-1 + \
n(x,y,z))*Derivative(1,0,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
gi332: 
 (-2*Derivative(0,1,0)(F1)(x,y,z))/E**(2*F1(x,y,z)) - (2*z**2*(-1 + \
n(x,y,z))*Derivative(0,1,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(z**2*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*z**2*(-1 + \
n(x,y,z))*Derivative(0,1,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
gi333: 
 (2*z*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*Derivative(0,0,1)(F1)(x,y,z))/E**(2*F1(x,y,z)) - (2*z**2*(-1 + \
n(x,y,z))*Derivative(0,0,1)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(z**2*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*z**2*(-1 + \
n(x,y,z))*Derivative(0,0,1)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
gi121: 
 (-2*(E**(-2*F1(x,y,z)) - E**(-2*F2(x,y,z)))*x**2*y)/(x**2 + y**2)**2 + \
((E**(-2*F1(x,y,z)) - E**(-2*F2(x,y,z)))*y)/(x**2 + y**2) + (y*(-1 + \
n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(y*w(x,y,z)**2)/(E**(2*F0(x,y,z))*n(x,y,z)) - \
(2*x*y*w(x,y,z)**2*Derivative(1,0,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) \
- (2*x*y*(-1 + \
n(x,y,z))*Derivative(1,0,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x*y*((-2*Derivative(1,0,0)(F1)(x,y,z))/E**(2*F1(x,y,z)) + \
(2*Derivative(1,0,0)(F2)(x,y,z))/E**(2*F2(x,y,z))))/(x**2 + y**2) + \
(x*y*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(x*y*w(x,y,z)**2*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) \
- (2*x*y*(-1 + \
n(x,y,z))*Derivative(1,0,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) + \
(2*x*y*w(x,y,z)*Derivative(1,0,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi122: 
 (-2*(E**(-2*F1(x,y,z)) - E**(-2*F2(x,y,z)))*x*y**2)/(x**2 + y**2)**2 + \
((E**(-2*F1(x,y,z)) - E**(-2*F2(x,y,z)))*x)/(x**2 + y**2) + (x*(-1 + \
n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x*w(x,y,z)**2)/(E**(2*F0(x,y,z))*n(x,y,z)) - \
(2*x*y*w(x,y,z)**2*Derivative(0,1,0)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)) \
- (2*x*y*(-1 + \
n(x,y,z))*Derivative(0,1,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x*y*((-2*Derivative(0,1,0)(F1)(x,y,z))/E**(2*F1(x,y,z)) + \
(2*Derivative(0,1,0)(F2)(x,y,z))/E**(2*F2(x,y,z))))/(x**2 + y**2) + \
(x*y*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(x*y*w(x,y,z)**2*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) \
- (2*x*y*(-1 + \
n(x,y,z))*Derivative(0,1,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) + \
(2*x*y*w(x,y,z)*Derivative(0,1,0)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi123: 
 \
(-2*x*y*w(x,y,z)**2*Derivative(0,0,1)(F0)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)\
) - (2*x*y*(-1 + \
n(x,y,z))*Derivative(0,0,1)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x*y*((-2*Derivative(0,0,1)(F1)(x,y,z))/E**(2*F1(x,y,z)) + \
(2*Derivative(0,0,1)(F2)(x,y,z))/E**(2*F2(x,y,z))))/(x**2 + y**2) + \
(x*y*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(x*y*w(x,y,z)**2*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z)**2) \
- (2*x*y*(-1 + \
n(x,y,z))*Derivative(0,0,1)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3) + \
(2*x*y*w(x,y,z)*Derivative(0,0,1)(w)(x,y,z))/(E**(2*F0(x,y,z))*n(x,y,z))
gi131: 
 (z*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - (2*x*z*(-1 + \
n(x,y,z))*Derivative(1,0,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x*z*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*x*z*(-1 + \
n(x,y,z))*Derivative(1,0,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
gi132: 
 (-2*x*z*(-1 + \
n(x,y,z))*Derivative(0,1,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x*z*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*x*z*(-1 + \
n(x,y,z))*Derivative(0,1,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
gi133: 
 (x*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - (2*x*z*(-1 + \
n(x,y,z))*Derivative(0,0,1)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(x*z*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*x*z*(-1 + \
n(x,y,z))*Derivative(0,0,1)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
gi231: 
 (-2*y*z*(-1 + \
n(x,y,z))*Derivative(1,0,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(y*z*Derivative(1,0,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*y*z*(-1 + \
n(x,y,z))*Derivative(1,0,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
gi232: 
 (z*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - (2*y*z*(-1 + \
n(x,y,z))*Derivative(0,1,0)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(y*z*Derivative(0,1,0)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*y*z*(-1 + \
n(x,y,z))*Derivative(0,1,0)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
gi233: 
 (y*(-1 + n(x,y,z)))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - (2*y*z*(-1 + \
n(x,y,z))*Derivative(0,0,1)(F1)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) + \
(y*z*Derivative(0,0,1)(n)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**2) - \
(2*y*z*(-1 + \
n(x,y,z))*Derivative(0,0,1)(r)(x,y,z))/(E**(2*F1(x,y,z))*r(x,y,z)**3)
