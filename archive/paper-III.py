# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

A simple code to view precomputed data files and analyze them.

@author: Alexander Wittig
"""

# math routines
import numpy as np
# our own routines
from scene import Scene

### User settings
#

scene = Scene()

# The following settings specify the data file to load.
# These must be exactly the same as when the data file was computed!
# They will be overwritten with the information in the scene data file when loading one!
#
# Which data set (I - XII) or metric (Flat, Schwarzschild, Kerr, EFlat, ESchwarzschild)
scene.dataset = '5'

# The radius of the observer (BL coordinates) or None for automatic
scene.r = None

# The theta angle of the observer (rad)
scene.theta = np.deg2rad(90.0)

# image size in pixels
#scene.size = (32,32)
#scene.size = (64,64)
#scene.size = (256,256)
#scene.size = (8,8)
scene.size = (1024,1024)
#scene.size = (1024,1448)         # poster size image (portrait, DIN ratio of sqrt(2))
#scene.size = (4096,2048)         # 360 degree VR sphere (AveiroCamera with full fov)
#scene.size = (64,4096)           # 5 (strip)
#scene.size = (15,4096)           # 5 (positive y-axis for polar image)

# The zoom region (image coordinates)
scene.zoom = ((-1.0,1.0),(-1.0,1.0))
#scene.zoom = ((0.18,0.49), (0.47,0.55))         # 4.1 (upper lobes formation)
#scene.zoom = ((0.37,0.42), (0.48,0.52))         # 4.1 (upper lobes formation zoom)
#scene.zoom = ((-0.06,0.39), (0.32,0.57))        # 4.2 (upper lobes formation)
#scene.zoom = ((0.16,0.18), (0.1,0.7))           # 5 (strip)
#scene.zoom = ((0.1,0.6), (0.3,0.6))             # 5 (upper brow)
#scene.zoom = ((0.34,0.50), (0.05,0.16))         # 5 (frontal chaos region)
#scene.zoom = ((-0.05,0.05),(0.0,1.0))           # 5 (positive y-axis for polar image)

# Increase integration precision?
scene.tolerance = scene.HIGH_ACCURACY
#scene.tolerance = scene.MEDIUM_ACCURACY
#scene.tolerance = scene.LOW_ACCURACY

# Use cartesian coordinates for the propagation
scene.cartesian = False

# Which projection to use ('S' sterographic, 'G' gnomonic, 'A' Aveiro)
scene.projection = 'A'

# Field of view for the camera (rad)
scene.fov = np.arctan(10.0/15.0)
#scene.fov = (np.arctan(10.0/15.0), np.arctan(14.14/15.0))          # fov for poster size images
#scene.fov = (np.pi, np.pi/2.0)          # full fov for equirectangular VR images

# Sky radius (None for automatic, 0.0 for infinite)
scene.rsky = None

# Custom suffix to be appended to the automatically generated file name (without leading -)
scene.my_suffix = ''
#scene.my_suffix = ' GPUCPU-Win'

# The following settings for the image rendering do not change the file name and do not require
# a recomputation of the dataset. They will not be overwritten when loading a scene data file.
#

# Show an Einstein ring of this angular size (rad), or None for no ring
scene.ring = None

# Show a grid of this many lines in theta (and twice that in phi) or None for no grid
scene.grid = 18

# Celestial sphere background file
scene.background = 'bg-color.png'

# Color to use for rays that fell in the black hole
scene.shadowColor = [0.0, 0.0, 0.0, 1.0]

# Color to use for rays that float around the black hole (or None for shadow color)
scene.floaterColor = [1.0, 0.0, 1.0, 1.0]

# Color to use for rays that have an integrator error (or None for shadow color)
scene.errorColor = [0.0, 1.0, 1.0, 1.0]

### Main code
#

#scene.raytrace()
#scene.raytrace_parallel(__name__, NP=25)
#scene.raytrace_MPI()
#scene.raytrace_GPU(device="GPUCPU", fact=15)

scene.load()

#scene.saveImage()
#scene.saveDescription()
#scene.saveWebGLTexture()
#scene.saveOverlay()

# hack for turning points
#cond = scene.c.directions[:,:,0] < 0
#scene.c.directions[cond,-1] = np.max(scene.c.directions[:,:,-1])*1.01
#np.savetxt('11-turningpoints.txt', scene.c.directions[:,:,-1], '%d')

scene.show(True)


scene.d.trace(0.806,0.395)
scene.d.trace(0.735, 0.011)
scene.d.trace(0.5045, 0.025)
scene.d.trace(0.3374, 0.4374)
scene.d.trace(0.394, 0.4263)
scene.d.trace(-0.00001, 0.260)
scene.d.trace(0.055,0.2645)
