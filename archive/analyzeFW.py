# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 09:46:03 2017

@author: Alexander Wittig
"""

X, theta = np.meshgrid(np.linspace(0, 1, 501), np.linspace(0, np.pi, 501))

F0=scene.g.f.F0(X, theta, 0, 1 )
F1=scene.g.f.F1(X, theta, 0, 1 )
F2=scene.g.f.F2(X, theta, 0, 1 )
W=scene.g.f.W(X, theta, 0, 1 )

print("Maximum d/dtheta:")
print("F0: ", np.max(np.abs(F0)))
print("F1: ", np.max(np.abs(F1)))
print("F2: ", np.max(np.abs(F2)))
print(" W: ", np.max(np.abs(W)))
