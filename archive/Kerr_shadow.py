# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

@author: Alexander Wittig
"""

# math routines
from math import sin, cos
import numpy as np
# image generation and plotting routines
import matplotlib.pyplot as pyplot
# timing routines
from time import clock
# our own routines
from metrics import Kerr, Schwarzschild, Flat, Interpolated
from cameras import StereographicCamera
from display import Display

def analyticalShadow(cam, rp):
    """Get viewing direction that has the same orbital constants as a circular orbit of radius rp"""
    # everything at critical radius rp
    Delta = rp**2 + cam.g.A**2 - cam.g.RS*rp
    Delta_prime = 2.0*rp - cam.g.RS
    KE = 16.0*rp**2*Delta/Delta_prime**2
    aLE = rp**2 + cam.g.A**2 - 4.0*rp*Delta/Delta_prime
    #print("LE= "+aLE/cam.g.A+"         KE= "+KE)
    # from now on everything at camera location
    cam.g.setPoint([cam.t, cam.r, cam.theta, cam.phi])
    Delta = cam.r**2 + cam.g.A**2 - cam.g.RS*cam.r
    rho2 = cam.r**2 + cam.g.A**2*cos(cam.theta)**2
    grr = rho2/Delta
    gpp = cam.g.tt*(-Delta*sin(cam.theta)**2) # convert upper to lower index: g_pp = g^tt/det(g^)=g^tt*det(g_)=g^tt*(-Delta*sin(theta)^2)
    RE = -KE/Delta + ((cam.r**2+cam.g.A**2)-aLE)**2/Delta**2
    sigmaE = np.sqrt(-cam.g.tt)*(1.0 - aLE/cam.g.A*(cam.g.tp/cam.g.tt))
    sina = np.sqrt(1.0-RE/sigmaE**2/grr)
    cosb = -aLE/(cam.g.A*sigmaE*sina*np.sqrt(gpp))
    a = np.arcsin( sina )
    b = np.arccos( cosb )
    return (a, b)

def plotShadow(cam, rmin, rmax, N=100):
    """Plot the border of the shadow based on the photon sphere orbits"""
    r = np.linspace(rmin, rmax, N)
    a,b = analyticalShadow(cam, r)
    x,y = cam.project(a, b)
    pyplot.figure(1).gca().plot(x, y, color='#aaaa33', linewidth=3.0)
    return (a, b)

def plotShadowDifference(cam, rmin, rmax, N=100):
    """Plot the difference between the numerical and analytical shadow"""
    r = np.linspace(rmin, rmax, N)
    a,b = analyticalShadow(cam, r)
    anum = np.zeros(N)
    for i in range(N):
        anum[i] = cam.getShadow(b[i])
    fig = pyplot.figure()
    fig.gca().plot(np.rad2deg(b), np.rad2deg(a-anum))
    fig.gca().set_xlabel('beta [degrees]')
    fig.gca().set_ylabel('alpha error (analytical-numerical) [degrees]')
    x,y = cam.project(anum, b)
    pyplot.figure(1).gca().plot(x, y, color='#33aaaa', linewidth=3.0)


g = Kerr(2.00, 1.0); suffix = '-K{:02}'.format(int(g.A*10))
c = StereographicCamera(g, 15.0, np.pi/2, np.pi/4, suffix=suffix)
c.load();
d = Display(c)
d.show()
plotShadow(c, 1.0001, 3.999)
plotShadowDifference(c, 1.000001, 3.99999)
