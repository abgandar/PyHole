# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

@author: Alexander Wittig
"""

# math routines
import numpy as np
from math import sqrt, sin, cos
# timing routines
from time import clock, time, sleep
# our own routines
from metrics import Flat, Schwarzschild, Kerr, ExponentialFlat, ExponentialSchwarzschild, ExponentialInterpolated
from cameras import StereographicCamera, AveiroCamera
from display import Display
# Configuration
from config import Configuration
import os.path
import sys

from scipy.optimize import minimize_scalar, minimize, newton, ridder

#import matplotlib.animation as animation
#import matplotlib.pyplot as pyplot
#from subprocess import run
#from tempfile import mkdtemp
#from shutil import rmtree

#from math import sqrt, acos

dataset='II'
g = ExponentialInterpolated(os.path.join(Configuration.data_path, 'configuration-'+dataset+'.npz'))
if g.m_ADM>0:
#    r = g.getRadius( g.m_ADM*15.0 )
#    rsky = g.getRadius( g.m_ADM*30.0 )
    r = g.getRadius( g.m_ADM*30.0 )
    rsky = g.getRadius( g.m_ADM*45.0 )
    suffix = '-MUSE2'
    #suffix = ''
else:
    r = g.getRadius(22.5)
    rsky = g.getRadius(2*22.5)

#dataset = ''
#g = Flat()
#g = Schwarzschild(2.0)
#g = Kerr(2.0,1.0)
#g = ExponentialFlat()
#g = ExponentialSchwarzschild(2.0)
#r = 15.0
#rsky = 30.0

theta = float( sys.argv[1] ) if len(sys.argv) > 1 else 90.0
#zoom = ((0.68,0.72),(0.08,0.12))
zoom=((-1.0,1.0),(-1.0,1.0))
#cartesian = True; suffix = 'C'
cartesian = False; #suffix = ''
size = (2048,2048)
#size = (1024,1024)
#size = (512,512)
#size = (256,256)
#size = (32,32)

cam = AveiroCamera(g, r, np.deg2rad(theta), np.arctan(10.0/15.0), integrator='vode', Rsky=rsky, Cartesian=cartesian, zoom=zoom)
#cam = StereographicCamera(g, r, np.pi/2, np.arctan(10.0/15.0), integrator='vode') #dopri5

cam.ring = False; suffix = suffix+'-NR'

#suffix = suffix+'-H'

#t0 = time()
#cam.generateDirections(size)
#t1 = time()
#cam.walltime = t1-t0
#if dataset+suffix == '':
#    cam.saveAll()
#else:
#    cam.saveAll(dataset+suffix)
#print("Comptuation time: {} s    ({} min)".format(t1-t0, (t1-t0)/60.0) )

if dataset+suffix == '':
    cam.load(size);
else:
    cam.load(size, dataset+suffix);
cam.grid = False
#cam.generateImage('cd1.png')
cam.generateImage('cd4.png')
#cam.generateImage('bg-office1.png')
#cam.generateImage('bg-color-smooth2.png')
if dataset+suffix == '':
    cam.saveImage()
else:
    cam.saveImage(dataset+suffix)

print(cam)

if r!=cam.r: print("Warning: Observer r in data file differs from r set in user code. Proceeding with r from data file.\n")

tot = cam.directions.shape[0]*cam.directions.shape[1]
nerr = np.count_nonzero(cam.directions[:,:,1] == -1.0)
nfloat = np.count_nonzero(cam.directions[:,:,1] == -2.0)
print("Total: {}     Captured: {} ({:.2f} %)    Floaters: {} ({:.2f} %)".format(tot, nerr, nerr/tot*100.0, nfloat, nfloat/tot*100.0))

if hasattr(cam,'getBHSize') and not cam.zoomed():
    s = cam.getBHSize()
    print("DC:   {}".format(15.0*cam.fov*abs(s[0]+s[1])/2))
    print("Dx:   {}".format(15.0*cam.fov*(s[1]-s[0])))
    print("Dy:   {}".format(15.0*cam.fov*(s[3]-s[2])))
    print("Ravg: {}".format(15.0*cam.fov*cam.int1()))
    print("Sigr: {}".format(15.0*cam.fov*cam.int2()))

d = Display(cam)
d.show()


import matplotlib.pyplot as pyplot
cam.showFloaters = False
imax = 150
for i in range(imax):
    dx = -i/(imax-1)
    dy = (dx+0.5)**2-0.28
    cam.generateImage('this is ignored at this point',dx+0.25,dy)
    d.redraw()
    pyplot.imsave('muse'+str(i)+'.png', cam.image)
    pyplot.pause(0.0001)




from util import *



#ans = minimize_scalar(obj,bracket=(-11.2,-10.8),bounds=(-11.2,-10.8),tol=1e-7)
#L=ans.x
#d.traceIC( getICJai(0.17,L), False )



# Some useful commands available on the console only:

# to plot orbit of an arbitrary initial condition (must open a trace window first by right clicking on any point):
#d.traceIC( [t, r, th, phi, pt, pr, pth, pphi], False )
#    where [t, r, th, phi, pt, pr, pth, pphi] are the initial conditions
#d.traceIC( [t, r, th, phi, alpha, beta], False )
#    where [t, r, th, phi, alpha, beta] are the starting point and the viewing direction

# to find the initial condition for a closed, circular orbit
#ic = g.find_circular(r)
#    where r is an initial guess for the location of the orbit (0.5 seems to work well for stable and 1.5 for unstable)
#    returns an initial condition of the form [t, r, th, phi, pt, pr, pth, pphi]
